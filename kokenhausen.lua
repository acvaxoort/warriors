setArea(2000, 1000)
setGridSize(10);
sweden = addTeam("Szwecja", 100, 120, 255)
poland = addTeam("Polska", 255, 100, 100)

--Prawe skrzydło polsko-litewskie
addHussarRegiment{team = poland, type = "Husarz", count = 500, rows = 10, x = 300, y = 720, angle = 90}
addHussarRegiment{team = poland, type = "Husarz", count = 500, rows = 10, x = 300, y = 850, angle = 90}
addHussarRegiment{team = poland, type = "Petyhorzec", count = 300, rows = 10, x = 270, y = 720, angle = 90}

--Lewe skrzydło polsko-litewskie
addHussarRegiment{team = poland, type = "Petyhorzec", count = 300, rows = 10, x = 300, y = 250, angle = 90, waiting_time = 550}
addPikeAndShootRegiment{team = poland, count = 200, rows = 6, cannons = 5, x = 300, y = 300, angle = 90}

--Trzon polsko-litewski
addHussarRegiment{team = poland, type = "Petyhorzec", count = 1000, rows = 10, x = 200, y = 600, angle = 90, waiting_time = 450}
addPikeAndShootRegiment{team = poland, count = 100, rows = 6, cannons = 4, x = 150, y = 500, angle = 90}


--Lewe skrzydło szwedzkie
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1500, y = 800, angle = 270}
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1550, y = 750, angle = 270}
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1600, y = 700, angle = 270}
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1650, y = 750, angle = 270}

--Prawe skrzydło szwedzkie
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1500, y = 200, angle = 270, waiting_time = 250, prioritize_infantry = true}
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1550, y = 250, angle = 270, waiting_time = 250, prioritize_infantry = true}
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1600, y = 300, angle = 270, waiting_time = 250, prioritize_infantry = true}
addReiterRegiment{team = sweden, count = 500, rows = 6, x = 1650, y = 250, angle = 270, waiting_time = 250, prioritize_infantry = true}

--Trzon szwedzki
addPikeAndShootRegiment{team = sweden, count = 900, rows = 10, cannons = 17, x = 1700, y = 500, angle = 270}
