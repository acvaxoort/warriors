//
// Created by aleksander on 11.07.18.
//

#ifndef WARRIORS_ENTITYSYSTEM_H
#define WARRIORS_ENTITYSYSTEM_H


#include <memory>
#include <vector>
#include <set>
#include <SFML/Graphics.hpp>
#include <entitysystem/Entity.h>
#include <entitysystem/SpatialIndexGrid.h>
#include <random>
#include <util/RandomWrapper.hpp>

class EntitySystem {
public:

  class OutwardsInner;
  class All;

  class DisplayData {
  public:
    void display(sf::RenderWindow& window, const EntitySystem::DisplayData& previous_frame,
                 double interpolation_fraction);
  private:
    std::vector<std::unique_ptr<Entity::DisplayData>> entities;
    friend class EntitySystem;
  };

  explicit EntitySystem(position_data_t grid_width = 10,
                        position_data_t grid_min_x = -128, position_data_t grid_min_y = -128,
                        position_data_t grid_size_x = 256, position_data_t grid_size_y = 256);

  virtual ~EntitySystem() = default;
  EntitySystem(const EntitySystem& other) = delete;
  EntitySystem& operator=(const EntitySystem& other) = delete;
  EntitySystem(EntitySystem&& other) noexcept;
  EntitySystem& operator=(EntitySystem&& other) noexcept;

  void addEntity(std::unique_ptr<Entity> entity);
  void removeEntity(int64_t index);
  void update();
  void update1_Entities();
  void update2_Index();
  void update3_Delete();
  void doCollideEach();
  void doCollideEachWith(EntitySystem& other);
  void forgetAbout(Entity* e);
  std::unique_ptr<EntitySystem::DisplayData> outputDisplayData();

  /*
   * Get range-for-compatible iterator of indices that belong to the grid cell that contains pos_x and pos_y and all
   * neighboring 8 cells.
   */
  SpatialIndexGrid::Adjacent adjacent(position_data_t pos_x, position_data_t pos_y) const;

  /*
   * Get range-for-compatible iterator of indices that belong to all grid cells that intersect with circle of given
   * radius with center in pos_x, pos_y. Makes it easy to process all entities in radius (the distance to each entity
   * can be larger than radius, the margin of error is equal to size of the grid cell).
   */
  SpatialIndexGrid::AdjacentRadius adjacentRadius(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const;

  /* Get range-for-compatible iterator of indices that belong to a square that is quaranteed to contain circle of
   * given radius centered around pos_x, pos_y.
   */
  SpatialIndexGrid::AdjacentRadiusSquare adjacentRadiusSquare(position_data_t pos_x, position_data_t pos_y,
                                                              position_data_t radius) const;

  /* Get range-for-compatible iterator of indices that belong to a square similar to adjacentRadiusSquare using
   * outer_radius but do not belong to a squarewith inner_radius.
   */
  SpatialIndexGrid::OutwardsOuter outwardsOuter(position_data_t pos_x, position_data_t pos_y,
                                                position_data_t inner_radius, position_data_t outer_radius) const;

  /* Get range-for-compatible iterator of indices that belong to a square similar to adjacentRadiusSquare using
   * outer_radius but do not belong to a squarewith inner_radius.
   */
  EntitySystem::OutwardsInner outwardsInnerSorted(position_data_t pos_x, position_data_t pos_y,
                                                  position_data_t inner_radius) const;

  /* Get range-for-compatible iterator of all indices.
   */
  EntitySystem::All all() const;

  class OutwardsInner {
  public:
    class Iterator {
    public:
      inline int64_t operator*() const { return curr->first; };
      inline bool operator!=(const Iterator& other) const { return curr != other.curr; };
      inline Iterator& operator++() { ++curr; return *this; };

    private:
      std::vector<std::pair<int64_t, position_data_t>>::iterator curr;
      std::shared_ptr<std::vector<std::pair<int64_t, position_data_t>>> vec;
      friend class OutwardsInner;
    };

    Iterator begin() const;
    Iterator end() const;

  private:
    OutwardsInner(const EntitySystem* system, position_data_t pos_x, position_data_t pos_y,
                  position_data_t inner_radius);
    std::shared_ptr<std::vector<std::pair<int64_t, position_data_t>>> vec;
    friend class EntitySystem;
  };

  class All {
  public:
    class Iterator {
    public:
      inline int64_t operator*() const { return curr; };
      inline bool operator!=(const Iterator& other) const { return curr != other.curr; };
      inline Iterator& operator++() { ++curr; return *this; };

    private:
      int64_t curr;
      friend class All;
    };

    Iterator begin() const;
    Iterator end() const;

  private:
    All(int64_t num_entities);
    int64_t num_entities;
    friend class EntitySystem;
  };

  SpatialIndexGrid spatial_index;
  std::vector<std::unique_ptr<Entity>> entities;
private:
  void doRemoveEntity(int64_t index);

  std::set<int64_t> deleted_indices;
  std::vector<int64_t> deletion_queue;
  std::vector<Entity*> forgetting_queue;
};


#endif //WARRIORS_ENTITYSYSTEM_H
