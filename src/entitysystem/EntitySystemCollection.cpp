//
// Created by aleksander on 23.05.19.
//

#include <iostream>
#include "EntitySystemCollection.h"

EntitySystemCollection::EntitySystemCollection(position_data_t grid_width,
    position_data_t grid_min_x, position_data_t grid_min_y, position_data_t grid_size_x, position_data_t grid_size_y)
    : grid_width(grid_width),
      grid_min_x(grid_min_x),
      grid_min_y(grid_min_y),
      grid_size_x(grid_size_x),
      grid_size_y(grid_size_y) {}

void EntitySystemCollection::addEntity(size_t subsystem, std::unique_ptr<Entity> entity) {
  while (subsystem >= subsystems.size()) {
    subsystems.emplace_back(grid_width, grid_min_x, grid_min_y, grid_size_x, grid_size_y);
  }
  subsystems[subsystem].addEntity(std::move(entity));
}

void EntitySystemCollection::removeEntity(size_t subsystem, int64_t index) {
  size_t subs = subsystems.size();
  if (subsystem >= subs) {
    std::cout<<"There is no subsystem " + std::to_string(subsystem) + "\n";
  }
  subsystems[subsystem].removeEntity(index);
}

void EntitySystemCollection::forgetAbout(Entity *e) {
  for (auto& sub : subsystems) {
     sub.forgetAbout(e);
  }
}

void EntitySystemCollection::update() {
  for (auto& sub : subsystems) {
    sub.update1_Entities();
  }
  for (auto& sub : subsystems) {
    sub.update2_Index();
    sub.update3_Delete();
  }
}

void EntitySystemCollection::doCollideEach() {
  size_t subs = subsystems.size();
  for (size_t i = 0; i < subs; ++i) {
    subsystems[i].doCollideEach();
    for (size_t j = i+1; j < subs; ++j) {
      subsystems[i].doCollideEachWith(subsystems[j]);
    }
  }
}

std::unique_ptr<EntitySystemCollection::DisplayData> EntitySystemCollection::outputDisplayData() {
  auto result = std::make_unique<EntitySystemCollection::DisplayData>();
  for (auto& sub : subsystems) {
    result->subsystems.push_back(sub.outputDisplayData());
  }
  return result;
}

void EntitySystemCollection::DisplayData::display(sf::RenderWindow &window,
    const EntitySystemCollection::DisplayData &previous_frame, double interpolation_fraction) {
  size_t sys_count = std::min(subsystems.size(), previous_frame.subsystems.size());
  for (size_t i = 0; i < sys_count; ++i) {
    subsystems[i]->display(window, *previous_frame.subsystems[i], interpolation_fraction);
  }
}
