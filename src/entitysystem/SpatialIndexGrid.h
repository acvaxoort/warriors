//
// Created by aleksander on 14.07.18.
//

#ifndef WARRIORS_SPATIALINDEXGRID_H
#define WARRIORS_SPATIALINDEXGRID_H

#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include "Entity.h"

class SpatialIndexGrid {
public:
  class Adjacent;
  class AdjacentRadius;
  class AdjacentRadiusSquare;
  class OutwardsOuter;

  explicit SpatialIndexGrid(position_data_t width = 10.0f,
                            position_data_t min_x = -128, position_data_t min_y = -128,
                            position_data_t size_x = 256, position_data_t size_y = 256);
  SpatialIndexGrid(const SpatialIndexGrid& other) = delete;
  SpatialIndexGrid& operator=(const SpatialIndexGrid& other) = delete;
  SpatialIndexGrid(SpatialIndexGrid&& other) noexcept = default;
  SpatialIndexGrid& operator=(SpatialIndexGrid&& other) noexcept = default;

  void emplace(int64_t index, position_data_t pos_x, position_data_t pos_y);
  void remove(int64_t index, position_data_t pos_x, position_data_t pos_y);
  void update(int64_t index, position_data_t pos_x, position_data_t pos_y, position_data_t old_pos_x, position_data_t old_pos_y);

  //Deprecated
  std::vector<int64_t> getAdjacentIndices(position_data_t pos_x, position_data_t pos_y) const;
  std::vector<int64_t> getIndicesInRadius(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const;
  void doOnAdjacentIndices(position_data_t pos_x, position_data_t pos_y, const std::function<void(int64_t)>& func) const;

  class Adjacent {
  public:
    class Iterator {
    public:
      inline int64_t operator*() const { return curr_val; };
      inline bool operator!=(const Iterator& other) const { return j != other.j; };
      Iterator& operator++();

    private:
      const SpatialIndexGrid* grid;
      int64_t index_x_beg;
      int64_t index_y_beg;
      int64_t index_x_end;
      int64_t index_y_end;
      int64_t i;
      int64_t j;
      int64_t curr_val;
      const std::vector<int64_t>* vec;
      std::vector<int64_t>::const_iterator cur;
      std::vector<int64_t>::const_iterator end;
      bool currently_vec;

      bool findNextVec();
      friend class Adjacent;
    };

    Adjacent(const SpatialIndexGrid* grid, position_data_t pos_x, position_data_t pos_y);
    Iterator begin() const;
    Iterator end() const;

  private:
    const SpatialIndexGrid* grid;
    position_data_t pos_x;
    position_data_t pos_y;
  };

  class AdjacentRadius {
  public:
    class Iterator {
    public:
      inline int64_t operator*() const { return curr_val; };
      inline bool operator!=(const Iterator& other) const { return j != other.j; };
      Iterator& operator++();

    private:
      const SpatialIndexGrid* grid;
      int64_t index_x;
      int64_t index_y;
      int64_t index_x_beg;
      int64_t index_y_beg;
      int64_t index_x_end;
      int64_t index_y_end;
      int64_t i;
      int64_t j;
      int64_t curr_val;
      position_data_t pos_x;
      position_data_t pos_y;
      position_data_t radius;
      const std::vector<int64_t>* vec;
      std::vector<int64_t>::const_iterator cur;
      std::vector<int64_t>::const_iterator end;
      bool currently_vec;

      bool findNextVec();
      friend class AdjacentRadius;
    };

    AdjacentRadius(const SpatialIndexGrid* grid, position_data_t pos_x, position_data_t pos_y, position_data_t radius);
    Iterator begin() const;
    Iterator end() const;

  private:
    const SpatialIndexGrid* grid;
    position_data_t pos_x;
    position_data_t pos_y;
    position_data_t radius;

    friend class SpatialIndexGrid;
  };

  class AdjacentRadiusSquare {
  public:
    class Iterator {
    public:
      inline int64_t operator*() const { return curr_val; };
      inline bool operator!=(const Iterator& other) const { return j != other.j; };
      Iterator& operator++();

    private:
      const SpatialIndexGrid* grid;
      int64_t index_x_beg;
      int64_t index_y_beg;
      int64_t index_x_end;
      int64_t index_y_end;
      int64_t i;
      int64_t j;
      int64_t curr_val;
      const std::vector<int64_t>* vec;
      std::vector<int64_t>::const_iterator cur;
      std::vector<int64_t>::const_iterator end;
      bool currently_vec;

      bool findNextVec();
      friend class AdjacentRadiusSquare;
    };

    AdjacentRadiusSquare(const SpatialIndexGrid* grid, position_data_t pos_x, position_data_t pos_y, position_data_t radius);
    Iterator begin() const;
    Iterator end() const;

  private:
    const SpatialIndexGrid* grid;
    position_data_t pos_x;
    position_data_t pos_y;
    position_data_t radius;

    friend class SpatialIndexGrid;
  };

  class OutwardsOuter {
  public:
    class Iterator {
    public:
      inline int64_t operator*() const { return curr_val; };
      inline bool operator!=(const Iterator& other) const { return curr_radius != other.curr_radius; };
      Iterator& operator++();

    private:
      const SpatialIndexGrid* grid;
      int64_t index_x;
      int64_t index_y;
      int64_t max_radius;
      int64_t curr_radius;
      int64_t curr_offset;
      int64_t curr_rotation;
      int64_t curr_val;
      const std::vector<int64_t>* vec;
      std::vector<std::pair<int64_t, position_data_t>> first_vec;
      std::vector<int64_t>::const_iterator cur;
      std::vector<int64_t>::const_iterator end;
      bool currently_vec;

      bool findNextVec();
      friend class OutwardsOuter;
    };

    OutwardsOuter(const SpatialIndexGrid* grid, position_data_t pos_x, position_data_t pos_y,
                        position_data_t inner_radius, position_data_t outer_radius);
    Iterator begin() const;
    Iterator end() const;

  private:
    const SpatialIndexGrid* grid;
    position_data_t pos_x;
    position_data_t pos_y;
    position_data_t inner_radius;
    position_data_t outer_radius;

    friend class SpatialIndexGrid;
  };

  std::string debugInfo();

private:
  position_data_t width;
  int64_t min_x;
  int64_t min_y;
  int64_t size_x;
  int64_t size_y;
  std::vector<std::vector<std::vector<int64_t>>> indices;
  std::map<std::pair<int64_t, int64_t>, std::vector<int64_t>> abnormal_indices;
};


#endif //WARRIORS_SPATIALINDEXGRID_H
