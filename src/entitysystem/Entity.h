//
// Created by aleksander on 11.07.18.
//

#ifndef WARRIORS_ENTITY_H
#define WARRIORS_ENTITY_H

#include <SFML/Graphics/RenderWindow.hpp>

class EntitySystem;

typedef double position_data_t;

#define OUTPUT_DISPLAY_DATA(Type) return std::make_unique<Type::DisplayData>(*this);
#define CREATE_CORPSE(Type) return std::make_unique<Type::Corpse>(getDefaultCorpseColor());

class Entity {
public:

  class DisplayData {
  public:
    explicit DisplayData(const Entity& e);
    virtual void display(sf::RenderWindow& window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) = 0;
    int entity_id;
    position_data_t pos_x;
    position_data_t pos_y;
    position_data_t angle;
  };

  Entity();
  virtual ~Entity() = default;
  void setPos(position_data_t x, position_data_t y);
  inline void setAngle(position_data_t a) { angle = a; };
  inline void setVel(position_data_t x, position_data_t y) { vel_x = x; vel_y = y; };
  inline void accelerate(position_data_t x, position_data_t y) { vel_x += x; vel_y += y; };
  inline position_data_t getPosX() const { return pos_x; };
  inline position_data_t getPosY() const { return pos_y; };
  inline position_data_t getAngle() const { return angle; };
  bool colllidesWith(const Entity& other) const;
  bool colllidesWithPoint(position_data_t x, position_data_t y) const;
  void ellasticCollision(Entity& other);
  void inellasticCollision(Entity &other);
  void setCollisionRadius(position_data_t r);
  virtual void removeSelf();
  virtual void init();
  virtual void update();
  virtual void onEntityCollision(Entity& other);
  virtual void forgetAbout(Entity* e);
  virtual std::unique_ptr<Entity::DisplayData> outputDisplayData();

  position_data_t pos_x;
  position_data_t pos_y;
  position_data_t vel_x;
  position_data_t vel_y;
  position_data_t angle;
  position_data_t collision_radius;

protected:
  EntitySystem* system;
  inline int getId() { return unique_id; }

private:
  position_data_t new_pos_x;
  position_data_t new_pos_y;
  static int next_id;
  int unique_id;

  friend class EntitySystem;


};


#endif //WARRIORS_ENTITY_H
