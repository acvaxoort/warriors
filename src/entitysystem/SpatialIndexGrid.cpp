//
// Created by aleksander on 14.07.18.
//

#include <algorithm>
#include <iostream>
#include <sstream>
#include "SpatialIndexGrid.h"
#include "EntitySystem.h"
#include <util/PlanarGeometryUtil.h>

SpatialIndexGrid::SpatialIndexGrid(position_data_t width,
    position_data_t min_x, position_data_t min_y, position_data_t size_x, position_data_t size_y)
    : width(width) {
  this->min_x = static_cast<int64_t>(std::floor(min_x / width));
  this->min_y = static_cast<int64_t>(std::floor(min_y / width));
  this->size_x = 1 + this->min_x + static_cast<int64_t>(std::ceil((size_x-min_x) / width));
  this->size_y = 1 + this->min_y + static_cast<int64_t>(std::ceil((size_y-min_y) / width));

  indices.resize(static_cast<size_t>(this->size_y), std::vector<std::vector<int64_t>>(static_cast<size_t>(this->size_x)));
}

void SpatialIndexGrid::emplace(int64_t index, position_data_t pos_x, position_data_t pos_y) {
  int64_t index_x = static_cast<int64_t>(pos_x / width) - min_x;
  int64_t index_y = static_cast<int64_t>(pos_y / width) - min_y;
  if (index_x < 0 || index_x >= size_x || index_y < 0 || index_y >= size_y) {
    abnormal_indices[{index_x, index_y}].emplace_back(index);
  } else {
    indices[index_y][index_x].emplace_back(index);
  }
}

void SpatialIndexGrid::remove(int64_t index, position_data_t pos_x, position_data_t pos_y) {
  int64_t index_x = static_cast<int64_t>(pos_x / width) - min_x;
  int64_t index_y = static_cast<int64_t>(pos_y / width) - min_y;
  std::vector<int64_t>* vec;
  if (index_x < 0 || index_x >= size_x || index_y < 0 || index_y >= size_y) {
    auto it = abnormal_indices.find({index_x, index_y});
    if (it != abnormal_indices.end()) {
      vec = &it->second;
    } else {
      std::cout<<"SpatialIndexGrid: attempted to delete a non-existant index (bad abnormal position)"<<std::endl;
      return;
    }
  } else {
    vec = &indices[index_y][index_x];
  }
  auto last = vec->end();
  auto it = std::find(vec->begin(), last, index);
  if (it != last) {
    --last;
    *it = *last;
    vec->pop_back();
  } else {
    std::cout<<"SpatialIndexGrid: attempted to delete a non-existant index (not found on position)"<<std::endl;
  }
}

void SpatialIndexGrid::update(int64_t index, position_data_t pos_x, position_data_t pos_y,
    position_data_t old_pos_x, position_data_t old_pos_y) {
  int64_t index_x = static_cast<int64_t>(pos_x / width);
  int64_t index_y = static_cast<int64_t>(pos_y / width);
  int64_t old_index_x = static_cast<int64_t>(old_pos_x / width);
  int64_t old_index_y = static_cast<int64_t>(old_pos_y / width);
  if (index_x == old_index_x && index_y == old_index_y) {
    return;
  }
  index_x -= min_x;
  index_y -= min_y;
  old_index_x -= min_x;
  old_index_y -= min_y;
  std::vector<int64_t>* vec;
  if (old_index_x < 0 || old_index_x >= size_x || old_index_y < 0 || old_index_y >= size_y) {
    auto it = abnormal_indices.find({old_index_x, old_index_y});
    if (it != abnormal_indices.end()) {
      vec = &it->second;
    } else {
      std::cout<<"SpatialIndexGrid (update): attempted to delete a non-existant index (bad abnormal position)"<<std::endl;
      vec = nullptr;
    }
  } else {
    vec = &indices[old_index_y][old_index_x];
  }
  if (vec) {
    auto last = vec->end();
    auto it = std::find(vec->begin(), last, index);
    if (it != last) {
      --last;
      *it = *last;
      vec->pop_back();
    } else {
      std::cout<<"SpatialIndexGrid (update): attempted to delete a non-existant index (not found on position)"<<std::endl;
    }
  }
  if (index_x < 0 || index_x >= size_x || index_y < 0 || index_y >= size_y) {
    abnormal_indices[{index_x, index_y}].emplace_back(index);
  } else {
    indices[index_y][index_x].emplace_back(index);
  }
}

std::vector<int64_t> SpatialIndexGrid::getAdjacentIndices(position_data_t pos_x, position_data_t pos_y) const {
  std::vector<int64_t> result;
  int64_t index_x = static_cast<int64_t>(pos_x / width) - min_x - 1;
  int64_t index_y = static_cast<int64_t>(pos_y / width) - min_y - 1;
  int64_t index_x_end = index_x + 3;
  int64_t index_y_end = index_y + 3;
  for (int64_t j = index_y; j < index_y_end; ++j) {
    for (int64_t i = index_x; i < index_x_end; ++i) {
      const std::vector<int64_t>* vec;
      if (i < 0 || i >= size_x || j < 0 || j >= size_y) {
        auto it = abnormal_indices.find({i, j});
        if (it != abnormal_indices.end()) {
          vec = &it->second;
        } else {
          vec = nullptr;
        }
      } else {
        vec = &indices[j][i];
      }
      if (vec) {
        result.insert(result.end(), vec->begin(), vec->end());
      }
    }
  }
  return result;
}

void SpatialIndexGrid::doOnAdjacentIndices(position_data_t pos_x, position_data_t pos_y, const std::function<void(int64_t)>& func) const {
  int64_t index_x = static_cast<int64_t>(pos_x / width) - min_x - 1;
  int64_t index_y = static_cast<int64_t>(pos_y / width) - min_y - 1;
  int64_t index_x_end = index_x + 3;
  int64_t index_y_end = index_y + 3;
  for (int64_t j = index_y; j < index_y_end; ++j) {
    for (int64_t i = index_x; i < index_x_end; ++i) {
      const std::vector<int64_t>* vec;
      if (i < 0 || i >= size_x || j < 0 || j >= size_y) {
        auto it = abnormal_indices.find({i, j});
        if (it != abnormal_indices.end()) {
          vec = &it->second;
        } else {
          vec = nullptr;
        }
      } else {
        vec = &indices[j][i];
      }
      if (vec) {
        for (auto it = vec->begin(), end = vec->end(); it != end; ++it) {
          func(*it);
        }
      }
    }
  }
}

SpatialIndexGrid::Adjacent::Adjacent(const SpatialIndexGrid* grid, position_data_t pos_x,
                                                   position_data_t pos_y)
    : grid(grid),
      pos_x(pos_x),
      pos_y(pos_y) {
}

SpatialIndexGrid::Adjacent::Iterator SpatialIndexGrid::Adjacent::begin() const {
  SpatialIndexGrid::Adjacent::Iterator it;
  it.index_x_beg = static_cast<int64_t>(pos_x / grid->width) - grid->min_x - 1;
  it.index_y_beg = static_cast<int64_t>(pos_y / grid->width) - grid->min_y - 1;
  it.index_x_end = it.index_x_beg + 3;
  it.index_y_end = it.index_y_beg + 3;
  it.i = it.index_x_end - 1;
  it.j = it.index_y_beg - 1;
  it.grid = grid;
  it.currently_vec = false;
  if (it.findNextVec()) {
    it.currently_vec = true;
    it.curr_val = *it.cur;
    ++it.cur;
    if (it.cur == it.end) {
      it.currently_vec = false;
    }
  }
  return it;
}

SpatialIndexGrid::Adjacent::Iterator SpatialIndexGrid::Adjacent::end() const {
  SpatialIndexGrid::Adjacent::Iterator it;
  it.j = static_cast<int64_t>(pos_y / grid->width) - grid->min_y + 2;
  return it;
}

SpatialIndexGrid::Adjacent::Iterator& SpatialIndexGrid::Adjacent::Iterator::operator++() {
  if (currently_vec) {
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  } else {
    if (!findNextVec()) {
      return *this;
    }
    currently_vec = true;
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  }
  return *this;
}

bool SpatialIndexGrid::Adjacent::Iterator::findNextVec() {
  vec = nullptr;
  while (!vec) {
    ++i;
    if (i == index_x_end) {
      i = index_x_beg;
      ++j;
      if (j == index_y_end) {
        return false;
      }
    }
    if (i < 0 || i >= grid->size_x || j < 0 || j >= grid->size_y) {
      auto it = grid->abnormal_indices.find({i, j});
      if (it != grid->abnormal_indices.end()) {
        vec = &it->second;
      }
    } else {
      vec = &grid->indices[j][i];
    }
    if (vec) {
      cur = vec->begin();
      end = vec->end();
      if (cur == end) {
        vec = nullptr;
      }
    }
  }
  return true;
}

std::vector<int64_t> SpatialIndexGrid::getIndicesInRadius(position_data_t pos_x, position_data_t pos_y,
                                                          position_data_t radius) const {
  std::vector<int64_t> result;
  radius /= width;
  pos_x /= width;
  pos_y /= width;
  pos_x -= min_x;
  pos_y -= min_y;
  int64_t index_x_beg = static_cast<int64_t>(pos_x - radius);
  int64_t index_y_beg = static_cast<int64_t>(pos_y - radius);
  int64_t index_x_end = static_cast<int64_t>(pos_x + radius) + 1;
  int64_t index_y_end = static_cast<int64_t>(pos_y + radius) + 1;
  int64_t index_x = static_cast<int64_t>(pos_x);
  int64_t index_y = static_cast<int64_t>(pos_y);
  radius *= radius;
  for (int64_t j = index_y_beg; j < index_y_end; ++j) {
    for (int64_t i = index_x_beg; i < index_x_end; ++i) {
      bool inside = true;
      if (i < index_x) {
        if (j < index_y) {
          inside = pgeom::getDistanceSqr(i + 1 - pos_x, j + 1 - pos_y) <= radius;
        } else if (j > index_y) {
          inside = pgeom::getDistanceSqr(i + 1 - pos_x, j - pos_y) <= radius;
        } else {
          inside = pgeom::getDistanceSqr(i + 1 - pos_x, (position_data_t) 0) <= radius;
        }
      } else if (i > index_x) {
        if (j < index_y) {
          inside = pgeom::getDistanceSqr(i - pos_x, j + 1 - pos_y) <= radius;
        } else if (j > index_y) {
          inside = pgeom::getDistanceSqr(i - pos_x, j - pos_y) <= radius;
        } else {
          inside = pgeom::getDistanceSqr(i - pos_x, (position_data_t) 0) <= radius;
        }
      } else if (j < index_y) {
        inside = pgeom::getDistanceSqr((position_data_t) 0, j + 1 - pos_y) <= radius;
      } else if (j > index_y) {
        inside = pgeom::getDistanceSqr((position_data_t) 0, j - pos_y) <= radius;
      }
      if (inside) {
        const std::vector<int64_t>* vec;
        if (i < 0 || i >= size_x || j < 0 || j >= size_y) {
          auto it = abnormal_indices.find({i, j});
          if (it != abnormal_indices.end()) {
            vec = &it->second;
          } else {
            vec = nullptr;
          }
        } else {
          vec = &indices[j][i];
        }
        if (vec) {
          result.insert(result.end(), vec->begin(), vec->end());
        }
      }
    }
  }
  return result;
}

std::string SpatialIndexGrid::debugInfo() {
  std::stringstream ss;
  ss<<"Normal cells (from "<<min_x*width<<", "<<min_y*width<<" to "<<(min_x+size_x)*width<<", "<<(min_y+size_y)*width<<"):\n";
  size_t nrow = 0;
  for (auto& row : indices) {
    size_t ncol = 0;
    for (auto& col : row) {
      if (!col.empty()) {
        ss<<" cell "<<(min_x+ncol)*width<<", "<<(min_y+nrow)*width<<":";
        for (auto ind : col) {
          ss<<" "<<ind;
        }
        ss<<"\n";
      }
      ncol++;
    }
    nrow++;
  }
  ss<<"Abnormal cells:\n";
  for (auto& cell : abnormal_indices) {
    ss<<" cell "<<min_x+cell.first.second*width<<", "<<min_y+cell.first.first*width<<":";
    for (auto ind : cell.second) {
      ss<<" "<<ind;
    }
    ss<<"\n";
  }
  return ss.str();
}

SpatialIndexGrid::AdjacentRadius::Iterator SpatialIndexGrid::AdjacentRadius::begin() const {
  SpatialIndexGrid::AdjacentRadius::Iterator it;
  it.radius = radius / grid->width;
  it.pos_x = pos_x / grid->width - grid->min_x;
  it.pos_y = pos_y / grid->width - grid->min_y;
  it.index_x_beg = static_cast<int64_t>(it.pos_x - it.radius);
  it.index_y_beg = static_cast<int64_t>(it.pos_y - it.radius);
  it.index_x_end = static_cast<int64_t>(it.pos_x + it.radius) + 1;
  it.index_y_end = static_cast<int64_t>(it.pos_y + it.radius) + 1;
  it.index_x = static_cast<int64_t>(it.pos_x);
  it.index_y = static_cast<int64_t>(it.pos_y);
  it.i = it.index_x_end - 1;
  it.j = it.index_y_beg - 1;
  it.radius *= it.radius;
  it.grid = grid;
  it.currently_vec = false;
  if (it.findNextVec()) {
    it.currently_vec = true;
    it.curr_val = *it.cur;
    ++it.cur;
    if (it.cur == it.end) {
      it.currently_vec = false;
    }
  }
  return it;
}

SpatialIndexGrid::AdjacentRadius::Iterator SpatialIndexGrid::AdjacentRadius::end() const {
  SpatialIndexGrid::AdjacentRadius::Iterator it;
  it.radius = radius / grid->width;
  it.pos_y = pos_y / grid->width - grid->min_y;
  it.j = static_cast<int64_t>(it.pos_y + it.radius) + 1;
  return it;
}

SpatialIndexGrid::AdjacentRadius::AdjacentRadius(const SpatialIndexGrid* grid, position_data_t pos_x,
                                                 position_data_t pos_y, position_data_t radius)
    : grid(grid),
      pos_x(pos_x),
      pos_y(pos_y),
      radius(radius) {
}

SpatialIndexGrid::AdjacentRadius::Iterator& SpatialIndexGrid::AdjacentRadius::Iterator::operator++() {
  if (currently_vec) {
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  } else {
    if (!findNextVec()) {
      return *this;
    }
    currently_vec = true;
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  }
  return *this;
}

bool SpatialIndexGrid::AdjacentRadius::Iterator::findNextVec() {
  vec = nullptr;
  while (!vec) {
    ++i;
    if (i == index_x_end) {
      i = index_x_beg;
      ++j;
      if (j == index_y_end) {
        return false;
      }
    }
    bool inside = true;
    if (i < index_x) {
      if (j < index_y) {
        inside = pgeom::getDistanceSqr(i + 1 - pos_x, j + 1 - pos_y) <= radius;
      } else if (j > index_y) {
        inside = pgeom::getDistanceSqr(i + 1 - pos_x, j - pos_y) <= radius;
      } else {
        inside = pgeom::getDistanceSqr(i + 1 - pos_x, (position_data_t) 0) <= radius;
      }
    } else if (i > index_x) {
      if (j < index_y) {
        inside = pgeom::getDistanceSqr(i - pos_x, j + 1 - pos_y) <= radius;
      } else if (j > index_y) {
        inside = pgeom::getDistanceSqr(i - pos_x, j - pos_y) <= radius;
      } else {
        inside = pgeom::getDistanceSqr(i - pos_x, (position_data_t) 0) <= radius;
      }
    } else if (j < index_y) {
      inside = pgeom::getDistanceSqr((position_data_t) 0, j + 1 - pos_y) <= radius;
    } else if (j > index_y) {
      inside = pgeom::getDistanceSqr((position_data_t) 0, j - pos_y) <= radius;
    }
    if (inside) {
      if (i < 0 || i >= grid->size_x || j < 0 || j >= grid->size_y) {
        auto it = grid->abnormal_indices.find({i, j});
        if (it != grid->abnormal_indices.end()) {
          vec = &it->second;
        }
      } else {
        vec = &grid->indices[j][i];
      }
    }
    if (vec) {
      cur = vec->begin();
      end = vec->end();
      if (cur == end) {
        vec = nullptr;
      }
    }
  }
  return true;
}

SpatialIndexGrid::AdjacentRadiusSquare::Iterator SpatialIndexGrid::AdjacentRadiusSquare::begin() const {
  SpatialIndexGrid::AdjacentRadiusSquare::Iterator it;
  int64_t cell_radius = static_cast<int64_t>(radius/grid->width + 1);
  it.index_x_beg = static_cast<int64_t>(pos_x / grid->width) - grid->min_x;
  it.index_y_beg = static_cast<int64_t>(pos_y / grid->width) - grid->min_y;
  it.index_x_end = it.index_x_beg + cell_radius + 1;
  it.index_y_end = it.index_y_beg + cell_radius + 1;
  it.index_x_beg -= cell_radius;
  it.index_y_beg -= cell_radius;
  it.i = it.index_x_end - 1;
  it.j = it.index_y_beg - 1;
  it.grid = grid;
  it.currently_vec = false;
  if (it.findNextVec()) {
    it.currently_vec = true;
    it.curr_val = *it.cur;
    ++it.cur;
    if (it.cur == it.end) {
      it.currently_vec = false;
    }
  }
  return it;
}

SpatialIndexGrid::AdjacentRadiusSquare::Iterator SpatialIndexGrid::AdjacentRadiusSquare::end() const {
  SpatialIndexGrid::AdjacentRadiusSquare::Iterator it;
  int64_t cell_radius = static_cast<int64_t>(radius/grid->width + 1);
  it.j = static_cast<int64_t>(pos_y / grid->width) - grid->min_y + cell_radius + 1;
  return it;
}

SpatialIndexGrid::AdjacentRadiusSquare::AdjacentRadiusSquare(const SpatialIndexGrid* grid, position_data_t pos_x,
                                                             position_data_t pos_y, position_data_t radius)
    : grid(grid),
      pos_x(pos_x),
      pos_y(pos_y),
      radius(radius) {
}

SpatialIndexGrid::AdjacentRadiusSquare::Iterator& SpatialIndexGrid::AdjacentRadiusSquare::Iterator::operator++() {
  if (currently_vec) {
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  } else {
    if (!findNextVec()) {
      return *this;
    }
    currently_vec = true;
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  }
  return *this;
}

bool SpatialIndexGrid::AdjacentRadiusSquare::Iterator::findNextVec() {
  vec = nullptr;
  while (!vec) {
    ++i;
    if (i == index_x_end) {
      i = index_x_beg;
      ++j;
      if (j == index_y_end) {
        return false;
      }
    }
    if (i < 0 || i >= grid->size_x || j < 0 || j >= grid->size_y) {
      auto it = grid->abnormal_indices.find({i, j});
      if (it != grid->abnormal_indices.end()) {
        vec = &it->second;
      }
    } else {
      vec = &grid->indices[j][i];
    }
    if (vec) {
      cur = vec->begin();
      end = vec->end();
      if (cur == end) {
        vec = nullptr;
      }
    }
  }
  return true;
}

SpatialIndexGrid::OutwardsOuter::Iterator SpatialIndexGrid::OutwardsOuter::begin() const {
  SpatialIndexGrid::OutwardsOuter::Iterator it;
  it.max_radius = std::max(static_cast<int64_t>(outer_radius/grid->width+1) + 2, (int64_t) 1);
  it.curr_radius = std::max(std::min(static_cast<int64_t>(inner_radius/grid->width + 1) + 1, it.max_radius), (int64_t) 0);
  it.index_x = static_cast<int64_t>(pos_x / grid->width - grid->min_x);
  it.index_y = static_cast<int64_t>(pos_y / grid->width - grid->min_y);
  it.curr_rotation = it.curr_radius == 0 ? 2 : 3;
  it.curr_radius--;
  it.curr_offset = it.curr_radius;
  it.grid = grid;
  it.currently_vec = false;
  if (it.findNextVec()) {
    it.currently_vec = true;
    it.curr_val = *it.cur;
    ++it.cur;
    if (it.cur == it.end) {
      it.currently_vec = false;
    }
  }
  return it;
}

SpatialIndexGrid::OutwardsOuter::Iterator SpatialIndexGrid::OutwardsOuter::end() const {
  SpatialIndexGrid::OutwardsOuter::Iterator it;
  it.curr_radius = std::max(static_cast<int64_t>(outer_radius/grid->width+1) + 2, (int64_t) 1);
  return it;
}

SpatialIndexGrid::OutwardsOuter::OutwardsOuter(const SpatialIndexGrid* grid,
                                           position_data_t pos_x, position_data_t pos_y,
                                           position_data_t inner_radius, position_data_t outer_radius)
    : grid(grid),
      pos_x(pos_x), pos_y(pos_y),
      inner_radius(inner_radius), outer_radius(outer_radius) {
}

SpatialIndexGrid::OutwardsOuter::Iterator& SpatialIndexGrid::OutwardsOuter::Iterator::operator++() {
  if (currently_vec) {
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  } else {
    if (!findNextVec()) {
      return *this;
    }
    currently_vec = true;
    curr_val = *cur;
    ++cur;
    if (cur == end) {
      currently_vec = false;
    }
  }
  return *this;
}

bool SpatialIndexGrid::OutwardsOuter::Iterator::findNextVec() {
  vec = nullptr;
  while (!vec) {
    if (curr_rotation < 3) {
      curr_rotation++;
    } else {
      curr_rotation = 0;
      if (curr_offset == curr_radius) {
        curr_offset = 0;
        curr_radius++;
        if (curr_radius == max_radius) {
          return false;
        }
      } else if (curr_offset > 0) {
        curr_offset = -curr_offset;
      } else {
        curr_offset = -curr_offset + 1;
      }
    }
    int64_t i = index_x;
    int64_t j = index_y;
    switch(curr_rotation) {
      case 0: {
        i += curr_radius;
        j += curr_offset;
      } break;
      case 1: {
        i -= curr_offset;
        j += curr_radius;
      } break;
      case 2: {
        i -= curr_radius;
        j -= curr_offset;
      } break;
      case 3: {
        i += curr_offset;
        j -= curr_radius;
      } break;
    }
    if (i < 0 || i >= grid->size_x || j < 0 || j >= grid->size_y) {
      auto it = grid->abnormal_indices.find({i, j});
      if (it != grid->abnormal_indices.end()) {
        vec = &it->second;
      }
    } else {
      vec = &grid->indices[j][i];
    }
    if (vec) {
      cur = vec->begin();
      end = vec->end();
      if (cur == end) {
        vec = nullptr;
      }
    }
  }
  return true;
}
