//
// Created by aleksander on 11.07.18.
//

#include <iostream>
#define _USE_MATH_DEFINES // Visual xd
#include <cmath>
#include <util/PlanarGeometryUtil.h>
#include "EntitySystem.h"

EntitySystem::EntitySystem(position_data_t grid_width, position_data_t grid_min_x, position_data_t grid_min_y,
                           position_data_t grid_size_x, position_data_t grid_size_y)
    : spatial_index(grid_width, grid_min_x, grid_min_y, grid_size_x, grid_size_y) {}

EntitySystem::EntitySystem(EntitySystem &&other) noexcept {
  spatial_index = std::move(other.spatial_index);
  entities = std::move(other.entities);
  deleted_indices = std::move(other.deleted_indices);
  deletion_queue = std::move(other.deletion_queue);
  for (auto& e : entities) {
    e->system = this;
  }
}

EntitySystem &EntitySystem::operator=(EntitySystem &&other) noexcept {
  spatial_index = std::move(other.spatial_index);
  entities = std::move(other.entities);
  deleted_indices = std::move(other.deleted_indices);
  deletion_queue = std::move(other.deletion_queue);
  for (auto& e : entities) {
    e->system = this;
  }
  return *this;
}

void EntitySystem::addEntity(std::unique_ptr<Entity> entity) {
  if (!entity) {
    std::cout<<"Attempted to add a null entity"<<std::endl;
    return;
  }
  entity->system = this;
  int64_t target_index = static_cast<int64_t>(entities.size());
  if (deleted_indices.empty()) {
    entities.emplace_back();
  } else {
    auto it = deleted_indices.begin();
    target_index = *it;
    deleted_indices.erase(it);
  }
  spatial_index.emplace(target_index, entity->pos_x, entity->pos_y);
  entity->init();
  entities[target_index] = std::move(entity);
}

void EntitySystem::removeEntity(int64_t index) {
  deletion_queue.push_back(index);
}

void EntitySystem::doRemoveEntity(int64_t index) {
  int64_t end_index = entities.size();
  if (index >= end_index) {
    std::cout<<"Attempted delete entity with index out of range: "<<index<<std::endl;
    return;
  }
  Entity* deleted_entity = entities[index].get();
  if (!deleted_entity) {
    std::cout<<"Attempted delete a null entity: "<<index<<std::endl;
    return;
  }
  spatial_index.remove(index, entities[index]->pos_x, entities[index]->pos_y);
  entities[index].reset();
  deleted_indices.emplace(index);
}

void EntitySystem::update() {
  update1_Entities();
  update2_Index();
  update3_Delete();
}

void EntitySystem::update1_Entities() {
  int64_t end_index = entities.size();
  for (int64_t i = 0; i < end_index; ++i) {
    if (entities[i]) {
      position_data_t old_pos_x = entities[i]->pos_x;
      position_data_t old_pos_y = entities[i]->pos_y;
      entities[i]->update();
      entities[i]->pos_x = old_pos_x;
      entities[i]->pos_y = old_pos_y;
    }
  }
}

void EntitySystem::update2_Index() {
  int64_t end_index = entities.size();
  for (int64_t i = 0; i < end_index; ++i) {
    if (entities[i]) {
      if (std::isnormal(entities[i]->new_pos_x) && std::isnormal(entities[i]->new_pos_y)) {
        spatial_index.update(i, entities[i]->new_pos_x, entities[i]->new_pos_y, entities[i]->pos_x, entities[i]->pos_y);
        entities[i]->pos_x = entities[i]->new_pos_x;
        entities[i]->pos_y = entities[i]->new_pos_y;
      } else {
        std::cout<<"nan or inf detected\n";
      }
    }
  }
}

void EntitySystem::update3_Delete() {
  for (int64_t index : deletion_queue) {
    doRemoveEntity(index);
  }
  size_t end_index = entities.size();
  for (size_t i = 0; i < end_index; ++i) {
    if (entities[i]) {
      for (Entity* e: forgetting_queue) {
        entities[i]->forgetAbout(e);
      }
    }
  }
  forgetting_queue.clear();
  deletion_queue.clear();
}

void EntitySystem::doCollideEach() {
  size_t end_index = entities.size();
  for (size_t i = 0; i < end_index; ++i) {
    Entity* e = entities[i].get();
    if (e) {
      for (int64_t index : adjacentRadius(e->pos_x, e->pos_y, 3)) {
        if (i < index) {
          Entity* e2 = entities[index].get();
          if (e->colllidesWith(*e2)) {
            e->ellasticCollision(*e2);
            e->onEntityCollision(*e2);
            e2->onEntityCollision(*e);
          }
        }
      }
    }
  }
}

void EntitySystem::doCollideEachWith(EntitySystem &other) {
  size_t end_index = entities.size();
  for (size_t i = 0; i < end_index; ++i) {
    Entity* e = entities[i].get();
    if (e) {
      for (int64_t index : other.adjacentRadius(e->pos_x, e->pos_y, 3)) {
        //std::cout<<"This system:\n"<<spatial_index.debugInfo();
        //std::cout<<"Other system:\n"<<other.spatial_index.debugInfo();
        //std::cout<<index<<"\n";
        Entity* e2 = other.entities[index].get();
        //std::cout<<e2->pos_x<<" "<<e2->pos_y<<" "<<e2->vel_x<<" "<<e2->vel_y<<"\n";
        if (e->colllidesWith(*e2)) {
          e->ellasticCollision(*e2);
          e->onEntityCollision(*e2);
          e2->onEntityCollision(*e);
        }
      }
    }
  }
}

void EntitySystem::forgetAbout(Entity *e) {
  forgetting_queue.emplace_back(e);
}

SpatialIndexGrid::Adjacent EntitySystem::adjacent(position_data_t pos_x, position_data_t pos_y) const {
  return {&spatial_index, pos_x, pos_y};
}

SpatialIndexGrid::AdjacentRadius
EntitySystem::adjacentRadius(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const {
  return {&spatial_index, pos_x, pos_y, radius};
}

SpatialIndexGrid::AdjacentRadiusSquare
EntitySystem::adjacentRadiusSquare(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const {
  return {&spatial_index, pos_x, pos_y, radius};
}

SpatialIndexGrid::OutwardsOuter
EntitySystem::outwardsOuter(position_data_t pos_x, position_data_t pos_y, position_data_t inner_radius,
                                  position_data_t outer_radius) const {
  return {&spatial_index, pos_x, pos_y, inner_radius, outer_radius};
}

inline bool sortedFromNearestSquareComp(const std::pair<int64_t, position_data_t>& first,
                                        const std::pair<int64_t, position_data_t>& second) {
  return first.second < second.second;
}

EntitySystem::OutwardsInner
EntitySystem::outwardsInnerSorted(position_data_t pos_x, position_data_t pos_y, position_data_t inner_radius) const {
  return {this, pos_x, pos_y, inner_radius};
}

EntitySystem::OutwardsInner::OutwardsInner(const EntitySystem* system, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t inner_radius)
    : vec(new std::vector<std::pair<int64_t, position_data_t>>){
  for (int64_t index : SpatialIndexGrid::AdjacentRadiusSquare(&system->spatial_index, pos_x, pos_y, inner_radius)) {
    vec->emplace_back(index, pgeom::getDistanceSqr(pos_x - system->entities[index]->pos_x,
                                                   pos_y - system->entities[index]->pos_y));
  }
  std::sort(vec->begin(), vec->end(), sortedFromNearestSquareComp);
}

EntitySystem::OutwardsInner::Iterator EntitySystem::OutwardsInner::begin() const {
  OutwardsInner::Iterator it;
  it.vec = vec;
  it.curr = vec->begin();
  return it;
}

EntitySystem::OutwardsInner::Iterator EntitySystem::OutwardsInner::end() const {
  OutwardsInner::Iterator it;
  it.curr = vec->end();
  return it;
}

EntitySystem::All EntitySystem::all() const {
  return {(int64_t) entities.size()};
}

EntitySystem::All::All(int64_t num_entities)
    : num_entities(num_entities) {}

EntitySystem::All::Iterator EntitySystem::All::begin() const {
  EntitySystem::All::Iterator result;
  result.curr = 0;
  return result;
}

EntitySystem::All::Iterator EntitySystem::All::end() const {
  EntitySystem::All::Iterator result;
  result.curr = num_entities;
  return result;
}

std::unique_ptr<EntitySystem::DisplayData> EntitySystem::outputDisplayData() {
  size_t entity_count = entities.size();
  std::vector<std::unique_ptr<Entity::DisplayData>> result(entity_count);
  for (size_t i = 0; i < entity_count; ++i) {
    if (entities[i]) {
      result[i] = entities[i]->outputDisplayData();
    }
  }
  auto result_esdd = std::make_unique<EntitySystem::DisplayData>();
  result_esdd->entities = std::move(result);
  return result_esdd;
}

void EntitySystem::DisplayData::display(sf::RenderWindow& window, const EntitySystem::DisplayData& previous_frame,
                                        double interpolation_fraction) {
  const std::vector<std::unique_ptr<Entity::DisplayData>>& previous = previous_frame.entities;
  position_data_t frac2 = static_cast<position_data_t>(1 - interpolation_fraction);
  size_t prev_size = previous.size();
  for (size_t ent_size = entities.size(), i = 0; i < ent_size; ++i) {
    if (entities[i]) {
      int64_t id = entities[i]->entity_id;
      position_data_t x = entities[i]->pos_x;
      position_data_t y = entities[i]->pos_y;
      position_data_t angle = entities[i]->angle;
      size_t other_index = i;
      if (prev_size <= i || previous[i]->entity_id != id) {
        for (size_t j = 0; j < prev_size; ++j) {
          if (previous[j]->entity_id == id) {
            other_index = j;
            break;
          }
        }
        if (other_index != i) {
          other_index = ent_size;
        }
      }
      if (other_index < ent_size) {
        if (std::fabs(x - previous[other_index]->pos_x) > 0.0001) {
          x *= interpolation_fraction;
          x += frac2 * previous[other_index]->pos_x;
        }
        if (std::fabs(y - previous[other_index]->pos_y) > 0.0001) {
          y *= interpolation_fraction;
          y += frac2 * previous[other_index]->pos_y;
        }
        position_data_t temp = angle - previous[other_index]->angle;
        if (std::fabs(temp) > 0.0001) {
          if (temp > M_PI) {
            angle -= ((int) (temp / M_PI)) * M_PI;
          } else if (temp < -M_PI) {
            angle += ((int) (-temp / M_PI)) * M_PI;
          }
          angle *= interpolation_fraction;
          angle += frac2 * previous[other_index]->angle;
        }
      }
      entities[i]->display(window, x, y, angle);
    }
  }
}
