//
// Created by aleksander on 11.07.18.
//

#include <iostream>
#include <cmath>
#include "Entity.h"
#include "EntitySystem.h"

int Entity::next_id = 0;

Entity::Entity(): pos_x(0), pos_y(0), vel_x(0), vel_y(0), collision_radius(1) {
  unique_id = next_id;
  ++next_id;
}

Entity::DisplayData::DisplayData(const Entity& e)
    : entity_id(e.unique_id),
      pos_x(e.pos_x),
      pos_y(e.pos_y),
      angle(e.angle) {
}

void Entity::setPos(position_data_t x, position_data_t y) {
  pos_x = x;
  pos_y = y;
}

void Entity::init() {

}

void Entity::update() {
  new_pos_x = pos_x + vel_x;
  new_pos_y = pos_y + vel_y;
  if (vel_x != 0 && vel_y != 0) {
    angle = std::atan2(vel_y, vel_x);
  }
}

void Entity::onEntityCollision(Entity &other) {

}

bool Entity::colllidesWith(const Entity &other) const {
  position_data_t r_sum = collision_radius + other.collision_radius;
  position_data_t dx = pos_x - other.pos_x;
  position_data_t dy = pos_y - other.pos_y;
  return dx*dx + dy*dy < r_sum*r_sum;
}

bool Entity::colllidesWithPoint(position_data_t x, position_data_t y) const {
  position_data_t dx = pos_x - x;
  position_data_t dy = pos_y - y;
  return dx*dx + dy*dy < collision_radius*collision_radius;
}

void Entity::ellasticCollision(Entity& other) {
  // https://www.plasmaphysics.org.uk/programs/coll2d_cpp.htm
  double m21,dvx2,a,x21,y21,vx21,vy21,fy21,sign;
  m21 = 1;
  x21 = other.pos_x - pos_x;
  y21 = other.pos_y - pos_y;
  vx21 = other.vel_x - vel_x;
  vy21 = other.vel_y - vel_y;
  if (std::sqrt(x21*x21 + y21*y21) > collision_radius + other.collision_radius || (vx21*x21 + vy21*y21) >= 0) {
    return;
  }
  fy21 = 1.0e-12 * std::abs(y21);
  if (std::abs(x21) < fy21) {
    if (x21 < 0) { sign=-1; } else { sign=1;}
    x21 = fy21 * sign;
  }
  a = y21 / x21;
  dvx2 = -2 * (vx21 + a*vy21) / ((1 + a*a) * (1 + m21));
  other.vel_x += dvx2;
  other.vel_y += a*dvx2;
  vel_x -= m21*dvx2;
  vel_y -= a*m21*dvx2;
}
void Entity::inellasticCollision(Entity &other) {
    // https://www.plasmaphysics.org.uk/programs/coll2d_cpp.htm
    double m21,dvx2,a,x21,y21,vx21,vy21,fy21,sign,vx_cm,vy_cm;
    m21 = 1;
    x21 = other.pos_x - pos_x;
    y21 = other.pos_y - pos_y;
    vx21 = other.vel_x - vel_x;
    vy21 = other.vel_y - vel_y;

    vx_cm = (1*vel_x+1*other.vel_x)/(1+1) ;
    vy_cm = (1*vel_y+1*other.vel_x)/(1+1);
    if (std::sqrt(x21*x21 + y21*y21) > collision_radius + other.collision_radius || (vx21*x21 + vy21*y21) >= 0) {
        return;
    }
    fy21 = 1.0e-12 * std::abs(y21);
    if (std::abs(x21) < fy21) {
        if (x21 < 0) { sign=-1; } else { sign=1;}
        x21 = fy21 * sign;
    }

    a = y21 / x21;
    dvx2 = -2 * (vx21 + a*vy21) / ((1 + a*a) * (1 + m21));
    other.vel_x += dvx2;
    other.vel_y += a*dvx2;
    vel_x -= m21*dvx2;
    vel_y -= a*m21*dvx2;

    vel_x=(vel_x-vx_cm)*0.5 + vx_cm;
    vel_y=(vel_y-vy_cm)*0.5 + vy_cm;
    other.vel_x=(other.vel_x-vx_cm)*1+ vx_cm;
    other.vel_y =(other.vel_y -vy_cm)*1 + vy_cm;

    return;
}


void Entity::setCollisionRadius(position_data_t r) {
  collision_radius = r;
}

void Entity::removeSelf() {
  size_t index = static_cast<size_t>(-1);
  for (size_t i = 0, len = system->entities.size(); i < len; ++i) {
    if (system->entities[i].get() == this) {
      index = i;
    }
  }
  if (index != static_cast<size_t>(-1)) {
    system->removeEntity(index);
  }
}

std::unique_ptr<Entity::DisplayData> Entity::outputDisplayData() {
  return nullptr;
}

void Entity::forgetAbout(Entity *e) {

}
