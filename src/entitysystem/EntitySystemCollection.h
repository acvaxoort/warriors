//
// Created by aleksander on 23.05.19.
//

#ifndef WARRIORS_ENTITYSYSTEMCOLLECTION_H
#define WARRIORS_ENTITYSYSTEMCOLLECTION_H

#include "EntitySystem.h"

class EntitySystemCollection {
public:
  class DisplayData {
  public:
    void display(sf::RenderWindow& window, const EntitySystemCollection::DisplayData& previous_frame,
                 double interpolation_fraction);
  private:
    std::vector<std::unique_ptr<EntitySystem::DisplayData>> subsystems;
    friend class EntitySystemCollection;
  };

  explicit EntitySystemCollection(position_data_t grid_width = 10, position_data_t grid_min_x = -128,
      position_data_t grid_min_y = -128, position_data_t grid_size_x = 256, position_data_t grid_size_y = 256);

  void addEntity(size_t subsystem, std::unique_ptr<Entity> entity);
  void removeEntity(size_t subsystem, int64_t index);
  void forgetAbout(Entity* e);
  void update();
  void doCollideEach();
  std::unique_ptr<EntitySystemCollection::DisplayData> outputDisplayData();

  template <class SystemIterator>
  class CollectionIteratorWrapper;

  //Iterators for whole collection, with the same names as iterators of EntitySystem, in 3 groups:
  // - without suffix - over whole collection
  // - In - in one subsystem
  // - Except - in all subsystems except one

  inline CollectionIteratorWrapper<SpatialIndexGrid::Adjacent>
  adjacent(position_data_t pos_x, position_data_t pos_y) const {
    CollectionIteratorWrapper<SpatialIndexGrid::Adjacent> result;
    for (auto& sub : subsystems) {
      result.collection.emplace_back(sub.adjacent(pos_x, pos_y), &sub);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius>
  adjacentRadius(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius> result;
    for (auto& sub : subsystems) {
      result.collection.emplace_back(sub.adjacentRadius(pos_x, pos_y, radius), &sub);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare>
  adjacentRadiusSquare(position_data_t pos_x, position_data_t pos_y,
                                               position_data_t radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare> result;
    for (auto& sub : subsystems) {
      result.collection.emplace_back(sub.adjacentRadiusSquare(pos_x, pos_y, radius), &sub);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter>
  outwardsOuter(position_data_t pos_x, position_data_t pos_y, position_data_t inner_radius,
                                        position_data_t outer_radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter> result;
    for (auto& sub : subsystems) {
      result.collection.emplace_back(sub.outwardsOuter(pos_x, pos_y, inner_radius, outer_radius), &sub);
    }
    return result;
  }

  inline CollectionIteratorWrapper<EntitySystem::OutwardsInner>
  outwardsInnerSorted(position_data_t pos_x, position_data_t pos_y,
                                              position_data_t inner_radius) const {
    CollectionIteratorWrapper<EntitySystem::OutwardsInner> result;
    for (auto& sub : subsystems) {
      result.collection.emplace_back(sub.outwardsInnerSorted(pos_x, pos_y, inner_radius), &sub);
    }
    return result;
  }

  inline CollectionIteratorWrapper<EntitySystem::All> all() const {
    CollectionIteratorWrapper<EntitySystem::All> result;
    for (auto& sub : subsystems) {
      result.collection.emplace_back(sub.all(), &sub);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::Adjacent>
  adjacentIn(size_t subsystem, position_data_t pos_x, position_data_t pos_y) const {
    CollectionIteratorWrapper<SpatialIndexGrid::Adjacent> result;
    if (subsystem < subsystems.size()) {
      result.collection.emplace_back(subsystems[subsystem].adjacent(pos_x, pos_y), &subsystems[subsystem]);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius>
  adjacentRadiusIn(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius> result;
    if (subsystem < subsystems.size()) {
      result.collection.emplace_back(subsystems[subsystem].adjacentRadius(pos_x, pos_y, radius), &subsystems[subsystem]);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare>
  adjacentRadiusSquareIn(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                                 position_data_t radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare> result;
    if (subsystem < subsystems.size()) {
      result.collection.emplace_back(subsystems[subsystem].adjacentRadiusSquare(pos_x, pos_y, radius),
                                     &subsystems[subsystem]);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter>
  outwardsOuterIn(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                          position_data_t inner_radius, position_data_t outer_radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter> result;
    if (subsystem < subsystems.size()) {
      result.collection.emplace_back(subsystems[subsystem].outwardsOuter(pos_x, pos_y, inner_radius, outer_radius),
                                     &subsystems[subsystem]);
    }
    return result;
  }

  inline CollectionIteratorWrapper<EntitySystem::OutwardsInner>
  outwardsInnerSortedIn(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                                position_data_t inner_radius) const {
    CollectionIteratorWrapper<EntitySystem::OutwardsInner> result;
    if (subsystem < subsystems.size()) {
      result.collection.emplace_back(subsystems[subsystem].outwardsInnerSorted(pos_x, pos_y, inner_radius),
                                     &subsystems[subsystem]);
    }
    return result;
  }

  inline CollectionIteratorWrapper<EntitySystem::All>
  allIn(size_t subsystem) const {
    CollectionIteratorWrapper<EntitySystem::All> result;
    if (subsystem < subsystems.size()) {
      result.collection.emplace_back(subsystems[subsystem].all(),
                                     &subsystems[subsystem]);
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::Adjacent>
  adjacentExcept(size_t subsystem, position_data_t pos_x, position_data_t pos_y) const {
    CollectionIteratorWrapper<SpatialIndexGrid::Adjacent> result;
    size_t subs = subsystems.size();
    for (size_t i = 0; i < subs; ++i) {
      if (i != subsystem) {
        result.collection.emplace_back(subsystems[i].adjacent(pos_x, pos_y), &subsystems[i]);
      }
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius>
  adjacentRadiusExcept(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                               position_data_t radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius> result;
    size_t subs = subsystems.size();
    for (size_t i = 0; i < subs; ++i) {
      if (i != subsystem) {
        result.collection.emplace_back(subsystems[i].adjacentRadius(pos_x, pos_y, radius), &subsystems[i]);
      }
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare>
  adjacentRadiusSquareExcept(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                                     position_data_t radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare> result;
    size_t subs = subsystems.size();
    for (size_t i = 0; i < subs; ++i) {
      if (i != subsystem) {
        result.collection.emplace_back(subsystems[i].adjacentRadiusSquare(pos_x, pos_y, radius), &subsystems[i]);
      }
    }
    return result;
  }

  inline CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter>
  outwardsOuterExcept(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                              position_data_t inner_radius, position_data_t outer_radius) const {
    CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter> result;
    size_t subs = subsystems.size();
    for (size_t i = 0; i < subs; ++i) {
      if (i != subsystem) {
        result.collection.emplace_back(subsystems[i].outwardsOuter(pos_x, pos_y, inner_radius, outer_radius),
                                       &subsystems[i]);
      }
    }
    return result;
  }

  inline CollectionIteratorWrapper<EntitySystem::OutwardsInner>
  outwardsInnerSortedExcept(size_t subsystem, position_data_t pos_x, position_data_t pos_y,
                                                    position_data_t inner_radius) const {
    CollectionIteratorWrapper<EntitySystem::OutwardsInner> result;
    size_t subs = subsystems.size();
    for (size_t i = 0; i < subs; ++i) {
      if (i != subsystem) {
        result.collection.emplace_back(subsystems[i].outwardsInnerSorted(pos_x, pos_y, inner_radius), &subsystems[i]);
      }
    }
    return result;
  }

  inline CollectionIteratorWrapper<EntitySystem::All>
  allExcept(size_t subsystem) const {
    CollectionIteratorWrapper<EntitySystem::All> result;
    size_t subs = subsystems.size();
    for (size_t i = 0; i < subs; ++i) {
      if (i != subsystem) {
        result.collection.emplace_back(subsystems[i].all(), &subsystems[i]);
      }
    }
    return result;
  }

  template <class SystemIterator>
  class CollectionIteratorWrapper {
  public:
    class Iterator {
    public:
      inline Entity* operator*() const { return sys_curr->second->entities[*inner_curr].get(); }
      inline bool operator!=(const Iterator& other) { return sys_curr != other.sys_curr; }
      inline Iterator& operator++() {
        ++inner_curr;
        tryAdvanceSystem();
        return *this;
      }
    private:
      inline void tryAdvanceSystem() {
        while (!(inner_curr != inner_end)) {
          ++sys_curr;
          if (sys_curr == sys_end) {
            return;
          }
          inner_curr = sys_curr->first.begin();
          inner_end = sys_curr->first.end();
        }
      }
      typename std::vector<std::pair<SystemIterator, const EntitySystem*>>::const_iterator sys_curr;
      typename std::vector<std::pair<SystemIterator, const EntitySystem*>>::const_iterator sys_end;
      typename SystemIterator::Iterator inner_curr;
      typename SystemIterator::Iterator inner_end;
      friend class CollectionIteratorWrapper;
    };
    Iterator begin() const {
      Iterator result;
      result.sys_curr = collection.begin();
      result.sys_end = collection.end();
      result.inner_curr = result.sys_curr->first.begin();
      result.inner_end = result.sys_curr->first.end();
      result.tryAdvanceSystem();
      return result;
    }
    Iterator end() const {
      Iterator result;
      result.sys_curr = collection.end();
      result.sys_end = collection.end();
      return result;
    }
  private:
    std::vector<std::pair<SystemIterator, const EntitySystem*>> collection;
    friend class EntitySystemCollection;
  };


public:
  std::vector<EntitySystem> subsystems;

  position_data_t grid_width;
  position_data_t grid_min_x;
  position_data_t grid_min_y;
  position_data_t grid_size_x;
  position_data_t grid_size_y;
};


#endif //WARRIORS_ENTITYSYSTEMCOLLECTION_H
