//
// Created by aleksander on 10.07.18.
//

#ifndef WARRIORS_COREINSTANCEWRAPPER_H
#define WARRIORS_COREINSTANCEWRAPPER_H

#include <mutex>
#include <queue>
#include <gamecore/CoreInstance.h>
#include <util/AccumulatedMeasure.hpp>

/**
 * Manages a running CoreInstance, handles synchronization between update and display threads.
 * This class provides a simple interface for GameCore and manages thread-safe independent updates and displays,
 * using CoreInstance and CoreInstance::DisplayData.
 */
class CoreInstanceWrapper {
public:
  explicit CoreInstanceWrapper(std::unique_ptr<CoreInstance> instance);
  ~CoreInstanceWrapper();

  CoreInstanceWrapper(const CoreInstanceWrapper& other) = delete;
  CoreInstanceWrapper& operator=(const CoreInstanceWrapper& other) = delete;

  /**
   * Request instance update.
   */
  void update();

  /**
   * Request display.
   * @param sfml_window context for rendering
   */
  void display(sf::RenderWindow& sfml_window);

  void setUpdateRate(double update_rate) {

  }
private:
  std::unique_ptr<CoreInstance> instance;
  std::unique_ptr<CoreInstance::DisplayData> display_data_current;
  std::unique_ptr<CoreInstance::DisplayData> display_data_previous;
  std::queue<std::unique_ptr<CoreInstance::DisplayData>> display_data_queue;
  sf::Int64 update_timestamp_current;
  sf::Int64 update_timestamp_previous;
  sf::Clock time_regulator;
  std::mutex display_queue_mutex;
  std::mutex update_mutex;
  bool new_instance_udpate;


  AccumulatedMeasure<double> measure_display;
  AccumulatedMeasure<double> measure_synchronize;
  AccumulatedMeasure<double> measure_update;
  AccumulatedMeasure<double> measure_display_lock;
  AccumulatedMeasure<double> measure_synchronize_lock;
  AccumulatedMeasure<double> measure_update_lock;
};

#endif //WARRIORS_COREINSTANCEWRAPPER_H
