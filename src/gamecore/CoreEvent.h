//
// Created by aleksander on 10.07.18.
//

#ifndef WARRIORS_COREEVENT_H
#define WARRIORS_COREEVENT_H

#include <string>
#include <memory>
#include "CoreInstance.h"

/**
 * Events for communicating with game core.
 * Event that can be sent from running instances to game core to regulate things handled by the game core,
 * this is a dirty part of underlying mechanisms, CoreInstance methods are meant to be used.
 */
struct CoreEvent {

  enum EventType {
    SWITCH, DELETE, MAKE, QUIT, FPS_SHOW, UPS_SHOW, FPS_SET, UPS_SET
  };

  explicit CoreEvent(EventType type, const std::string& instance_name = "", std::unique_ptr<CoreInstance> instance = nullptr, double data = 0.0);
  EventType type;
  std::string instance_name;
  CoreInstance* instance;
  double data;
};


#endif //WARRIORS_COREEVENT_H
