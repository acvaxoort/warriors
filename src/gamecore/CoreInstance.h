//
// Created by aleksander on 20.06.18.
//

#ifndef WARRIORS_COREINSTANCE_H
#define WARRIORS_COREINSTANCE_H

#include <SFML/Graphics.hpp>

class GameCore;

/**
 * Abstraction of interactive sections of the game with separate rules of operation.
 * This class provides a base for interactive sections of the game with definable update and display rules.
 */
class CoreInstance {
public:

  /**
   * Contains all data needed to display a frame.
   * Objects of this class are created and passed to display thread from update thread after each update;
   * display thread remembers current and previous state and calls the display function repeatedly,
   * providing fraction which allows smooth movement at high framerate while updating the state relatively rarely.
   */
  class DisplayData {
  public:
    virtual ~DisplayData() = default;

    /**
     * Display the frame with given previous state and fraction.
     * This function should display interpolated state between this and previous state, with provided fraction,
     * it can also be used to extrapolate, by calculating estimated future frame; estimating future state can reduce
     * visible input delay but also show inaccurate estimates that may sometimes be perceived as choppy movement.
     * @param window rendering context
     * @param previous previous frame
     * @param fraction interpolation fraction, will allways be between 0 and 1 inclusive,
     * the value specifies at which point between two states the frame should be rendered, 0 - previous, 1 - current
     */
    virtual void display(sf::RenderWindow& window, const CoreInstance::DisplayData& previous, double fraction) = 0;
  };

  virtual ~CoreInstance() = default;

  /**
   * Switches instance to another.
   * Pushes an event to the game core requesting instance switch, all events are processed after an update finishes.
   * @param name instance to be switched to (identifier)
   */
  void switchToInstance(const std::string &name);

  /**
   * Deletes an instance.
   * Pushes an event to the game core requesting instance deletion, all events are processed after an update finishes.
   * @param name instance to be deleted (identifier)
   */
  void deleteInstance(const std::string &name);

  /**
   * Creates an instance.
   * Pushes an event to the game core requesting instance creation, all events are processed after an update finishes.
   * @param name instance name (identifier)
   * @param instance created instance to put in the GameCore
   */
  void makeInstance(const std::string &name, std::unique_ptr<CoreInstance> instance);

  /**
   * Switches FPS counter on and off.
   * Pushes an event to the game core requesting FPS counter on/off, all events are processed after an update finishes.
   */
  void toggleFPSCounter();

  /**
   * Switches UPS counter on and off.
   * Pushes an event to the game core requesting UPS counter on/off, all events are processed after an update finishes.
   */
  void toggleUPSCounter();

  /**
   * Sets a new FPS cap.
   * Pushes an event to the game core requesting a new FPS cap, all events are processed after an update finishes.
   * @param fps new FPS cap, a nonpositive value means no cap
   */
  void setFPSCap(double fps);

  /**
   * Sets a new UPS cap.
   * Pushes an event to the game core requesting a new update rate, all events are processed after an update finishes.
   * @param ups new UPS cap, a nonpositive value means no cap
   */
  void setUPSCap(double ups);

  /**
   * Quits the game.
   * Pushes an event to the game core requesting the application to exit, all events are processed after an update finishes.
   */
  void quit();

  /**
   * Dirty access to the game window.
   * Can be safely used to access sfml window events, drawing from it may be a bad idea,
   * use CoreInstance::DisplayData for display.
   * @return the game window
   */
  sf::RenderWindow& getWindow();

  /**
   * Overridable game update method.
   * The instance update method, it should process user input, update GUI, menu animations,
   * move the game world forward in time etc.
   */
  virtual void update() = 0;

  /**
   * Create an object that contains all data needed to properly display a frame.
   * It should not contain pointers/references to data that may change during an update.
   * @return the DisplayData object corresponding to current instance state
   */
  virtual std::unique_ptr<DisplayData> outputDisplayData() = 0;

private:
  //So that GameCore can access core attribute directly
  friend class GameCore;
  GameCore* core = nullptr;
};


#endif //WARRIORS_COREINSTANCE_H
