//
// Created by aleksander on 17.06.18.
//

#ifndef WARRIORS_GAMECORE_H
#define WARRIORS_GAMECORE_H


#include <map>
#include <memory>
#include <thread>
#include <mutex>
#include <queue>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <gamecore/CoreEvent.h>
#include <gamecore/CoreInstanceWrapper.h>
#include <util/PeriodicExecutionManager.h>

/**
 * Core of the game engine.
 * This class runs the main loop (game update loop) and the display loop in another thread,
 * tries to maintain stable update rate and as high as possible display rate (FPS),
 * manages switching between instances (every instance is identified by a name/std::string);
 * the run() method starts display thread and runs the game.
 */
class GameCore {
public:
  GameCore(const std::string& first_instance_name, std::unique_ptr<CoreInstance> first_instance, double update_rate);
  GameCore(const GameCore& other) = delete;
  GameCore& operator=(const GameCore& other) = delete;
  ~GameCore();

  /**
   * Adds core event to queue, to be handled between updates, allowing instances to interact with game core
   */
  void pushEventQueue(const CoreEvent &event);

  /**
   * Main function of the game.
   * Starts the display thread and calls instance update function repeatedly at a fixed timestep,
   * as specified by update_rate variable. Handles core events between updates.
   */
  void run();

  void addInstance(const std::string& instance_name, std::unique_ptr<CoreInstance> instance);

private:

  /**
   * Handles events between updates, making it possible to for instances to interact with game core
   */
  void handleEvent(const CoreEvent& event);

  /**
   * Main function of display thread.
   * Repeatedly calls display function of current instance and adds FPS counter if showFPS is true,
   * setting frame rate (via core events) to a positive value restricts the frame rate,
   * when it's not restricted, it works as fast as possible or as fast as VSync allows.
   */
  void displayThreadFunction();

  bool running;
  std::map<std::string, CoreInstanceWrapper> instances;
  std::queue<CoreEvent> event_queue;
  std::mutex event_queue_mutex;
  std::mutex instance_list_mutex;
  std::string current_instance;
  std::thread display_thread;

  bool showFPS = true;
  bool showUPS = false;
  int current_ups = 0;
  double beginning_update_rate;

  std::unique_ptr<PeriodicExecutionManager> update_execution_manager;
  std::unique_ptr<PeriodicExecutionManager> display_execution_manager;

  //So that CoreInstance can access the window field directly
  friend class CoreInstance;
  sf::RenderWindow window;
};


#endif //WARRIORS_GAMECORE_H
