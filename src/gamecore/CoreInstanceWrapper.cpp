//
// Created by aleksander on 10.07.18.
//

#include <iostream>
#include <entitysystem/Entity.h>
#include "CoreInstanceWrapper.h"

//#define SHOW_WRAPPER_TIMINGS

CoreInstanceWrapper::CoreInstanceWrapper(std::unique_ptr<CoreInstance> instance)
    : instance(std::move(instance)),
      update_timestamp_current(-1),
      update_timestamp_previous(-1){
  if (!this->instance) {
    throw std::runtime_error("Instance pointer is null");
  }
  display_data_current = this->instance->outputDisplayData();
  display_data_previous = this->instance->outputDisplayData();
}

CoreInstanceWrapper::~CoreInstanceWrapper() {
  /*
  std::cout<<"Display avg: "<<measure_display.getAverage()<<", sd: "<<measure_display.getStandardDeviation()<<std::endl;
  std::cout<<"Display lock avg: "<<measure_display_lock.getAverage()<<", sd: "<<measure_display_lock.getStandardDeviation()<<std::endl;
  std::cout<<"Synchronize avg: "<<measure_synchronize.getAverage()<<", sd: "<<measure_synchronize.getStandardDeviation()<<std::endl;
  std::cout<<"Synchronize lock avg: "<<measure_synchronize_lock.getAverage()<<", sd: "<<measure_synchronize_lock.getStandardDeviation()<<std::endl;
  std::cout<<"Update avg: "<<measure_update.getAverage()<<", sd: "<<measure_update.getStandardDeviation()<<std::endl;
  std::cout<<"Update lock avg: "<<measure_update_lock.getAverage()<<", sd: "<<measure_update_lock.getStandardDeviation()<<std::endl;
   */
}

void CoreInstanceWrapper::update() {
  sf::Clock measurement;
  sf::Clock measurement1;
  sf::Int64 t1;

  //std::lock_guard<std::mutex> lock(update_mutex);
  t1 = measurement1.getElapsedTime().asMicroseconds();
  instance->update();


#ifdef SHOW_WRAPPER_TIMINGS
  std::cout<<"Update: "<<measurement.getElapsedTime().asMilliseconds()<<", lock: "<<t1/1000<<std::endl;
#endif
  measure_update.add(measurement.getElapsedTime().asMicroseconds()*1e-3);
  measure_update_lock.add(t1*1e-3);
  measurement.restart();

  std::unique_ptr<CoreInstance::DisplayData> new_display_data = this->instance->outputDisplayData();
  if (!new_display_data) {
    std::cout<<"Current instance did not return any display data"<<std::endl;
  } else { //synchronized
    measurement1.restart();
    std::lock_guard<std::mutex> lock(display_queue_mutex);
    t1 = measurement1.getElapsedTime().asMicroseconds();

    /*
    if (update_timestamp_current < 0) {
      time_regulator.restart();
      update_timestamp_current = 0;
    } else {
      update_timestamp_current = (time_regulator.getElapsedTime().asMicroseconds() + tick_period / 2) / tick_period * tick_period;
    }
     */
    update_timestamp_previous = update_timestamp_current;
    update_timestamp_current = time_regulator.getElapsedTime().asMicroseconds();

    display_data_queue.push(std::move(new_display_data));
  } //end of synchronized block
#ifdef SHOW_WRAPPER_TIMINGS
  std::cout<<"Synchronize: "<<measurement.getElapsedTime().asMilliseconds()<<", lock: "<<t1/1000<<std::endl;
#endif
  measure_synchronize.add(measurement.getElapsedTime().asMicroseconds()*1e-3);
  measure_synchronize_lock.add(t1*1e-3);
}

void CoreInstanceWrapper::display(sf::RenderWindow &sfml_window) {
  sf::Clock measurement;
  sf::Int64 t1 = 0;
  sf::Clock measurement1;
  sf::Int64 update_timestamp_current_copy;
  sf::Int64 update_timestamp_previous_copy;

  { //synchronized
    std::lock_guard<std::mutex> lock(display_queue_mutex);
    t1 = measurement1.getElapsedTime().asMilliseconds();
    update_timestamp_current_copy = update_timestamp_current;
    update_timestamp_previous_copy = update_timestamp_previous;
    while (!display_data_queue.empty()) {
      display_data_previous = std::move(display_data_current);
      display_data_current = std::move(display_data_queue.front());
      display_data_queue.pop();
    }
  } //end of synchronized block

  //double fraction = time_regulator.getElapsedTime().asMicroseconds() - update_timestamp_current_copy;
  //fraction /= tick_period;
  //fraction = std::max(0.0, std::min(1.0, fraction));

  double fraction = (time_regulator.getElapsedTime().asMicroseconds() - update_timestamp_current) / (double)(update_timestamp_current - update_timestamp_previous);

  if (display_data_previous && display_data_current) {
    display_data_current->display(sfml_window, *display_data_previous, fraction);
  }
#ifdef SHOW_WRAPPER_TIMINGS
  std::cout<<"Display: "<<measurement.getElapsedTime().asMilliseconds()<<", lock: "<<t1/1000<<std::endl;
#endif
  measure_display.add(measurement.getElapsedTime().asMicroseconds()*1e-3);
  measure_display_lock.add(t1*1e-3);
}
