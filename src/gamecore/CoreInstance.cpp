//
// Created by aleksander on 10.07.18.
//

#include "CoreInstance.h"
#include "GameCore.h"

void CoreInstance::switchToInstance(const std::string &name) {
  core->pushEventQueue(CoreEvent(CoreEvent::SWITCH, name));
}

void CoreInstance::deleteInstance(const std::string &name) {
  core->pushEventQueue(CoreEvent(CoreEvent::DELETE, name));
}

void CoreInstance::makeInstance(const std::string &name, std::unique_ptr<CoreInstance> instance) {
  core->pushEventQueue(CoreEvent(CoreEvent::MAKE, name, std::move(instance)));
}

void CoreInstance::quit() {
  core->pushEventQueue(CoreEvent(CoreEvent::QUIT));
}

void CoreInstance::toggleFPSCounter() {
  core->pushEventQueue(CoreEvent(CoreEvent::FPS_SHOW));
}

void CoreInstance::toggleUPSCounter() {
  core->pushEventQueue(CoreEvent(CoreEvent::UPS_SHOW));
}

void CoreInstance::setFPSCap(double fps) {
  core->pushEventQueue(CoreEvent(CoreEvent::FPS_SET, "", nullptr, fps));
}

void CoreInstance::setUPSCap(double ups) {
  core->pushEventQueue(CoreEvent(CoreEvent::UPS_SET, "", nullptr, ups));
}

sf::RenderWindow& CoreInstance::getWindow() {
  return core->window;
}