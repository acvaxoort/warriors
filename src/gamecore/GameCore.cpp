//
// Created by aleksander on 17.06.18.
//

#include <iostream>
#include <util/SfmlWindowUtil.h>
#include <util/Fonts.h>
#include "GameCore.h"

GameCore::GameCore(const std::string& first_instance_name, std::unique_ptr<CoreInstance> first_instance,
                   double update_rate)
    : running(false),
      beginning_update_rate(update_rate) {
  if (!first_instance) {
    throw std::runtime_error("First instance is null");
  }
  first_instance->core = this;
  instances.emplace(std::piecewise_construct,
                    std::forward_as_tuple(first_instance_name),
                    std::forward_as_tuple(std::move(first_instance)));
  current_instance = first_instance_name;
}

GameCore::~GameCore() {
  if (display_thread.joinable()) {
    display_thread.join();
  }

  /*
  if (update_execution_manager) {
    std::cout<<"Average update rate: "<<update_execution_manager->measure.getAverage()
      <<", sd: "<<update_execution_manager->measure.getStandardDeviation()<<std::endl;
  }

  if (display_execution_manager) {
    std::cout<<"Average frame rate: "<<display_execution_manager->measure.getAverage()
             <<", sd: "<<display_execution_manager->measure.getStandardDeviation()<<std::endl;
  }*/
}

void GameCore::addInstance(const std::string &instance_name, std::unique_ptr<CoreInstance> instance) {
  if (!instance) {
    return;
  }
  instance->core = this;
  instances.emplace(std::piecewise_construct,
                    std::forward_as_tuple(instance_name),
                    std::forward_as_tuple(std::move(instance)));
}

void GameCore::pushEventQueue(const CoreEvent &event) {
  std::lock_guard<std::mutex> lock(event_queue_mutex);
  event_queue.emplace(event);
}

void GameCore::handleEvent(const CoreEvent &event) {
  auto it = instances.find(event.instance_name);
  switch (event.type) {
    case CoreEvent::EventType::DELETE: {
      std::lock_guard<std::mutex> lock(instance_list_mutex);
      if (it != instances.end()) {
        instances.erase(it);
      }
    } break;
    case CoreEvent::EventType::SWITCH: {
      std::lock_guard<std::mutex> lock(instance_list_mutex);
      if (it != instances.end()) {
        current_instance = event.instance_name;
      } else {
        std::cout<<"Attempted to switch to an instance that doesn't exist: "+event.instance_name<<std::endl;
      }
    } break;
    case CoreEvent::EventType::MAKE: {
      std::lock_guard<std::mutex> lock(instance_list_mutex);
      if (it != instances.end()) {
        std::cout<<"Attempted to create instance that already exists: "+event.instance_name<<std::endl;
      } else {
        event.instance->core = this;
        instances.emplace(std::piecewise_construct,
                          std::forward_as_tuple(event.instance_name),
                          std::forward_as_tuple(std::unique_ptr<CoreInstance>(event.instance)));
      }
    } break;
    case CoreEvent::EventType::FPS_SHOW: {
      showFPS = !showFPS;
    } break;
    case CoreEvent::EventType::UPS_SHOW: {
      showUPS = !showUPS;
    } break;
    case CoreEvent::EventType::FPS_SET: {
      if (display_execution_manager) {
        display_execution_manager->setUpdatesPerSecond(event.data);
      }
    } break;
    case CoreEvent::EventType::UPS_SET: {
      if (update_execution_manager) {
        update_execution_manager->setUpdatesPerSecond(event.data);
      }
    } break;
    case CoreEvent::EventType::QUIT: {
      if (display_execution_manager) {
        display_execution_manager->stop();
      }
      if (update_execution_manager) {
        update_execution_manager->stop();
      }
      running = false;
    } break;
  }
}

void GameCore::run() {
  if (running) {
    std::cout<<"Attempted to run a running core."<<std::endl;
    return;
  } else {
    running = true;
  }
  display_thread = std::thread(&GameCore::displayThreadFunction, this);

  class PeriodicUpdate : public PeriodicExecutionManager {
  public:
    explicit PeriodicUpdate(GameCore& core): PeriodicExecutionManager(core.beginning_update_rate), core(core) {}
    GameCore& core; //inner classes when

    void update() override {
      auto it = core.instances.find(core.current_instance);
      if (it != core.instances.end()) {
        it->second.update();
      } else {
        throw std::runtime_error("Update: No instance named " + core.current_instance);
      }
      //Handling event queue
      core.event_queue_mutex.lock();
      while (!core.event_queue.empty()) {
        core.handleEvent(core.event_queue.front());
        core.event_queue.pop();
      }
      core.event_queue_mutex.unlock();

      core.current_ups = lround(measure.getLocalAverage(5));
    }
  };

  update_execution_manager = std::make_unique<PeriodicUpdate>(*this);
  update_execution_manager->startMeasuring();
  update_execution_manager->run();
}

void GameCore::displayThreadFunction() {
  int frame_count = 0;
  double frame_rate;
  sf::Int64 frame_time = 0;
  sf::Int64 frame_period = 0;
  sf::Clock run_time_measure_clock;

  sf::Int64 frame_compensation = 0;
  sf::Clock frame_clock;
  class PeriodicDisplay : public PeriodicExecutionManager {
  public:
    explicit PeriodicDisplay(GameCore& core): PeriodicExecutionManager(0.0), core(core) {}
    GameCore& core; //inner classes when

    void update() override {
      if (core.instance_list_mutex.try_lock()) {
        auto it = core.instances.find(core.current_instance);
        if (it != core.instances.end()) {

          core.window.clear(sf::Color::Black);
          it->second.display(core.window);

          core.instance_list_mutex.unlock();

          if (core.showFPS || core.showUPS) {
            sf::Text text;
            text.setFont(Fonts::gfont);
            text.setFillColor(sf::Color(127, 127, 127, 127));
            text.setCharacterSize(16);

            std::string display_message;
            if (core.showFPS) {
              display_message += std::to_string( lround(measure.getLocalAverage(5)) );
            }
            if (core.showUPS) {
              display_message += "(" + std::to_string(core.current_ups) + ")";
            }

            text.setString(display_message);
            sf::View v = core.window.getView();
            core.window.setView(sfmlwutil::getDefaultView(core.window));
            core.window.draw(text);
            core.window.setView(v);
          }

          core.window.display();

        } else {
          throw std::runtime_error("Display: No instance named " + core.current_instance);
        }
      }
    }
  };

  display_execution_manager = std::make_unique<PeriodicDisplay>(*this);
  display_execution_manager->startMeasuring();
  display_execution_manager->run();
}