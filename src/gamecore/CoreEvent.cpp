//
// Created by aleksander on 10.07.18.
//

#include "CoreEvent.h"

CoreEvent::CoreEvent(CoreEvent::EventType type, const std::string &instance_name, std::unique_ptr<CoreInstance> instance, double data)
    : type(type),
      instance_name(instance_name),
      instance(instance.release()),
      data(data) {
}
