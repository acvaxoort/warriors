//
// Created by aleksander on 02.06.18.
//


#include <iostream>
#include <gamecore/GameCore.h>
#include <X11/Xlib.h>
#include <instances/simulation/SimulationInstance.h>
#include <world/Formations.h>
#include <scriptloader/BattleConfig.h>
#include <instances/menu/MenuInstance.h>
#include <util/RenderShapes.h>

int main(int argc, const char** argv) {
  XInitThreads();
  rendershapes::prepare();
  if (argc >= 2) {
    try {
      BattleConfig config(argv[1]);
      std::unique_ptr<GameCore> game_core = std::make_unique<GameCore>("simulation", std::make_unique<SimulationInstance>(std::move(config)), 60);
      game_core->addInstance("menu", std::make_unique<MenuInstance>(argv[1]));
      game_core->run();
    } catch (const std::runtime_error& e) {
      std::cout<<e.what()<<"\n";
    }
  } else {
    std::unique_ptr<GameCore> game_core = std::make_unique<GameCore>("menu", std::make_unique<MenuInstance>(), 60);
    game_core->run();
  }
  return 0;
}