//
// Created by aleksander on 06.05.19.
//

#ifndef WARRIORS_TEAMCONFIG_H
#define WARRIORS_TEAMCONFIG_H


#include <string>
#include <SFML/Graphics/Color.hpp>

struct TeamConfig {
  TeamConfig(std::string name, sf::Color color) : name(std::move(name)), color(color) {}

  std::string name;
  sf::Color color;
};


#endif //WARRIORS_TEAMCONFIG_H
