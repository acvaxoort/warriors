//
// Created by aleksander on 06.05.19.
//

#include <cmath>
#include <scriptloader/reigments/ReiterRegimentConfig.h>
#include <util/PlanarGeometryUtil.h>
#include <scriptloader/reigments/PikeAndShootRegimentConfig.h>
#include <scriptloader/reigments/HussarRegimentConfig.h>
#include <util/LuaUtil.hpp>
#include <scriptloader/reigments/CavalryRegimentConfig.h>
#include <scriptloader/reigments/FalangeRegimentConfig.h>
#include "LuaLibrary.h"
#include "scriptloader/reigments/RectangleRegimentConfig.h"

BattleConfig* lua_library::current_config = nullptr;

void lua_library::loadLibrary(lua_State* L) {
  lua_pushcfunction(L, setArea);
  lua_setglobal(L, "setArea");
  lua_pushcfunction(L, setGridSize);
  lua_setglobal(L, "setGridSize");
  lua_pushcfunction(L, addTeam);
  lua_setglobal(L, "addTeam");
  lua_pushcfunction(L, addRectangleRegiment);
  lua_setglobal(L, "addRectangleRegiment");
  lua_pushcfunction(L, addReiterRegiment);
  lua_setglobal(L, "addReiterRegiment");
  lua_pushcfunction(L, addHussarRegiment);
  lua_setglobal(L, "addHussarRegiment");
  lua_pushcfunction(L, addPikeAndShootRegiment);
  lua_setglobal(L, "addPikeAndShootRegiment");
  lua_pushcfunction(L, addPikeAndShootRegiment);
  lua_setglobal(L, "addPikeAndShootRegiment");
    lua_pushcfunction(L, addCavalryRegiment);
    lua_setglobal(L, "addCavalryRegiment");
    lua_pushcfunction(L, addFalangeRegiment);
    lua_setglobal(L, "addFalangeRegiment");
}

int lua_library::setArea(lua_State* L) {
  if (!current_config) {
    luaL_error(L, "setArea: no config is being loaded (internal error)");
  }

  current_config->width = (int) luaL_checkint(L, 1);
  if (current_config->width <= 0) {
    luaL_error(L, "setArea: Width must be greater than 0");
  }

  current_config->height = (int) luaL_checkint(L, 2);
  if (current_config->height <= 0) {
    luaL_error(L, "setArea: Height must be greater than 0");
  }
  return 0;
}

int lua_library::setGridSize(lua_State *L) {
  if (!current_config) {
    luaL_error(L, "setGridSize: No config is being loaded");
  }

  current_config->grid_size = (double) luaL_checknumber(L, 1);
  if (current_config->grid_size <= 0) {
    luaL_error(L, "setGridSize: Grid size must be greater than 0");
  }
  return 0;
}

int lua_library::addTeam(lua_State* L) {
  if (!current_config) {
    luaL_error(L, "addTeam: no config is being loaded (internal error)");
  }

  std::string team_name = luaL_checkstring(L, 1);

  sf::Color team_color;

  long int color_value = luaL_checkint(L, 2);
  if (color_value < 0 || color_value > 255) {
    luaL_error(L, "addTeam: red color channel value must be between 0 and 255");
  }
  team_color.r = static_cast<sf::Uint8>(color_value);

  color_value = luaL_checkint(L, 3);
  if (color_value < 0 || color_value > 255) {
    luaL_error(L, "addTeam: green color channel value must be between 0 and 255");
  }
  team_color.g = static_cast<sf::Uint8>(color_value);

  color_value = luaL_checkint(L, 4);
  if (color_value < 0 || color_value > 255) {
    luaL_error(L, "addTeam: blue color channel value must be between 0 and 255");
  }
  team_color.b = static_cast<sf::Uint8>(color_value);

  size_t team_index = current_config->teams.size();
  current_config->teams.emplace_back(team_name, team_color);
  lua_pushinteger(L, team_index);
  return 1;
}

int lua_library::addRectangleRegiment(lua_State* L) {
  try {
    if (!current_config) {
      throw luautil::LoadingError("no config is being loaded (internal error)");
    }

    size_t team_id;
    luautil::getValueNumber(L, "team", team_id);
    if (team_id >= current_config->teams.size()) {
      throw luautil::LoadingError("there is no team with id " + std::to_string(team_id));
    }

    std::string soldier_type;
    luautil::getValueString(L, "type", soldier_type);
    bool valid = false;
    constexpr static int SOLDIER_TYPES_COUNT = 3;
    constexpr static const char* SOLDIER_TYPES[SOLDIER_TYPES_COUNT] = { "TestWarrior", "Archer", "Reiter" };
    for (auto t : SOLDIER_TYPES) {
      if (soldier_type == t) {
        valid = true;
        break;
      }
    }
    if (!valid) {
      luaL_error(L, "addRectangleRegiment: Invalid soldier type %s", soldier_type.c_str());
    }

    int count;
    luautil::getValueNumber(L, "count", count);
    if (count <= 0) {
      throw luautil::LoadingError("count must be greater than 0");
    }

    int rows;
    luautil::getValueNumber<int>(L, "rows", rows);
    if (rows <= 0) {
      throw luautil::LoadingError("rows count must be greater than 0");
    }

    int cannons = 0;
    luautil::getValueNumberOptional(L, "cannons", cannons);
    if (cannons < 0) {
      cannons = 0;
    }

    double center_x;
    luautil::getValueNumber(L, "x", center_x);
    if (center_x < 0 || center_x > current_config->width) {
      throw luautil::LoadingError("formation's x position is out of the battlefield (" + std::to_string(center_x) + ")");
    }

    double center_y;
    luautil::getValueNumber<double>(L, "y", center_y);
    if (center_y < 0 || center_y > current_config->height) {
      throw luautil::LoadingError("formation's y position is out of the battlefield (" + std::to_string(center_y) + ")");
    }

    double angle;
    luautil::getValueNumber(L, "angle", angle);
    angle = pgeom::clockwiseToRad(angle);

    double space;
    luautil::getValueNumber(L, "space", space);
    if (space <= 0) {
      throw luautil::LoadingError("space between soldiers must be greater than 0");
    }

    luautil::unloadTable(L);

    current_config->regiments.emplace_back(std::make_unique<RectangleRegimentConfig>(
        team_id, soldier_type, count, rows, center_x, center_y, angle, space
    ));

  } catch (const luautil::LoadingError& e) {
    std::stringstream err;
    err << "addRectangleRegiment: ";
    err << e.what();
    luaL_error(L, err.str().c_str());
  }
  return 0;
}

int lua_library::addReiterRegiment(lua_State *L) {
  try {
    if (!current_config) {
      throw luautil::LoadingError("no config is being loaded (internal error)");
    }

    size_t team_id;
    luautil::getValueNumber(L, "team", team_id);
    if (team_id >= current_config->teams.size()) {
      throw luautil::LoadingError("there is no team with id " + std::to_string(team_id));
    }

    int count;
    luautil::getValueNumber(L, "count", count);
    if (count <= 0) {
      throw luautil::LoadingError("count must be greater than 0");
    }

    int rows;
    luautil::getValueNumber<int>(L, "rows", rows);
    if (rows <= 0) {
      throw luautil::LoadingError("rows count must be greater than 0");
    }

    int cannons = 0;
    luautil::getValueNumberOptional(L, "cannons", cannons);
    if (cannons < 0) {
      cannons = 0;
    }

    double center_x;
    luautil::getValueNumber(L, "x", center_x);
    if (center_x < 0 || center_x > current_config->width) {
      throw luautil::LoadingError("formation's x position is out of the battlefield (" + std::to_string(center_x) + ")");
    }

    double center_y;
    luautil::getValueNumber<double>(L, "y", center_y);
    if (center_y < 0 || center_y > current_config->height) {
      throw luautil::LoadingError("formation's y position is out of the battlefield (" + std::to_string(center_y) + ")");
    }

    double angle;
    luautil::getValueNumber(L, "angle", angle);
    angle = pgeom::clockwiseToRad(angle);

    TargetingStrategy targeting_strategy;
    luautil::getValueBoolOptional(L, "prioritize_infantry", targeting_strategy.prioritize_infantry);
    luautil::getValueBoolOptional(L, "reckless_charge", targeting_strategy.reckless_charge);
    luautil::getValueNumberOptional(L, "waiting_time", targeting_strategy.waiting_time);

    luautil::unloadTable(L);

    current_config->regiments.emplace_back(std::make_unique<ReiterRegimentConfig>(
        team_id, count, rows, center_x, center_y, angle, targeting_strategy
    ));

  } catch (const luautil::LoadingError& e) {
    std::stringstream err;
    err << "addReiterRegiment: ";
    err << e.what();
    luaL_error(L, err.str().c_str());
  }
  return 0;
}

int lua_library::addPikeAndShootRegiment(lua_State *L) {
  try {
    if (!current_config) {
      throw luautil::LoadingError("no config is being loaded (internal error)");
    }

    size_t team_id;
    luautil::getValueNumber(L, "team", team_id);
    if (team_id >= current_config->teams.size()) {
      throw luautil::LoadingError("there is no team with id " + std::to_string(team_id));
    }

    int count;
    luautil::getValueNumber(L, "count", count);
    if (count <= 0) {
      throw luautil::LoadingError("count must be greater than 0");
    }

    int rows;
    luautil::getValueNumber<int>(L, "rows", rows);
    if (rows <= 0) {
      throw luautil::LoadingError("rows count must be greater than 0");
    }

    int cannons = 0;
    luautil::getValueNumberOptional(L, "cannons", cannons);
    if (cannons < 0) {
      cannons = 0;
    }

    double center_x;
    luautil::getValueNumber(L, "x", center_x);
    if (center_x < 0 || center_x > current_config->width) {
      throw luautil::LoadingError("formation's x position is out of the battlefield (" + std::to_string(center_x) + ")");
    }

    double center_y;
    luautil::getValueNumber<double>(L, "y", center_y);
    if (center_y < 0 || center_y > current_config->height) {
      throw luautil::LoadingError("formation's y position is out of the battlefield (" + std::to_string(center_y) + ")");
    }

    double angle;
    luautil::getValueNumber(L, "angle", angle);
    angle = pgeom::clockwiseToRad(angle);

    TargetingStrategy targeting_strategy;
    luautil::getValueBoolOptional(L, "prioritize_infantry", targeting_strategy.prioritize_infantry);
    luautil::getValueBoolOptional(L, "reckless_charge", targeting_strategy.reckless_charge);
    luautil::getValueNumberOptional(L, "waiting_time", targeting_strategy.waiting_time);

    luautil::unloadTable(L);

    current_config->regiments.emplace_back(std::make_unique<PikeAndShootRegimentConfig>(
        team_id, count, rows, cannons, center_x, center_y, angle, targeting_strategy
    ));

  } catch (const luautil::LoadingError& e) {
    std::stringstream err;
    err << "addPikeAndShootRegiment: ";
    err << e.what();
    luaL_error(L, err.str().c_str());
  }
  return 0;
}
int lua_library::addFalangeRegiment(lua_State *L) {
    try {
        if (!current_config) {
            throw luautil::LoadingError("no config is being loaded (internal error)");
        }

        size_t team_id;
        luautil::getValueNumber(L, "team", team_id);
        if (team_id >= current_config->teams.size()) {
            throw luautil::LoadingError("there is no team with id " + std::to_string(team_id));
        }

        std::string soldier_type;
        luautil::getValueString(L, "type", soldier_type);
        bool valid = false;
        constexpr static int SOLDIER_TYPES_COUNT = 1;
        constexpr static const char* SOLDIER_TYPES[SOLDIER_TYPES_COUNT] = { "Hoplite" };
        for (auto t : SOLDIER_TYPES) {
            if (soldier_type == t) {
                valid = true;
                break;
            }
        }
        if (!valid) {
            luaL_error(L, "addRectangleRegiment: Invalid soldier type %s", soldier_type.c_str());
        }

        int count;
        luautil::getValueNumber(L, "count", count);
        if (count <= 0) {
            throw luautil::LoadingError("count must be greater than 0");
        }

        int rows;
        luautil::getValueNumber<int>(L, "rows", rows);
        if (rows <= 0) {
            throw luautil::LoadingError("rows count must be greater than 0");
        }

        int cannons = 0;
        luautil::getValueNumberOptional(L, "cannons", cannons);
        if (cannons < 0) {
            cannons = 0;
        }

        double center_x;
        luautil::getValueNumber(L, "x", center_x);
        if (center_x < 0 || center_x > current_config->width) {
            throw luautil::LoadingError("formation's x position is out of the battlefield (" + std::to_string(center_x) + ")");
        }

        double center_y;
        luautil::getValueNumber<double>(L, "y", center_y);
        if (center_y < 0 || center_y > current_config->height) {
            throw luautil::LoadingError("formation's y position is out of the battlefield (" + std::to_string(center_y) + ")");
        }

        double angle;
        luautil::getValueNumber(L, "angle", angle);
        angle = pgeom::clockwiseToRad(angle);

        double space;
        luautil::getValueNumber(L, "space", space);
        if (space <= 0) {
            throw luautil::LoadingError("space between soldiers must be greater than 0");
        }

        double fal_id;
        luautil::getValueNumber(L, "fal_id", fal_id);
        if (space <= 0) {
            throw luautil::LoadingError("fal_id must be greater than 0");
        }
        luautil::unloadTable(L);

        current_config->regiments.emplace_back(std::make_unique<FalangeRegimentConfig>(
                team_id, soldier_type, count, rows, center_x, center_y, angle, space, fal_id
        ));

    } catch (const luautil::LoadingError& e) {
        std::stringstream err;
        err << "addFalangeRegiment: ";
        err << e.what();
        luaL_error(L, err.str().c_str());
    }
    return 0;
}
int lua_library::addCavalryRegiment(lua_State *L) {
    try {
        if (!current_config) {
            throw luautil::LoadingError("no config is being loaded (internal error)");
        }

        size_t team_id;
        luautil::getValueNumber(L, "team", team_id);
        if (team_id >= current_config->teams.size()) {
            throw luautil::LoadingError("there is no team with id " + std::to_string(team_id));
        }

        std::string soldier_type;
        luautil::getValueString(L, "type", soldier_type);
        bool valid = false;
        constexpr static int SOLDIER_TYPES_COUNT = 1;
        constexpr static const char* SOLDIER_TYPES[SOLDIER_TYPES_COUNT] = { "Cavalryman"};
        for (auto t : SOLDIER_TYPES) {
            if (soldier_type == t) {
                valid = true;
                break;
            }
        }
        if (!valid) {
            luaL_error(L, "addRectangleRegiment: Invalid soldier type %s", soldier_type.c_str());
        }

        int count;
        luautil::getValueNumber(L, "count", count);
        if (count <= 0) {
            throw luautil::LoadingError("count must be greater than 0");
        }

        int rows;
        luautil::getValueNumber<int>(L, "rows", rows);
        if (rows <= 0) {
            throw luautil::LoadingError("rows count must be greater than 0");
        }

        int cannons = 0;
        luautil::getValueNumberOptional(L, "cannons", cannons);
        if (cannons < 0) {
            cannons = 0;
        }

        double center_x;
        luautil::getValueNumber(L, "x", center_x);
        if (center_x < 0 || center_x > current_config->width) {
            throw luautil::LoadingError("formation's x position is out of the battlefield (" + std::to_string(center_x) + ")");
        }

        double center_y;
        luautil::getValueNumber<double>(L, "y", center_y);
        if (center_y < 0 || center_y > current_config->height) {
            throw luautil::LoadingError("formation's y position is out of the battlefield (" + std::to_string(center_y) + ")");
        }

        double angle;
        luautil::getValueNumber(L, "angle", angle);
        angle = pgeom::clockwiseToRad(angle);

        double space;
        luautil::getValueNumber(L, "space", space);
        if (space <= 0) {
            throw luautil::LoadingError("space between soldiers must be greater than 0");
        }

        luautil::unloadTable(L);

        current_config->regiments.emplace_back(std::make_unique<CavalryRegimentConfig>(
                team_id, soldier_type, count, rows, center_x, center_y, angle, space
        ));

    } catch (const luautil::LoadingError& e) {
        std::stringstream err;
        err << "addCavalryRegiment: ";
        err << e.what();
        luaL_error(L, err.str().c_str());
    }
    return 0;
}
int lua_library::addHussarRegiment(lua_State *L) {
  try {
    if (!current_config) {
      throw luautil::LoadingError("no config is being loaded (internal error)");
    }

    size_t team_id;
    luautil::getValueNumber(L, "team", team_id);
    if (team_id >= current_config->teams.size()) {
      throw luautil::LoadingError("there is no team with id " + std::to_string(team_id));
    }

    std::string soldier_type;
    luautil::getValueString(L, "type", soldier_type);
    bool valid = false;
    constexpr static const char* SOLDIER_TYPES[2] = { "Husarz", "Petyhorzec" };
    for (auto t : SOLDIER_TYPES) {
      if (soldier_type == t) {
        valid = true;
        break;
      }
    }
    if (!valid) {
      luaL_error(L, "addRectangleRegiment: Invalid soldier type %s", soldier_type.c_str());
    }

    int count;
    luautil::getValueNumber(L, "count", count);
    if (count <= 0) {
      throw luautil::LoadingError("count must be greater than 0");
    }

    int rows;
    luautil::getValueNumber<int>(L, "rows", rows);
    if (rows <= 0) {
      throw luautil::LoadingError("rows count must be greater than 0");
    }

    int cannons = 0;
    luautil::getValueNumberOptional(L, "cannons", cannons);
    if (cannons < 0) {
      cannons = 0;
    }

    double center_x;
    luautil::getValueNumber(L, "x", center_x);
    if (center_x < 0 || center_x > current_config->width) {
      throw luautil::LoadingError("formation's x position is out of the battlefield (" + std::to_string(center_x) + ")");
    }

    double center_y;
    luautil::getValueNumber<double>(L, "y", center_y);
    if (center_y < 0 || center_y > current_config->height) {
      throw luautil::LoadingError("formation's y position is out of the battlefield (" + std::to_string(center_y) + ")");
    }

    double angle;
    luautil::getValueNumber(L, "angle", angle);
    angle = pgeom::clockwiseToRad(angle);

    TargetingStrategy targeting_strategy;
    luautil::getValueBoolOptional(L, "prioritize_infantry", targeting_strategy.prioritize_infantry);
    luautil::getValueBoolOptional(L, "reckless_charge", targeting_strategy.reckless_charge);
    luautil::getValueNumberOptional(L, "waiting_time", targeting_strategy.waiting_time);

    luautil::unloadTable(L);

    current_config->regiments.emplace_back(std::make_unique<HussarRegimentConfig>(
        team_id, soldier_type, count, rows, center_x, center_y, angle, targeting_strategy
    ));

  } catch (const luautil::LoadingError& e) {
    std::stringstream err;
    err << "addHussarRegiment: ";
    err << e.what();
    luaL_error(L, err.str().c_str());
  }
  return 0;
}