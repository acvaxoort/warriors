//
// Created by aleksander on 06.05.19.
//

#ifndef WARRIORS_BATTLECONFIG_H
#define WARRIORS_BATTLECONFIG_H

#include <vector>
#include <memory>
#include "TeamConfig.h"
#include "RegimentConfig.h"

struct BattleConfig {
  BattleConfig(int width, int height, position_data_t grid_size);
  explicit BattleConfig(const std::string& filename);

  int width;
  int height;
  position_data_t grid_size;
  std::vector<TeamConfig> teams;
  std::vector<std::unique_ptr<RegimentConfig>> regiments;
};


#endif //WARRIORS_BATTLECONFIG_H
