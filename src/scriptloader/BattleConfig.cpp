//
// Created by aleksander on 06.05.19.
//

#include "BattleConfig.h"
#include "LuaLibrary.h"
#include <lua5.2/lua.hpp>
#include <stdexcept>

BattleConfig::BattleConfig(int width, int height, position_data_t grid_size)
    : width(width), height(height), grid_size(grid_size) {}

BattleConfig::BattleConfig(const std::string& filename)
    : width(0), height(0), grid_size(16) {
  lua_State* L = luaL_newstate();
  luaL_openlibs(L);
  lua_library::current_config = this;
  lua_library::loadLibrary(L);
  if (luaL_loadfile(L, filename.c_str()) != LUA_OK) {
    lua_close(L);
    lua_library::current_config = nullptr;
    throw std::runtime_error("Cannot open config file:\n" + std::string(lua_tostring(L, -1), lua_rawlen(L, -1)));
  }
  if (lua_pcall(L, 0, 0, 0) != LUA_OK) {
    std::string error_message = "Error while loading config file:\n" + std::string(lua_tostring(L, -1), lua_rawlen(L, -1));
    lua_close(L);
    lua_library::current_config = nullptr;
    throw std::runtime_error(error_message);
  }
  lua_close(L);
  lua_library::current_config = nullptr;
  if (width <= 0 || height <= 0) {
    throw std::runtime_error("Area was not set");
  }
}
