//
// Created by aleksander on 06.05.19.
//

#ifndef WARRIORS_LUALIB_H
#define WARRIORS_LUALIB_H

#include <lua5.2/lua.hpp>
#include "BattleConfig.h"

namespace lua_library {

    extern BattleConfig *current_config;

    void loadLibrary(lua_State *L);

    double clockwiseToRad(double angle_deg_clockwise);

    //Args: width (int), height (int)
    int setArea(lua_State *L);

    //Args: grid_size (double)
    int setGridSize(lua_State *L);

    //Args: name (string), r, g, b (integers between 0 and 255)
    int addTeam(lua_State *L);

    //Args: team id (int), soldier type (string), count (int), rows (int),
    //x position (num), y position (num), angle (num, clockwise in degrees), space between soldiers (num)
    int addRectangleRegiment(lua_State *L);

    //Args: team id (int), count (int), rows (int), x position (num), y position (num), angle (num, clockwise in degrees)
    int addReiterRegiment(lua_State *L);

    //Args: team id (int), count (int), rows (int), cannons (int)
    //x position (num), y position (num), angle (num, clockwise in degrees)
    int addPikeAndShootRegiment(lua_State *L);

    //Args: team id (int), type(string), count (int), rows (int)
    //x position (num), y position (num), angle (num, clockwise in degrees)
    int addHussarRegiment(lua_State *L);

    int addCavalryRegiment(lua_State *L);

    int addFalangeRegiment(lua_State *L);
}


#endif //WARRIORS_LUALIB_H
