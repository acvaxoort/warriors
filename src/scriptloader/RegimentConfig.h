//
// Created by aleksander on 06.05.19.
//

#ifndef WARRIORS_REGIMENTCONFIG_H
#define WARRIORS_REGIMENTCONFIG_H

#include <world/entities/SimulationEntitySystem.h>

class RegimentConfig {
public:
  virtual ~RegimentConfig() = default;
  virtual void create(SimulationEntitySystem& system) = 0;
};

#endif //WARRIORS_REGIMENTCONFIG_H
