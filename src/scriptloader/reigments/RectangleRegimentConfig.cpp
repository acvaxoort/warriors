//
// Created by aleksander on 25.05.19.
//

#include "RectangleRegimentConfig.h"
#include <world/entities/types/TestWarrior.h>
#include <world/regiments/types/DefaultRegiment.h>
#include <world/Formations.h>
#include <world/entities/types/kokenhausen/Reiter.h>

RectangleRegimentConfig::RectangleRegimentConfig(size_t team_id, std::string soldier_type, int count, int rows,
                                                 double center_x, double center_y, double angle, double spacing)
    : team_id(team_id), soldier_type(std::move(soldier_type)), count(count), rows(rows),
      center_x(center_x), center_y(center_y), angle(angle), spacing(spacing) {}

void RectangleRegimentConfig::create(SimulationEntitySystem &system)  {
  auto formation = Formation::rectangle(count, rows, spacing);
  std::vector<std::unique_ptr<Soldier>> temp_entities(static_cast<unsigned long>(count));

  /*if(soldier_type == "Archer")
  {
      for (int i = 0; i < count; ++i) {
          temp_entities[i] = std::make_unique<Archer>();
          auto position = Formation::positionInFormation(formation.positions[i].first, formation.positions[i].second,
                                                         center_x, center_y, angle);
          temp_entities[i]->setPos(position.first, position.second);
          temp_entities[i]->setAngle(angle);
          temp_entities[i]->setFormationOffset(formation.positions[i].first, formation.positions[i].second);
      }
  }
  else*/
  if (soldier_type == "Reiter") {
    for (int i = 0; i < count; ++i) {
      temp_entities[i] = std::make_unique<Reiter>();
      temp_entities[i]->setFormationOffset(formation.positions[i].first, formation.positions[i].second);
    }
  } else {
    for (int i = 0; i < count; ++i) {
      temp_entities[i] = std::make_unique<TestWarrior>();
      temp_entities[i]->setFormationOffset(formation.positions[i].first, formation.positions[i].second);
    }
  }

  auto temp_regiment=std::make_unique<DefaultRegiment>(center_x, center_y, angle, 0.25f, count, rows, spacing);

  Team* team = system.getTeam(team_id);
  temp_regiment->team = team;
  for (int i = 0; i < count; ++i) {
    temp_regiment->addSoldier(temp_entities[i].get());
  }
  temp_regiment->updateFormation();
  temp_regiment->setSoldiersInitPositions();

  team->addRegiment(std::move(temp_regiment));
  for (int i = 0; i < count; ++i) {
    system.addSimulationEntity(team_id, std::move(temp_entities[i]));
  }
}
