//
// Created by aleksander on 26.05.19.
//

#ifndef WARRIORS_PIKEANDSHOOTREGIMENTCONFIG_H
#define WARRIORS_PIKEANDSHOOTREGIMENTCONFIG_H

#include <scriptloader/RegimentConfig.h>
#include <world/regiments/TargetingStrategy.h>

class PikeAndShootRegimentConfig : public RegimentConfig {
public:
  PikeAndShootRegimentConfig(size_t team_id, int count, int rows, int cannons,
                               double center_x, double center_y, double angle,
                               TargetingStrategy targeting_strategy);

  void create(SimulationEntitySystem &system) override;

private:
  size_t team_id;
  int count;
  int rows;
  int cannons;
  double center_x;
  double center_y;
  double angle;
  TargetingStrategy targeting_strategy;
};

#endif //WARRIORS_PIKEANDSHOOTREGIMENTCONFIG_H
