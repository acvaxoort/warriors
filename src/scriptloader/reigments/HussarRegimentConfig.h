//
// Created by aleksander on 30.05.19.
//

#ifndef WARRIORS_HUSSARREGIMENTCONFIG_H
#define WARRIORS_HUSSARREGIMENTCONFIG_H

#include <scriptloader/RegimentConfig.h>
#include <world/regiments/TargetingStrategy.h>

class HussarRegimentConfig : public RegimentConfig {
public:
  HussarRegimentConfig(size_t team_id, std::string type, int count, int rows, double center_x,
                         double center_y, double angle, TargetingStrategy targeting_strategy);

  void create(SimulationEntitySystem &system) override;

private:
  size_t team_id;
  std::string type;
  int count;
  int rows;
  double center_x;
  double center_y;
  double angle;
  TargetingStrategy targeting_strategy;
};

#endif //WARRIORS_HUSSARREGIMENTCONFIG_H
