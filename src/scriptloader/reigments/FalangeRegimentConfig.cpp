//
// Created by kuba on 01.06.19.
//

#include <world/entities/types/leuctra/Hoplite.h>
#include <world/regiments/types/leuctra/Falange.h>
#include <world/entities/types/leuctra/KingOfSparta.h>
#include <world/entities/types/leuctra/SacredBandSoldier.h>
#include <world/entities/types/leuctra/SpartanEliteUnit.h>
#include "FalangeRegimentConfig.h"

FalangeRegimentConfig::FalangeRegimentConfig(size_t team_id, std::string soldier_type, int count, int rows,
                                             double center_x, double center_y, double angle, double spacing, int fal_id)
        : team_id(team_id), soldier_type(std::move(soldier_type)), count(count), rows(rows),
          center_x(center_x), center_y(center_y), angle(angle), spacing(spacing), fal_id(fal_id) {}

void FalangeRegimentConfig::create(SimulationEntitySystem &system) {
    std::vector<std::unique_ptr<Soldier>> temp_entities(static_cast<unsigned long>(count));
    if (fal_id == 10) { // pierwsza formacja , krol mniej wiecej po srodku
        if (soldier_type == "Hoplite") {
            for (int i = 0; i < count; ++i) {
                if(i ==  1150)
                {
                    temp_entities[i] = std::make_unique<KingOfSparta>();
                }
                else
                {
                    temp_entities[i] = std::make_unique<Hoplite>();
                }


            }
        } else {
            std::cout << "FalangeRegimentConfig: Invalid soldier type " + soldier_type + "\n";
        }
    }
    else if (fal_id == 1)
    {
        if (soldier_type == "Hoplite") {
            for (int i = 0; i < count; ++i) {
                if(i > count - 300)
                {
                    temp_entities[i] = std::make_unique<SacredBandSoldier>();
                }
                else
                {
                    temp_entities[i] = std::make_unique<Hoplite>();
                }


            }
        } else {
            std::cout << "FalangeRegimentConfig: Invalid soldier type " + soldier_type + "\n";
        }
    }
    else if (fal_id == 5)
    {
        if (soldier_type == "Hoplite") {
            for (int i = 0; i < count; ++i) {
                if(i > count - 300)
                {
                    temp_entities[i] = std::make_unique<SpartanEliteUnit>();
                }
                else
                {
                    temp_entities[i] = std::make_unique<Hoplite>();
                }


            }
        } else {
            std::cout << "FalangeRegimentConfig: Invalid soldier type " + soldier_type + "\n";
        }
    }




    else {
        if (soldier_type == "Hoplite") {
            for (int i = 0; i < count; ++i) {
                temp_entities[i] = std::make_unique<Hoplite>();

            }
        } else {
            std::cout << "FalangeRegimentConfig: Invalid soldier type " + soldier_type + "\n";
        }
    }

    auto temp_regiment = std::make_unique<Falange>(center_x, center_y, angle, 0.5f, count, rows, spacing, fal_id);

    Team *team = system.getTeam(team_id);
    temp_regiment->team = team;
    for (int i = 0; i < count; ++i) {
        temp_regiment->addSoldier(temp_entities[i].get());
    }

    temp_regiment->updateFormation();
    temp_regiment->setSoldiersInitPositions();

    team->addRegiment(std::move(temp_regiment));
    for (int i = 0; i < count; ++i) {
        system.addSimulationEntity(team_id, std::move(temp_entities[i]));
    }
}
