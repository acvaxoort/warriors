//
// Created by kuba on 01.06.19.
//

#ifndef WARRIORS_CAVALRYREGIMENTCONFIG_H
#define WARRIORS_CAVALRYREGIMENTCONFIG_H

#include <scriptloader/RegimentConfig.h>

class CavalryRegimentConfig : public RegimentConfig {
public:
    CavalryRegimentConfig(size_t team_id, std::string soldier_type, int count, int rows, double center_x,
                            double center_y, double angle, double spacing);

    void create(SimulationEntitySystem &system) override;

private:
    size_t team_id;
    std::string soldier_type;
    int count;
    int rows;
    double center_x;
    double center_y;
    double angle;
    double spacing;
};



#endif //WARRIORS_CAVALRYREGIMENTCONFIG_H
