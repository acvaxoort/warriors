//
// Created by aleksander on 06.05.19.
//

#ifndef WARRIORS_RECTANGLEREGIMENTCONFIG_H
#define WARRIORS_RECTANGLEREGIMENTCONFIG_H

#include <world/regiments/TargetingStrategy.h>
#include "scriptloader/RegimentConfig.h"

class RectangleRegimentConfig : public RegimentConfig {
public:
  RectangleRegimentConfig(size_t team_id, std::string soldier_type, int count, int rows,
                            double center_x, double center_y, double angle, double spacing);

  void create(SimulationEntitySystem &system) override;

private:
  size_t team_id;
  std::string soldier_type;
  int count;
  int rows;
  double center_x;
  double center_y;
  double angle;
  double spacing;
};


#endif //WARRIORS_RECTANGLEREGIMENTCONFIG_H
