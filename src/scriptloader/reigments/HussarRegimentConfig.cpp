#include <utility>

//
// Created by aleksander on 30.05.19.
//

#include "HussarRegimentConfig.h"

#include <world/Formations.h>
#include <world/regiments/types/kokenhausen/HussarRegiment.h>
#include <world/entities/types/kokenhausen/Hussar.h>
#include <world/entities/types/kokenhausen/Petyhorzec.h>

HussarRegimentConfig::HussarRegimentConfig(size_t team_id, std::string type, int count, int rows, double center_x,
                                           double center_y, double angle, TargetingStrategy targeting_strategy)
    : team_id(team_id), type(std::move(type)), count(count), rows(rows), center_x(center_x), center_y(center_y),
      angle(angle), targeting_strategy(targeting_strategy) {}

void HussarRegimentConfig::create(SimulationEntitySystem &system) {
  std::vector<std::unique_ptr<Hussar>> temp_entities(static_cast<unsigned long>(count));
  if (type == "Husarz") {
    for (int i = 0; i < count; ++i) {
      temp_entities[i] = std::make_unique<Hussar>();
    }
  } else if (type == "Petyhorzec") {
    for (int i = 0; i < count; ++i) {
      temp_entities[i] = std::make_unique<Petyhorzec>();
    }
  } else {
    std::cout<<"HussarRegimentConfig: Invalid soldier type " + type + "\n";
  }

  auto temp_regiment = std::make_unique<HussarRegiment>(center_x, center_y, angle, count, rows, targeting_strategy);

  Team* team = system.getTeam(team_id);
  temp_regiment->team = team;
  for (int i = 0; i < count; ++i) {
    temp_regiment->addSoldier(temp_entities[i].get());
  }

  temp_regiment->updateFormation();
  temp_regiment->setSoldiersInitPositions();

  team->addRegiment(std::move(temp_regiment));
  for (int i = 0; i < count; ++i) {
    system.addSimulationEntity(team_id, std::move(temp_entities[i]));
  }
}
