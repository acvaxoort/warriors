//
// Created by aleksander on 25.05.19.
//

#ifndef WARRIORS_REITERREGIMENTCONFIG_H
#define WARRIORS_REITERREGIMENTCONFIG_H

#include <scriptloader/RegimentConfig.h>
#include <world/regiments/TargetingStrategy.h>

class ReiterRegimentConfig : public RegimentConfig {
public:
  ReiterRegimentConfig(size_t team_id, int count, int rows, double center_x, double center_y,
                         double angle, TargetingStrategy targeting_strategy);

  void create(SimulationEntitySystem &system) override;

private:
  size_t team_id;
  int count;
  int rows;
  double center_x;
  double center_y;
  double angle;
  TargetingStrategy targeting_strategy;
};

#endif //WARRIORS_REITERREGIMENTCONFIG_H
