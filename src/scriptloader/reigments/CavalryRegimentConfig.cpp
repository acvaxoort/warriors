//
// Created by kuba on 01.06.19.
//

#include "CavalryRegimentConfig.h"

#include <world/Formations.h>
#include <world/regiments/types/leuctra/CavalryRegiment.h>
#include <world/entities/types/leuctra/Cavalryman.h>

CavalryRegimentConfig::CavalryRegimentConfig(size_t team_id, std::string soldier_type, int count, int rows,
                                             double center_x, double center_y, double angle, double spacing)
        : team_id(team_id), soldier_type(std::move(soldier_type)), count(count), rows(rows),
          center_x(center_x), center_y(center_y), angle(angle), spacing(spacing) {}

void CavalryRegimentConfig::create(SimulationEntitySystem &system) {
    std::vector<std::unique_ptr<Cavalryman>> temp_entities(static_cast<unsigned long>(count));
    if (soldier_type == "Cavalryman") {
        for (int i = 0; i < count; ++i) {
            temp_entities[i] = std::make_unique<Cavalryman>();
        }
    } else {
        std::cout << "CavalryRegimentConfig: Invalid soldier type " + soldier_type + "\n";
    }

    auto temp_regiment = std::make_unique<CavalryRegiment>(center_x, center_y, angle,0.6f, count, rows,spacing);

    Team *team = system.getTeam(team_id);
    temp_regiment->team = team;
    for (int i = 0; i < count; ++i) {
        temp_regiment->addSoldier(temp_entities[i].get());
    }

    temp_regiment->updateFormation();
    temp_regiment->setSoldiersInitPositions();

    team->addRegiment(std::move(temp_regiment));
    for (int i = 0; i < count; ++i) {
        system.addSimulationEntity(team_id, std::move(temp_entities[i]));
    }
}
