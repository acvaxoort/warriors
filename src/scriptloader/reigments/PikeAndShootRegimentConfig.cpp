//
// Created by aleksander on 26.05.19.
//

#include "PikeAndShootRegimentConfig.h"
#include <world/regiments/types/kokenhausen/PikeAndShootLine.h>
#include <world/entities/types/kokenhausen/Pikeman.h>
#include <world/entities/types/kokenhausen/Musketeer.h>
#include <world/entities/types/kokenhausen/Cannon.h>
#include <world/regiments/types/kokenhausen/CannonLine.h>

PikeAndShootRegimentConfig::PikeAndShootRegimentConfig(size_t team_id, int count, int rows, int cannons,
                                                       double center_x, double center_y, double angle,
                                                       TargetingStrategy targeting_strategy)
    : team_id(team_id), count(count), rows(rows), cannons(cannons),
      center_x(center_x), center_y(center_y), angle(angle), targeting_strategy(targeting_strategy) {}

void PikeAndShootRegimentConfig::create(SimulationEntitySystem &system) {
  std::vector<std::unique_ptr<Soldier>> temp_entities(static_cast<unsigned long>(count));
  int count_half = count / 2;
  for (int i = 0; i < count_half; ++i) {
    temp_entities[i] = std::make_unique<Pikeman>();
  }
  for (int i = count_half; i < count; ++i) {
    temp_entities[i] = std::make_unique<Musketeer>();
  }
  auto temp_regiment = std::make_unique<PikeAndShootLine>(center_x, center_y, angle, count, rows, targeting_strategy);

  Team* team = system.getTeam(team_id);
  temp_regiment->team = team;
  for (int i = 0; i < count; ++i) {
    temp_regiment->addSoldier(temp_entities[i].get());
  }

  temp_regiment->updateFormation();
  temp_regiment->setSoldiersInitPositions();

  if (cannons > 0) {
    std::vector<std::unique_ptr<Soldier>> temp_cannons(static_cast<unsigned long>(cannons));
    for (int i = 0; i < cannons; ++i) {
      temp_cannons[i] = std::make_unique<Cannon>();
    }
    auto cannon_regiment = std::make_unique<CannonLine>(temp_regiment.get(), cannons);
    cannon_regiment->team = team;
    for (int i = 0; i < cannons; ++i) {
      cannon_regiment->addSoldier(temp_cannons[i].get());
    }

    cannon_regiment->updateFormation();
    cannon_regiment->setSoldiersInitPositions();

    team->addRegiment(std::move(cannon_regiment));
    for (int i = 0; i < cannons; ++i) {
      system.addSimulationEntity(team_id, std::move(temp_cannons[i]));
    }
  }

  team->addRegiment(std::move(temp_regiment));
  for (int i = 0; i < count; ++i) {
    system.addSimulationEntity(team_id, std::move(temp_entities[i]));
  }
}


