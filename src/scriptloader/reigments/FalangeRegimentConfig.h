//
// Created by kuba on 01.06.19.
//

#ifndef WARRIORS_FALANGEREGIMENTCONFIG_H
#define WARRIORS_FALANGEREGIMENTCONFIG_H


#include <scriptloader/RegimentConfig.h>

class FalangeRegimentConfig : public RegimentConfig {
public:
    FalangeRegimentConfig(size_t team_id, std::string soldier_type, int count, int rows, double center_x,
                          double center_y, double angle, double spacing, int fal_id);

    void create(SimulationEntitySystem &system) override;
    int fal_id;
private:
    size_t team_id;
    std::string soldier_type;
    int count;
    int rows;
    double center_x;
    double center_y;
    double angle;
    double spacing;
};

#endif //WARRIORS_FALANGEREGIMENTCONFIG_H
