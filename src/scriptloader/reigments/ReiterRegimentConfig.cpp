//
// Created by aleksander on 25.05.19.
//

#include "ReiterRegimentConfig.h"
#include <world/Formations.h>
#include <world/regiments/types/kokenhausen/ReiterRegiment.h>
#include <world/entities/types/kokenhausen/Reiter.h>

ReiterRegimentConfig::ReiterRegimentConfig(size_t team_id, int count, int rows, double center_x, double center_y,
                                           double angle, TargetingStrategy targeting_strategy)
    : team_id(team_id), count(count), rows(rows), center_x(center_x), center_y(center_y), angle(angle),
      targeting_strategy(targeting_strategy) {}

void ReiterRegimentConfig::create(SimulationEntitySystem &system) {
  std::vector<std::unique_ptr<Reiter>> temp_entities(static_cast<unsigned long>(count));
  for (int i = 0; i < count; ++i) {
    temp_entities[i] = std::make_unique<Reiter>();
  }
  auto temp_regiment = std::make_unique<ReiterRegiment>(center_x, center_y, angle, count, rows, targeting_strategy);

  Team* team = system.getTeam(team_id);
  temp_regiment->team = team;
  for (int i = 0; i < count; ++i) {
    temp_regiment->addSoldier(temp_entities[i].get());
  }

  temp_regiment->updateFormation();
  temp_regiment->setSoldiersInitPositions();

  team->addRegiment(std::move(temp_regiment));
  for (int i = 0; i < count; ++i) {
    system.addSimulationEntity(team_id, std::move(temp_entities[i]));
  }
}


