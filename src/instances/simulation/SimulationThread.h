//
// Created by aleksander on 24.05.19.
//

#ifndef WARRIORS_SIMULATIONTHREAD_H
#define WARRIORS_SIMULATIONTHREAD_H


#include <world/environment/Environment.h>
#include <world/entities/SimulationEntitySystem.h>
#include <world/Team.h>
#include <util/PeriodicExecutionManager.h>
#include <thread>

class SimulationThread {
public:
  SimulationThread(std::unique_ptr<Environment> environment, std::unique_ptr<SimulationEntitySystem> entity_system);
  ~SimulationThread();

  int64_t getTimestamp();

  void pause();
  void resume();
  void setUPS(double updates_per_second);

  double getCurrentUPS();

  struct State {
    std::shared_ptr<SimulationEntitySystem::DisplayData> previous_entity_system;
    std::shared_ptr<Environment::DisplayData> previous_environment;
    std::shared_ptr<SimulationEntitySystem::DisplayData> current_entity_system;
    std::shared_ptr<Environment::DisplayData> current_environment;
    std::vector<TeamStats> team_stats;
    double fraction;
  };

  State getState();

private:
  std::unique_ptr<SimulationEntitySystem> entity_system;
  std::unique_ptr<Environment> environment;

  std::shared_ptr<SimulationEntitySystem::DisplayData> previous_entity_system;
  std::shared_ptr<Environment::DisplayData> previous_environment;
  std::shared_ptr<SimulationEntitySystem::DisplayData> current_entity_system;
  std::shared_ptr<Environment::DisplayData> current_environment;
  std::vector<TeamStats> team_stats;

  std::chrono::time_point<std::chrono::high_resolution_clock> timer_start;
  int64_t previous_timestamp;
  int64_t current_timestamp;

  std::thread simulation_thread;
  std::unique_ptr<PeriodicExecutionManager> execution_manager;
  std::mutex state_synchronization_mutex;
};


#endif //WARRIORS_SIMULATIONTHREAD_H
