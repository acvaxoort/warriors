//
// Created by aleksander on 12.07.18.
//

#ifndef WARRIORS_LEVELINSTANCE_H
#define WARRIORS_LEVELINSTANCE_H

#include <gamecore/CoreInstance.h>
#include <world/environment/Environment.h>
#include <world/entities/SimulationEntitySystem.h>
#include <world/Team.h>
#include <util/SmoothView.h>
#include <util/AccumulatedMeasure.hpp>
#include <scriptloader/BattleConfig.h>
#include "SimulationThread.h"

class SimulationInstance : public CoreInstance {
public:

  class DisplayData : public CoreInstance::DisplayData {
  public:
    void display(sf::RenderWindow& window, const CoreInstance::DisplayData& previous, double fraction) override;
    sf::View view;
    SimulationThread::State simulation_state;
    double update_rate_multiplier;
    double updates_per_second;
    bool is_running;
  };

  SimulationInstance(BattleConfig&& config);
  void update() override;
  std::unique_ptr<CoreInstance::DisplayData> outputDisplayData() override;
private:
  //Environment environment;
  //SimulationEntitySystem entity_system;
  std::unique_ptr<SimulationThread> simulation_thread;
  SmoothView view;
  sf::Vector2i prev_mouse_pos;
  sf::Vector2i prev_window_pos;
  bool scrolling_with_mouse = false;
  double middle_x = 0;
  double middle_y = 0;
  double default_scale = 1;
  bool is_running = false;
  double simulation_update_rate = 2;
  double update_rate_multiplier = 1;
  double last_update_rate_multiplier = 1;
  int update_rate_multiplier_log = 0;
};


#endif //WARRIORS_LEVELINSTANCE_H
