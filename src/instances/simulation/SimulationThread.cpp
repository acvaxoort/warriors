//
// Created by aleksander on 24.05.19.
//

#include "SimulationThread.h"
#include <world/Team.h>

SimulationThread::SimulationThread(std::unique_ptr<Environment> environment_,
                                   std::unique_ptr<SimulationEntitySystem> entity_system_)
    : entity_system(std::move(entity_system_)),
      environment(std::move(environment_)) {
  if (!entity_system) {
    throw std::runtime_error("Entity system is null");
  }
  if (!environment) {
    throw std::runtime_error("Environment is null");
  }

  class PeriodicDisplay : public PeriodicExecutionManager {
  public:
    explicit PeriodicDisplay(SimulationThread& core): PeriodicExecutionManager(2.0), core(core) {}
    SimulationThread& core; //inner classes when

    void update() override {
      core.environment->update();
      core.entity_system->updateRegiments();
      core.entity_system->doCollideEach();
      core.entity_system->update();

      std::lock_guard<std::mutex> lock(core.state_synchronization_mutex);
      core.previous_timestamp = core.current_timestamp;
      core.current_timestamp = core.getTimestamp();
      core.previous_entity_system = core.current_entity_system;
      core.previous_environment = core.current_environment;
      core.current_entity_system = core.entity_system->outputDisplayData();
      core.current_environment = core.environment->outputDisplayData();
      core.team_stats.clear();
      for (auto& team : core.entity_system->teams) {
        core.team_stats.emplace_back(team->getStats());
      }
    }
  };

  execution_manager = std::make_unique<PeriodicDisplay>(*this);
  execution_manager->pause();
  execution_manager->startMeasuring();

  current_timestamp = getTimestamp();
  previous_timestamp = current_timestamp;
  current_entity_system = entity_system->outputDisplayData();
  current_environment = environment->outputDisplayData();
  previous_entity_system = current_entity_system;
  previous_environment = current_environment;

  for (auto& team : entity_system->teams) {
    team_stats.emplace_back(team->getStats());
  }

  simulation_thread = std::thread(&PeriodicExecutionManager::run, execution_manager.get());
  execution_manager->measure.setMemoryLength(1);
}

SimulationThread::~SimulationThread() {
  if (execution_manager) {
    execution_manager->stop();
    if (simulation_thread.joinable()) {
      simulation_thread.join();
    }
  }
}

int64_t SimulationThread::getTimestamp() {
  return std::chrono::duration_cast<std::chrono::microseconds>
      (std::chrono::high_resolution_clock::now() - timer_start).count();
}
void SimulationThread::pause() {
  if (execution_manager) {
    execution_manager->pause();
  }
}

void SimulationThread::resume() {
  if (execution_manager) {
    execution_manager->resume();
  }
}

void SimulationThread::setUPS(double updates_per_second) {
  if (execution_manager) {
    execution_manager->setUpdatesPerSecond(updates_per_second);
  }
}

double SimulationThread::getCurrentUPS() {
  return execution_manager->measure.getAverage();
}

SimulationThread::State SimulationThread::getState() {
  std::lock_guard<std::mutex> lock(state_synchronization_mutex);

  SimulationThread::State state;
  state.previous_entity_system = previous_entity_system;
  state.previous_environment = previous_environment;
  state.current_entity_system = current_entity_system;
  state.current_environment = current_environment;
  if (current_timestamp != previous_timestamp) {
    state.fraction = (double) (getTimestamp() - current_timestamp) / (current_timestamp - previous_timestamp);
  } else {
    state.fraction = 1.0;
  }
  if (state.fraction > 1.0) {
    state.fraction = 1.0;
  }
  state.team_stats = team_stats;
  return state;
}
