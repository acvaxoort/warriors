//
// Created by aleksander on 12.07.18.
//

#include "SimulationInstance.h"
#include <iostream>
#include <sstream>
#include <iomanip>

#include <util/SfmlWindowUtil.h>
#include <util/PlanarGeometryUtil.h>
#include <util/TimeUtil.hpp>
#include <util/Fonts.h>
#include <world/Formations.h>
#include <scriptloader/reigments/RectangleRegimentConfig.h>
#include <instances/menu/MenuInstance.h>
#include <world/entities/types/TestWarrior.h>
#include <world/regiments/types/DefaultRegiment.h>

SimulationInstance::SimulationInstance(BattleConfig&& config)
    : view(0.001, 0.02, 0.00333, 0.0667) {
  view.setScrollSpeedMax(0.006);
  view.setScaleSpeedMax(0.02);
  middle_x = config.width / 2.0;
  middle_y = config.height / 2.0;
  default_scale = log2(config.width + config.height) - 3;
  view.setPosition(middle_x, middle_y);
  view.setScale(default_scale);

  auto environment_ptr = std::make_unique<Environment>(config.width, config.height);
  auto entity_system_ptr = std::make_unique<SimulationEntitySystem>(environment_ptr.get(), config.grid_size);

  SimulationEntitySystem& entity_system = *entity_system_ptr;

  for (auto& team : config.teams) {
    auto new_team = std::make_unique<Team>(team.color, team.name);
    for (auto& t : entity_system.teams) {
      t->enemies.push_back(new_team.get());
      new_team->enemies.push_back(t.get());
    }
    entity_system.addTeam(std::move(new_team));
  }

  for (auto& reg : config.regiments) {
    reg->create(entity_system);
  }
  simulation_thread = std::make_unique<SimulationThread>(std::move(environment_ptr), std::move(entity_system_ptr));
}

void SimulationInstance::update() {
  if (!getWindow().isOpen()) {
    return;
  }
  auto mouse_pos = sf::Mouse::getPosition(getWindow());
  auto window_size = getWindow().getSize();
  sf::Event event;
  while (getWindow().pollEvent(event)) {
    switch(event.type) {
      case sf::Event::Closed: {
        quit();
      } break;
      case sf::Event::KeyPressed: {
        switch (event.key.code) {
          case sf::Keyboard::Escape: {
            switchToInstance("menu");
            deleteInstance("simulation");
          } break;
          case sf::Keyboard::Space: {
            if (is_running) {
              is_running = false;
              simulation_thread->pause();
            } else {
              is_running = true;
              simulation_thread->resume();
            }
          } break;
          case sf::Keyboard::F: {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
              toggleFPSCounter();
            }
          } break;
          case sf::Keyboard::G: {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
              toggleUPSCounter();
            }
          } break;
          case sf::Keyboard::R: {
            view.setPosition(middle_x, middle_y);
            view.setScale(default_scale);
          } break;
          case sf::Keyboard::N: {
            if (update_rate_multiplier_log > -4) {
              update_rate_multiplier_log--;
              update_rate_multiplier = std::pow(2, update_rate_multiplier_log);
            }
          } break;
          case sf::Keyboard::M: {
            update_rate_multiplier_log++;
            update_rate_multiplier = std::pow(2, update_rate_multiplier_log);
          } break;
          case sf::Keyboard::V: {
          } break;
        }
      } break;
      case sf::Event::MouseButtonPressed: {
        if (event.mouseButton.button == sf::Mouse::Button::Left) {
          if (mouse_pos.x >= 0 && mouse_pos.y >= 0 && mouse_pos.x < window_size.x && mouse_pos.y < window_size.y) {
            scrolling_with_mouse = true;
          }
        }
      } break;
      case sf::Event::MouseButtonReleased: {
        if (event.mouseButton.button == sf::Mouse::Button::Left) {
          scrolling_with_mouse = false;
        }
      } break;
      case sf::Event::MouseWheelScrolled: {
        view.mouseScroll(getWindow(), event.mouseWheelScroll.delta*2);
      } break;
    }
  }
  double view_dx2 = 0;
  double view_dy2 = 0;
  double view_ds2 = 0;
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
    view_dy2 -= 1;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
    view_dy2 += 1;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
    view_dx2 -= 1;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
    view_dx2 += 1;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
    view_ds2 -= 1;
  }
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
    view_ds2 += 1;
  }
  if (getWindow().isOpen()) {
    if (scrolling_with_mouse) {
      view.mouseDrag(getWindow(), mouse_pos.x-prev_mouse_pos.x, mouse_pos.y-prev_mouse_pos.y);
    } else {
      view.accelerate(view_dx2, view_dy2);
    }
    view.accelerateScale(view_ds2);
    prev_mouse_pos = mouse_pos;
  }

  view.tick();
  if (last_update_rate_multiplier != update_rate_multiplier) {
    simulation_thread->setUPS(simulation_update_rate*update_rate_multiplier);
    last_update_rate_multiplier = update_rate_multiplier;
  }
}

void SimulationInstance::DisplayData::display(sf::RenderWindow &window, const CoreInstance::DisplayData &previous,
                                          double fraction) {
  if (!window.isOpen()) {
    sfmlwutil::openWindowFromConfig(window);
  }

  try {
    const SimulationInstance::DisplayData& prev = dynamic_cast<const SimulationInstance::DisplayData&>(previous); //CLion is stupid yet again, dont try auto
    auto window_dims = window.getSize();
    sf::View interpolated_view = sfmlwutil::interpolate(prev.view, view, fraction);
    window.setView(interpolated_view);
    if (prev.simulation_state.previous_entity_system && prev.simulation_state.previous_environment) {
      prev.simulation_state.previous_environment->display(window);
      prev.simulation_state.current_entity_system->display(window, *prev.simulation_state.previous_entity_system,
          prev.simulation_state.fraction);
      prev.simulation_state.previous_environment->displayIndicators(window);
    }
    int text_pos = 20;
    for (auto& team: prev.simulation_state.team_stats) {
      sf::Text text;
      text.setFont(Fonts::gfont);
      text.setFillColor(team.color);
      text.setCharacterSize(16);
      text.setPosition(20, text_pos);
      text_pos += 20;

      text.setString(team.name + ": "
        + std::to_string(team.alive_count) + " fighting, "
        + std::to_string(team.dead_count) + " dead, "
        + std::to_string(team.escaped_count) + " fled" );
      sf::View v = window.getView();
      window.setView(sfmlwutil::getDefaultView(window));
      window.draw(text);
      window.setView(v);
    }
    sf::Text text;
    text.setFont(Fonts::gfont);
    text.setFillColor(sf::Color(127, 127, 127));
    text.setCharacterSize(16);
    text.setPosition(10, window_dims.y - 30);

    std::stringstream ss;
    if (!is_running) {
      ss << "[paused] ";
    }
    ss << std::setprecision(3) << updates_per_second << " UPS (" << update_rate_multiplier << std::setprecision(10) << "x)";
    text.setString(ss.str());

    sf::View v = window.getView();
    window.setView(sfmlwutil::getDefaultView(window));
    window.draw(text);
    window.setView(v);
  } catch(const std::bad_cast& e) {
    std::cout<<"Bad cast (interpolation)"<<std::endl;
    return;
  }
}

std::unique_ptr<CoreInstance::DisplayData> SimulationInstance::outputDisplayData() {
  auto display_data = std::make_unique<SimulationInstance::DisplayData>();

  display_data->view = view.getSfmlView(getWindow());
  display_data->update_rate_multiplier = update_rate_multiplier;
  display_data->updates_per_second = simulation_thread->getCurrentUPS();
  display_data->simulation_state = simulation_thread->getState();
  display_data->is_running = is_running;

  return std::move(display_data);
}
