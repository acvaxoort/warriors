//
// Created by aleksander on 15.10.18.
//

#ifndef WARRIORS_MENUINSTANCE_H
#define WARRIORS_MENUINSTANCE_H

#include <gamecore/CoreInstance.h>

class MenuInstance : public CoreInstance {
public:
  MenuInstance(const std::string& filename = "");

  class DisplayData : public CoreInstance::DisplayData {
  public:
    void display(sf::RenderWindow& window, const CoreInstance::DisplayData& previous, double fraction) override;
    std::wstring input_string;
    std::string error_message;
    bool show_text_position;
  };

  void update() override;
  std::unique_ptr<CoreInstance::DisplayData> outputDisplayData() override;

  std::wstring input_string;
  std::string error_message;
  int blinking = 0;
};

#endif //WARRIORS_MENUINSTANCE_H
