//
// Created by aleksander on 15.10.18.
//

#include <scriptloader/BattleConfig.h>
#include <instances/simulation/SimulationInstance.h>
#include <codecvt>
#include <util/SfmlWindowUtil.h>
#include <util/Fonts.h>
#include "MenuInstance.h"

MenuInstance::MenuInstance(const std::string &filename) {
  std::wstring_convert<std::codecvt_utf8<wchar_t>> convert;
  input_string = convert.from_bytes(filename);
}

void MenuInstance::update() {
  if (!getWindow().isOpen()) {
    return;
  }
  sf::Event event;
  while (getWindow().pollEvent(event)) {
    switch (event.type) {
      case sf::Event::Closed: {
        quit();
      } break;
      case sf::Event::KeyPressed: {
        switch (event.key.code) {
          case sf::Keyboard::Escape: {
            quit();
          } break;
          case sf::Keyboard::BackSpace: {
            if (error_message.empty() && !input_string.empty()) {
              input_string.pop_back();
            }
            blinking = 0;
          } break;
          case sf::Keyboard::Return: {
            if (error_message.empty()) {
              try {
                std::wstring_convert<std::codecvt_utf8<wchar_t>> convert;
                BattleConfig config(convert.to_bytes(input_string));
                makeInstance("simulation", std::make_unique<SimulationInstance>(std::move(config)));
                switchToInstance("simulation");
              } catch (const std::runtime_error& e) {
                error_message = e.what();
              }
            } else {
              error_message.clear();
            }
          } break;
        }
      } break;
      case sf::Event::TextEntered: {
        if (error_message.empty()) {
          if (event.text.unicode >= ' ') {
            input_string.push_back((wchar_t) event.text.unicode);
            blinking = 0;
          }
        }
      }
    }
  }
  blinking++;
  if (blinking >= 60) {
    blinking = 0;
  }
}

void MenuInstance::DisplayData::display(sf::RenderWindow &window, const CoreInstance::DisplayData &previous,
                                        double fraction) {
  if (!window.isOpen()) {
    sfmlwutil::openWindowFromConfig(window);
  }
  window.setView(sfmlwutil::getDefaultView(window));
  if (error_message.empty()) {
    sf::Text text;
    text.setFont(Fonts::gfont);
    text.setFillColor(sf::Color::White);
    text.setCharacterSize(32);
    text.setPosition(20, 20);
    text.setString("Warriors - battle simulation");
    window.draw(text);

    text.setCharacterSize(16);
    text.setPosition(20, 60);
    text.setString(L"File name: " + input_string + (show_text_position ? L"_" : L""));
    window.draw(text);
  } else {
    sf::Text text;
    text.setFont(Fonts::gfont);
    text.setFillColor(sf::Color::White);
    text.setCharacterSize(16);
    text.setPosition(20, 20);
    text.setString(error_message);
    window.draw(text);
  }
}

std::unique_ptr<CoreInstance::DisplayData> MenuInstance::outputDisplayData() {
  auto display_data = std::make_unique<MenuInstance::DisplayData>();

  display_data->input_string = input_string;
  display_data->error_message = error_message;
  display_data->show_text_position = blinking < 30;

  return std::move(display_data);
}
