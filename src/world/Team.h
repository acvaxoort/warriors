//
// Created by aleksander on 14.02.19.
//

#ifndef WARRIORS_TEAM_H
#define WARRIORS_TEAM_H


#include <SFML/Graphics/Color.hpp>
#include <string>
#include <vector>
#include <memory>
#include <world/regiments/Regiment.h>
#include "TeamStats.h"

class TestWarrior;

class Team {
public:
  Team(const sf::Color& color, const std::string& name);
  Team(const Team& other) = delete;
  Team& operator=(const Team& other) = delete;

  sf::Color color;
  std::string name;
  std::vector<Team*> enemies;
  std::vector<std::unique_ptr<Regiment>> regiments;
  std::vector<Regiment*> removal_queue;

  void addRegiment(std::unique_ptr<Regiment> reg);

  inline void notifyMemberCreation() { alive_count++; };
  inline void notifyMemberDeath() { alive_count--; dead_count++; };
  inline void notifyMemberEscape() { alive_count--; escaped_count++; };

  inline size_t getAliveCount() const { return alive_count; }
  inline size_t getDeadCount() const { return dead_count; }
  inline size_t getEscapedCount() const { return escaped_count; }

  TeamStats getStats();
  void update();

  SimulationEntitySystem* system;
private:
  size_t alive_count = 0;
  size_t dead_count = 0;
  size_t escaped_count = 0;
};


#endif //WARRIORS_TEAM_H
