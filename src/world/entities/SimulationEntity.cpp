//
// Created by aleksander on 15.02.19.
//

#include "SimulationEntity.h"
#include <world/entities/types/Soldier.h>

void SimulationEntity::removeSelf() {
  size_t index = static_cast<size_t>(-1);
  for (size_t i = 0, len = system->entities.size(); i < len; ++i) {
    if (system->entities[i].get() == this) {
      index = i;
    }
  }
  if (index != static_cast<size_t>(-1)) {
    system->removeEntity(index);
    collection->forgetAbout(this);
  }
}
