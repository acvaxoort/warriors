//
// Created by aleksander on 15.02.19.
//

#include <iostream>
#include <world/entities/SimulationEntity.h>
#include "SimulationEntitySystem.h"

SimulationEntitySystem::SimulationEntitySystem(Environment *environment, position_data_t grid_size)
    : EntitySystemCollection(grid_size,
          -(position_data_t)10*environment->getWidth(), -(position_data_t)10*environment->getHeight(),
          30*environment->getWidth(), 30*environment->getHeight()),
      environment(environment) {
}

void SimulationEntitySystem::addSimulationEntity(size_t team_id, std::unique_ptr<SimulationEntity> e)  {
  if (team_id >= teams.size()) {
    std::cout<<"No team with id " + std::to_string(team_id) + "\n";
  }
  e->environment = this->environment;
  e->collection = this;
  e->team_id = team_id;
  e->team = teams[team_id].get();
  e->team->notifyMemberCreation();
  addEntity(team_id, std::move(e));
}

Team* SimulationEntitySystem::getTeam(size_t index) {
  if (index < teams.size()) {
    return teams[index].get();
  }
  return nullptr;
}

void SimulationEntitySystem::updateRegiments(){
  for(auto & team: teams){
    team->update();
  }
}

void SimulationEntitySystem::addTeam(std::unique_ptr<Team> team) {
  team->system = this;
  teams.emplace_back(std::move(team));
}
