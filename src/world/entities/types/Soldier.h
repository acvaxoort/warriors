//
// Created by aleksander on 21.05.19.
//

#ifndef WARRIORS_SOLDIER_H
#define WARRIORS_SOLDIER_H

#include <world/entities/SimulationEntitySystem.h>
#include <world/entities/SimulationEntity.h>
#include <world/regiments/Regiment.h>
#include <util/SfmlWindowUtil.h>

//Kilka uwag:
// -Nie należy poruszać się zmieniając pos_x czy pos_y, można zmieniać vel_x i vel_y i używać accelerate(...)
// -Jeśli klasa posiada jakiś wzkaźnik na innego żołnieża, należy overloadować metodę forgetAbout(...) klasy Entity,
//  jest ona wykonywana jak jakiś żołnierz jest usuwany, dzięki temu można ustawić na null wszystkie odwołania do niego
// -Przeszukanie jednostek można realizować poprzez użycie iteratorów korzystających z indeksu przesrzennego,
//  iteratory te znajdują się w klasie SimulationEntity, warto zwrócić uwagę na iteratory po allies czy enemies
// -Najlepszym sposobem na zbadanie dystansu do jednostki jest porówanie kwadratu promienia i kwadratu dystansu
//  (uniknięcie obliczania sqrt), realizuje to funkcja pgeom::getDistanceSqr (która zwraca po prostu sumę kwadratów)
// -Obiekt jest tworzony poza kontekstem (SimulationEntitySystem, Environment), więc odwołanie do environment albo
//  system w konstruktorze skończy się naruszeniem ochrony pamięci. Można zamiast tego overloadować metodę init(),
//  jest ona wykonywana po dodaniu jednostki do systemu/środowiska
// -Metoda Soldier::update przetwarza stres/panikę i sprawdza czy żołnież nie uciekł lub nie zginął
// -W konstruktorze można ustawiać parametry jak hp, stress, stress_threshold, stress_on_enemy_death,
//  stress_on_ally_death, stress_decay, speed, inertia

class Soldier : public SimulationEntity {
public:
  //Nie bać się dodawać kolejnych na potrzeby danych formacji/jednostek
  enum Action {
    FORMATION,
    FLEE,
    CHARGE,
    ATTACK,
    CARACOLE,
    LOOSE_FORMATION
  };

  //Zawsze wywoływać Soldier::init w klasach dziedziczących
  void init() override;

  //Zawsze wywowoływać Soldier::update w klasach dziedziczących
  void update() override;

  //Ustawienie pozycji w formacji (przydatne dla oddziału zarządzającego jednostką)
  void setFormationOffset(position_data_t x, position_data_t y);

  //Pobranie pozycij w formacji
  std::pair<position_data_t, position_data_t> getFormationOffset();

  //Ustaw akcję (przydatne dla oddziału zarządzającego jednostką)
  void setAction(Soldier::Action action);

  //Uzyskaj bieżącą akcję
  Soldier::Action getAction();

  //Ustaw oddział
  void setRegiment(Regiment* regiment);

  //Pomaga na zdefiniowanie innej reakcji na zadawane obrażenia
  virtual void takeDamage(Soldier* attacker, float value);


protected:
  //Utworzenie ciała
  virtual std::unique_ptr<Entity> createCorpse();

  //Domyślny kolor ciała
  sf::Color getDefaultCorpseColor();

  const static sf::Color CorpseBlackColor;

  //Pójście w określonym kierunku, zgodnie z atrybutem speed
  void goTo(position_data_t x, position_data_t y, position_data_t speed_multiplier = 1.0);

  //Pójście do celu, zgodnie z atrybutem speed, unikanie kolizji z pobliskimi jednostkami
  void goToFlocking(position_data_t x, position_data_t y, position_data_t attraction_range,
                    position_data_t avoidange_range, position_data_t speed_multiplier = 1.0);

  //Pójście w określonym kierunku, zgodnie z atrybutem speed, unikanie kolizji z pobliskimi jednostkami
  void accelerateFlocking(position_data_t x, position_data_t y, position_data_t attraction_range,
                          position_data_t avoidange_range, position_data_t speed_multiplier = 1.0);

  //Domyślne utrzymywanie formacji
  void defaultKeepingFormation();

  //Domyślne zachowanie uciekania
  void defaultFleeingBehaviour();

  //Domyślne szukanie celu
  Soldier* defaultFindTarget();

  //Domyślne szukanie celu
  Soldier* defaultFindNotFleeingTarget();

  //Domyślny strzał (z broni palnej)
  void defaultShootAt(Soldier* s, double accuracy_distance_drop, position_data_t accuracy_radius_increase,
                      float ranged_max_damage);

  //Unikanie kontaktu - wchodzi w sład defaultFleeingBehviour ale można wykorzystać osobno
  //Zwraca kierunek od lokalnej średniej jednostek
  std::pair<position_data_t, position_data_t> avoidContact(position_data_t radius, position_data_t speed);

  //Atrybuty, które ma każda jednostka - można je ustawiać w konstruktorze
  float hp = 8;
  float stress = 0;
  float stress_threshold = 50;
  float stress_on_enemy_death = -0.9f;
  float stress_on_ally_death = 1.0f;
  float stress_decay = 0.95f;
  position_data_t speed = 0.5;
  position_data_t inertia = 0.85;

  //Pola, z których można korzystać w dziedziczących klasach
  Regiment* regiment = nullptr;
  Action action = FORMATION;
  int detailed_update_cd = 0;
  position_data_t formation_x_offset = 0;
  position_data_t formation_y_offset = 0;
  position_data_t local_enemy_x = 0;
  position_data_t local_enemy_y = 0;


  RandomWrapper<std::minstd_rand> random;

  void die();
  void escape();
  int stress_check_cd = 0;
  bool did_remove_self = false;
};


#endif //WARRIORS_SOLDIER_H
