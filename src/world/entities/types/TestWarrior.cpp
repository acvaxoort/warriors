//
// Created by aleksander on 05.04.19.
//

#include <entitysystem/EntitySystem.h>
#include <util/PlanarGeometryUtil.h>
#include <util/SfmlWindowUtil.h>
#include <world/Formations.h>
#include <iostream>
#include <world/entities/indicators/MeleeIndicator.h>
#include "TestWarrior.h"

TestWarrior::TestWarrior() {
  collision_radius = 0.4;
  hp = 80;
  speed = 0.4;
  inertia = 0.85;
}

void TestWarrior::update() {

  //Wykonywanie bierzącej akcji
  switch (action) {

    //Stanie w formacji, podążanie z formacją, dowodzenie oddziałem
    case FORMATION: {

      //Jeśli oddział nie istnieje (jest nullptr), przejście w tryb szarżowania
      if (!regiment) {
        action = CHARGE;
        detailed_update_cd = 0;
        formation_x_offset = 0;
        formation_y_offset = 0;

      //Jeśli oddział istnieje
      } else {

        //Jeśli ma cel, podązanie za nim
        if (target) {
          goTo(target->pos_x, target->pos_y);
          pointless_chase_counter++;

        //Jeżeli nie ma celu, stanie w formacji
        } else {
          defaultKeepingFormation();
        }

        if (detailed_update_cd == 0) {
          detailed_update_cd = random.randInt(3, 5);

          //Gonienie za pobliskimi wrogami
          position_data_t target_distance_sqr = 16 * 16;
          target = nullptr;
          for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 16)) {
            position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
            if (checked_distance_sqr < target_distance_sqr) {
              if (auto p = dynamic_cast<TestWarrior *>(e)) {
                target = p;
                target_distance_sqr = checked_distance_sqr;
              }
            }
          }

          //Nie oddalanie się za daleko
          position_data_t chase_rad = regiment->approx_radius + 32;
          if (target && pgeom::getDistanceSqr(regiment->pos_x - target->pos_x, regiment->pos_y - target->pos_y)
                        > chase_rad * chase_rad) {
            target = nullptr;
          }
        }
        detailed_update_cd--;
      }
    }
    break;

    //Szarżowanie - atak najbliższej jednostki, nie będąc związany z oddziałem
    case CHARGE: {
      if (detailed_update_cd == 0) {
        seekTarget();
        detailed_update_cd = random.randInt(3, 5);
      }
      detailed_update_cd--;

      if (target) {
        goToFlocking(target->pos_x, target->pos_y, 6, 2);
        pointless_chase_counter++;
      }
    } break;

    //Ucieczka z pola bitwy
    case FLEE: {
      defaultFleeingBehaviour();
    } break;
  }

//Licznik ataku
  if (attackCooldown > 0) {
    attackCooldown--;
  }

//Metoda update klasy bazowej, potrzebna
  Soldier::update();
}

void TestWarrior::forgetAbout(Entity *e) {
  if (e == target) {
    target = nullptr;
  }
}

//Szukaj celu
//pointless_chase_counter powoduje że żołnierz nie będzie zbyt długo gonił uciekającego celu którego nie może dogonić
void TestWarrior::seekTarget() {
  if (pointless_chase_counter < 30) {
    target = defaultFindTarget();
  } else {
    target = defaultFindNotFleeingTarget();
  }
}

//Kolizja - jeśli żołnierz jest gotowy do ataku (minął attackCooldown), atakuj
void TestWarrior::onEntityCollision(Entity& other) {
  if (attackCooldown == 0 && action != FLEE) {
    if (auto p = dynamic_cast<Soldier*>(&other)) {
      if (team != p->team) {
        p->takeDamage(this, random.randFloat(10, 20));
        attackCooldown = 3;
        pointless_chase_counter = 0;
        environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, other));
      }
    }
  }
}

std::unique_ptr<Entity::DisplayData> TestWarrior::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(TestWarrior)
}

std::unique_ptr<Entity> TestWarrior::createCorpse() {
  return std::make_unique<Corpse>(getDefaultCorpseColor(), collision_radius);
}

TestWarrior::DisplayData::DisplayData(const TestWarrior& e)
    : Entity::DisplayData(e),
      radius(e.collision_radius),
      color(e.team->color) {
}

void TestWarrior::DisplayData::display(sf::RenderWindow& window, position_data_t pos_x, position_data_t pos_y,
                                       position_data_t angle) {
  sf::CircleShape shape(radius, 8);
  shape.setFillColor(color);
  shape.setOrigin(radius, radius);
  shape.setPosition(pos_x, pos_y);
  window.draw(shape);
}

TestWarrior::Corpse::Corpse(const sf::Color& color, position_data_t radius): color(color) {
  collision_radius = radius;
}

void TestWarrior::Corpse::update() {
  Entity::update();
}

void TestWarrior::Corpse::onEntityCollision(Entity& other) {
  Entity::onEntityCollision(other);
}

std::unique_ptr<Entity::DisplayData> TestWarrior::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Corpse)
}

TestWarrior::Corpse::DisplayData::DisplayData(const TestWarrior::Corpse& e)
    : Entity::DisplayData(e),
      radius(e.collision_radius),
      color(e.color) {
}

void TestWarrior::Corpse::DisplayData::display(sf::RenderWindow& window, position_data_t pos_x, position_data_t pos_y,
                                  position_data_t angle) {
  sf::CircleShape shape(radius, 8);
  shape.setFillColor(color);
  shape.setOrigin(radius, radius);
  shape.setPosition(pos_x, pos_y);
  window.draw(shape);
}
