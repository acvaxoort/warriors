//
// Created by aleksander on 26.05.19.
//

#include "SoldierTemplate.h"

SoldierTemplate::SoldierTemplate() {
  collision_radius = 0.3;
  hp = 50;
  speed = 0.4;
  inertia = 0.85;
}

void SoldierTemplate::update() {

  switch (action) {
    case FORMATION: {
      if (!regiment) {
        action = CHARGE;
        detailed_update_cd = 0;
        formation_x_offset = 0;
        formation_y_offset = 0;
      } else {
        defaultKeepingFormation();
      }
    } break;
    case CHARGE: {

    } break;
    case FLEE: {
      defaultFleeingBehaviour();
    } break;
    default: {
      action = FORMATION;
    }
  }

  Soldier::update();
}

void SoldierTemplate::forgetAbout(Entity *e) {

}

std::unique_ptr<Entity::DisplayData> SoldierTemplate::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(SoldierTemplate);
}

std::unique_ptr<Entity> SoldierTemplate::createCorpse() {
  CREATE_CORPSE(SoldierTemplate);
}

SoldierTemplate::DisplayData::DisplayData(const SoldierTemplate &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void SoldierTemplate::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                   position_data_t angle) {

}

SoldierTemplate::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> SoldierTemplate::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(SoldierTemplate::Corpse);
}

SoldierTemplate::Corpse::DisplayData::DisplayData(const SoldierTemplate::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void SoldierTemplate::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {

}
