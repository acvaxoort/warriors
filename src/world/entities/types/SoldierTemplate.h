//
// Created by aleksander on 26.05.19.
//

#ifndef WARRIORS_SOLDIERTEMPLATE_H
#define WARRIORS_SOLDIERTEMPLATE_H

#include <world/entities/types/Soldier.h>

class SoldierTemplate : public Soldier {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const SoldierTemplate &e);
    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
  private:
    sf::Color color;
  };

  SoldierTemplate();
  void update() override;
  void forgetAbout(Entity *e) override;
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

protected:
  std::unique_ptr<Entity> createCorpse() override;


  class Corpse : public Entity {
  public:
    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);
      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
    private:
      sf::Color color;
    };

    explicit Corpse(const sf::Color& color);
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  private:
    sf::Color color;
  };
};


#endif //WARRIORS_SOLDIERTEMPLATE_H
