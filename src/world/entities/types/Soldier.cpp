//
// Created by aleksander on 21.05.19.
//

#include "Soldier.h"
#include <util/SfmlWindowUtil.h>
#include <world/Formations.h>
#include <world/entities/indicators/RangedIndicator.h>
#include <world/entities/indicators/RangedMissedIndicator.h>


//Ta funkcja jest wywoływana po włożeniu jednostki do systemu
//Można odwołać się do system i environment
void Soldier::init() {
  random.seed(static_cast<uint_fast32_t>(getId() + environment->seed));
}

void Soldier::update() {
//Jeśli hp spadło do zera, zgon
  if (hp < 0) {
    die();
  }

//Jeśli sprawdzenie poziomu stresu/paniki
  if (stress_check_cd == 0) {
    if (stress > stress_threshold && action != FLEE) {
      int local_enemy_count = 0;
      int local_ally_count = 0;
      for (Entity* e : entitiesAdjacentRadius(pos_x, pos_y, 32)) {
        if (auto p = dynamic_cast<Soldier*>(e)) {
          if (team != p->team) {
            local_enemy_count++;
          } else {
            local_ally_count++;
          }
        }
      }
      if (local_enemy_count > 0) {
        //Jeśli w dookoła jest dużo sojuszników, potrzeba większego poziomu stresu/paniki żeby zainicjować ucieczkę
        if (local_ally_count == 0 || (double) local_enemy_count / local_ally_count > stress_threshold / stress) {
          action = FLEE;
          detailed_update_cd = 0;
          if (regiment) {
            regiment->removeSoldier(this);
          }
        }
      }
    }
    stress_check_cd = random.randInt(5, 10);
  }
  stress_check_cd--;

//Spadek stresu/paniki z czasem
  stress *= stress_decay;

//Bezwładność
  vel_x *= inertia;
  vel_y *= inertia;

//Sprawdzanie czy żołnierz uciekł poza granice mapy
  if ((pos_x < collision_radius && vel_x < 0) || (pos_x > (environment->getWidth() - collision_radius) && vel_x > 0)) {
    if (action == FLEE) {
      escape();
    } else {
      vel_x = -vel_x;
    }
  }

  if ((pos_y < collision_radius && vel_y < 0) || (pos_y > (environment->getHeight() - collision_radius) && vel_y > 0)) {
    if (action == FLEE) {
      escape();
    } else {
      vel_y = -vel_y;
    }
  }

//Metoda update klasy bazowej, potrzebna
  Entity::update();
}



void Soldier::takeDamage(Soldier* attacker, float value) {
  hp -= value;
}



std::unique_ptr<Entity> Soldier::createCorpse() {
  return nullptr;
}

sf::Color Soldier::getDefaultCorpseColor() {
  return sfmlwutil::interpolate(team->color, sf::Color(220, 220, 220), 0.8);
}

const sf::Color Soldier::CorpseBlackColor = sf::Color(176, 176, 176);

void Soldier::goTo(position_data_t x, position_data_t y, position_data_t speed_multiplier) {
  position_data_t dx = x - pos_x;
  position_data_t dy = y - pos_y;
  if (dx == 0 && dy == 0) {
    return;
  }
  position_data_t dpos = pgeom::getDistance(dx, dy);
  position_data_t mul = speed_multiplier * speed / dpos;
  if (dpos < speed) {
    mul *= dpos / 0.5;
  }
  accelerate(dx*mul, dy*mul);
}

void Soldier::goToFlocking(position_data_t x, position_data_t y, position_data_t attraction_range,
                           position_data_t avoidange_range, position_data_t speed_multiplier) {
  const position_data_t attr_weight = 0.2;
  const position_data_t avoid_weight = 1.0;
  const position_data_t heading_weight = 0.6;

  position_data_t attr_x = 0;
  position_data_t attr_y = 0;
  position_data_t attr_sum = 0;
  position_data_t avoid_x = 0;
  position_data_t avoid_y = 0;
  position_data_t avoid_sum = 0;
  position_data_t heading_x = 0;
  position_data_t heading_y = 0;
  position_data_t heading_sum = 0;
  position_data_t attraction_sqr = attraction_range*attraction_range;
  position_data_t avoidange_sqr = avoidange_range*avoidange_range;
  for (Entity* e : alliesAdjacentRadius(pos_x, pos_y, 8)) {
    position_data_t av_dx = e->pos_x - pos_x;
    position_data_t av_dy = e->pos_y - pos_y;
    position_data_t dist_sqr = pgeom::getDistanceSqr(av_dx, av_dy);
    if (dist_sqr < attraction_sqr && dist_sqr > 1e-10) {
      position_data_t dist = std::sqrt(dist_sqr);
      position_data_t dist_weight = 1.0 / (1.0 + dist);
      position_data_t weight_with_inv = dist_weight / dist;
      attr_x += av_dx * weight_with_inv;
      attr_y += av_dy * weight_with_inv;
      attr_sum += dist_weight;
      position_data_t vel_value = pgeom::getDistance(e->vel_x, e->vel_y);
      if (e->vel_x != 0 || e->vel_y != 0) {
        heading_x += e->vel_x * speed / vel_value * dist_weight;
        heading_y += e->vel_y * speed / vel_value * dist_weight;
      }
      heading_sum += dist_weight;
      if (dist_sqr < avoidange_sqr) {
        avoid_x -= av_dx * weight_with_inv;
        avoid_y -= av_dy * weight_with_inv;
        avoid_sum += dist_weight;
      }
    }
  }
  position_data_t total_weight = 0;
  position_data_t total_x = 0;
  position_data_t total_y = 0;
  position_data_t dx = x - pos_x;
  position_data_t dy = y - pos_y;
  if (dx != 0 && dy != 0) {
    position_data_t dpos = pgeom::getDistance(dx, dy);
    position_data_t mul = speed_multiplier * speed / dpos;
    if (dpos < speed) {
      mul *= dpos / 0.5;
    }
    total_x += dx*mul;
    total_y += dy*mul;
    total_weight += 1.0;
  }
  if (attr_sum > 0) {
    total_x += attr_x * attr_weight / attr_sum;
    total_y += attr_y * attr_weight / attr_sum;
    total_weight += attr_weight;
  }
  if (avoid_sum > 0) {
    total_x += avoid_x * avoid_weight / avoid_sum;
    total_y += avoid_y * avoid_weight / avoid_sum;
    total_weight += avoid_weight;
  }
  if (heading_sum > 0) {
    total_x += heading_x * heading_weight / heading_sum;
    total_y += heading_y * heading_weight / heading_sum;
    total_weight += heading_weight;
  }
  if (total_weight > 0) {
    accelerate(total_x / total_weight, total_y / total_weight);
  }
}

void Soldier::accelerateFlocking(position_data_t x, position_data_t y, position_data_t attraction_range,
                                 position_data_t avoidange_range, position_data_t speed_multiplier) {
  const position_data_t attr_weight = 0.2;
  const position_data_t avoid_weight = 1.0;
  const position_data_t heading_weight = 0.6;

  position_data_t attr_x = 0;
  position_data_t attr_y = 0;
  position_data_t attr_sum = 0;
  position_data_t avoid_x = 0;
  position_data_t avoid_y = 0;
  position_data_t avoid_sum = 0;
  position_data_t heading_x = 0;
  position_data_t heading_y = 0;
  position_data_t heading_sum = 0;
  position_data_t attraction_sqr = attraction_range*attraction_range;
  position_data_t avoidange_sqr = avoidange_range*avoidange_range;
  for (Entity* e : alliesAdjacentRadius(pos_x, pos_y, 8)) {
    position_data_t av_dx = e->pos_x - pos_x;
    position_data_t av_dy = e->pos_y - pos_y;
    position_data_t dist_sqr = pgeom::getDistanceSqr(av_dx, av_dy);
    if (dist_sqr < attraction_sqr && dist_sqr > 1e-10) {
      position_data_t dist = std::sqrt(dist_sqr);
      position_data_t dist_weight = 1.0 / (1.0 + dist);
      position_data_t weight_with_inv = dist_weight / dist;
      attr_x += av_dx * weight_with_inv;
      attr_y += av_dy * weight_with_inv;
      attr_sum += dist_weight;
      position_data_t vel_value = pgeom::getDistance(e->vel_x, e->vel_y);
      heading_x += e->vel_x * speed / vel_value * dist_weight;
      heading_y += e->vel_y * speed / vel_value * dist_weight;
      heading_sum += dist_weight;
      if (dist_sqr < avoidange_sqr) {
        avoid_x -= av_dx * weight_with_inv;
        avoid_y -= av_dy * weight_with_inv;
        avoid_sum += dist_weight;
      }
    }
  }
  position_data_t total_weight = 0;
  position_data_t total_x = 0;
  position_data_t total_y = 0;
  if (x != 0 && y != 0) {
    position_data_t dpos = pgeom::getDistance(x, y);
    position_data_t mul = speed_multiplier * speed / dpos;
    if (dpos < speed) {
      mul *= dpos / 0.5;
    }
    total_x += x*mul;
    total_y += y*mul;
    total_weight += 1.0;
  }
  if (attr_sum > 0) {
    total_x += attr_x * attr_weight / attr_sum;
    total_y += attr_y * attr_weight / attr_sum;
    total_weight += attr_weight;
  }
  if (avoid_sum > 0) {
    total_x += avoid_x * avoid_weight / avoid_sum;
    total_y += avoid_y * avoid_weight / avoid_sum;
    total_weight += avoid_weight;
  }
  if (heading_sum > 0) {
    total_x += heading_x * heading_weight / heading_sum;
    total_y += heading_y * heading_weight / heading_sum;
    total_weight += heading_weight;
  }
  if (total_weight > 0) {
    accelerate(total_x / total_weight, total_y / total_weight);
  }
}

void Soldier::setFormationOffset(position_data_t x, position_data_t y) {
  formation_x_offset = x;
  formation_y_offset = y;
}

std::pair<position_data_t, position_data_t> Soldier::getFormationOffset() {
  return {formation_x_offset, formation_y_offset};
}

void Soldier::setAction(Soldier::Action action) {
  if (this->action == FLEE) {
    return;
  }
  this->action = action;
  this->detailed_update_cd = 0;
}

Soldier::Action Soldier::getAction() {
  return action;
}

void Soldier::setRegiment(Regiment* regiment) {
  this->regiment = regiment;
}

void Soldier::defaultKeepingFormation() {
  auto expected = Formation::positionInFormation(formation_x_offset , formation_y_offset,
                                                 regiment->pos_x, regiment->pos_y,
                                                 regiment->angle_sin, regiment->angle_cos);

  bool is_in_good_position = pgeom::getDistanceSqr(pos_x - expected.first, pos_y - expected.second) < 1;
  if (is_in_good_position) {
    position_data_t follow_speed_diff_x = regiment->vel_x - vel_x;
    position_data_t follow_speed_diff_y = regiment->vel_y - vel_y;
    accelerate(follow_speed_diff_x, follow_speed_diff_y);
  }
  goTo(expected.first, expected.second);
}

void Soldier::defaultFleeingBehaviour() {
  if (detailed_update_cd <= 0) {
    //Uciekanie od średniej pozycji pobliskich wrogów
    auto local_enemies = avoidContact(48, speed);
    if (local_enemies.first != 0 && local_enemies.second != 0) {
      local_enemy_x = local_enemies.first;
      local_enemy_y = local_enemies.second;
    }
    //Jeśli obecnie nie ma kierunku do uciekania, uciekanie do najbliższej krawędzi mapy
    if (local_enemy_x == 0 && local_enemy_y == 0) {
      int shortest = 0;
      position_data_t len = pos_x;
      if (pos_y < len) {
        shortest = 1;
        len = pos_y;
      }
      if (environment->getWidth() - pos_x < len) {
        shortest = 2;
        len = environment->getWidth() - pos_x;
      }
      if (environment->getHeight() - pos_y < len) {
        shortest = 3;
      }
      switch (shortest) {
        case 0: local_enemy_x = -speed; break;
        case 1: local_enemy_y = -speed; break;
        case 2: local_enemy_x = speed; break;
        case 3: local_enemy_y = speed; break;
      }
    }
    detailed_update_cd = random.randInt(4, 6);
  }
  accelerate(local_enemy_x, local_enemy_y);
  detailed_update_cd--;
}

Soldier* Soldier::defaultFindTarget() {
  position_data_t target_distance_sqr = 1e300;
  Soldier* temp_target = nullptr;
  for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 64)) {
    position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
    if (checked_distance_sqr < target_distance_sqr) {
      if (auto p = dynamic_cast<Soldier*>(e)) {
        temp_target = p;
        target_distance_sqr = checked_distance_sqr;
      }
    }
  }
  if (!temp_target) {
    for (Entity* e : enemiesAll()) {
      if (auto p = dynamic_cast<Soldier*>(e)) {
        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
        if (checked_distance_sqr < target_distance_sqr) {
          temp_target = p;
          target_distance_sqr = checked_distance_sqr;
        }
      }
    }
  }
  return temp_target;
}

Soldier* Soldier::defaultFindNotFleeingTarget() {
  position_data_t target_distance_sqr = 1e300;
  Soldier* temp_target = nullptr;
  for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 64)) {
    if (auto p = dynamic_cast<Soldier *>(e)) {
      if (p->action != FLEE) {
        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
        if (checked_distance_sqr < target_distance_sqr) {
          temp_target = p;
          target_distance_sqr = checked_distance_sqr;
        }
      }
    }
  }
  if (!temp_target) {
    for (Entity* e : enemiesAll()) {
      if (auto p = dynamic_cast<Soldier*>(e)) {
        if (p->action != FLEE) {
          position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
          if (checked_distance_sqr < target_distance_sqr) {
            temp_target = p;
            target_distance_sqr = checked_distance_sqr;
          }
        }
      }
    }
  }
  return temp_target;
}

void Soldier::defaultShootAt(Soldier* s, double accuracy_distance_drop, position_data_t accuracy_radius_increase,
                             float ranged_max_damage) {
  position_data_t dist_sqr = pgeom::getDistanceSqr(pos_x - s->pos_x, pos_y - s->pos_y);
  position_data_t dist = std::sqrt(dist_sqr);
  double target_speed = pgeom::getDistance(s->vel_x, s->vel_y);
  double chance_to_hit = 1.0 / (1.0 + accuracy_distance_drop * dist * (1.0 + target_speed));
  if (chance_to_hit > random.randDouble()) {
    if (dist < 10) {
      s->takeDamage(this, random.randFloat(ranged_max_damage*0.5f, ranged_max_damage));
      environment->indicators.addEntity(std::make_unique<RangedIndicator>(*this, *s));
      return;
    } else {
      position_data_t dist_sqrt = std::sqrt(dist);
      position_data_t potential_radius = 0.5*std::sqrt(accuracy_radius_increase*dist_sqrt);
      position_data_t random_offset = random.randDouble(potential_radius);
      position_data_t random_angle = random.randDouble(M_PI*2);
      position_data_t random_angle_sin = std::sin(random_angle);
      position_data_t random_angle_cos = std::cos(random_angle);
      position_data_t alt_target_x = s->pos_x + random_angle_cos*random_offset - random_angle_sin*random_offset;
      position_data_t alt_target_y = s->pos_y + random_angle_sin*random_offset + random_angle_cos*random_offset;
      for (Entity* e : entitiesAdjacentRadius(s->pos_x, s->pos_y, potential_radius)) {
        if (e->colllidesWithPoint(alt_target_x, alt_target_y)) {
          if (auto another = dynamic_cast<Soldier*>(e)) {
            another->takeDamage(this, random.randFloat(ranged_max_damage*0.5f, ranged_max_damage));
            environment->indicators.addEntity(std::make_unique<RangedIndicator>(*this, *another));
            return;
          }
        }
      }
    }
  }
  environment->indicators.addEntity(std::make_unique<RangedMissedIndicator>(*this, *s));
}

std::pair<position_data_t, position_data_t> Soldier::avoidContact(position_data_t radius, position_data_t speed) {
  position_data_t local_enemy_middle_x = 0;
  position_data_t local_enemy_middle_y = 0;
  int local_enemy_count = 0;
  for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, radius)) {
    if (pgeom::getDistanceSqr(e->pos_x - pos_x, e->pos_y - pos_y) > radius*radius) {
      continue;
    }
    if (auto p = dynamic_cast<Soldier*>(e)) {
      if (team != p->team) {
        local_enemy_middle_x += p->pos_x;
        local_enemy_middle_y += p->pos_y;
        local_enemy_count++;
      }
    }
  }
  if (local_enemy_count > 0) {
    local_enemy_middle_x /= local_enemy_count;
    local_enemy_middle_y /= local_enemy_count;
    position_data_t dx = pos_x - local_enemy_middle_x;
    position_data_t dy = pos_y - local_enemy_middle_y;
    position_data_t dpos = pgeom::getDistance(dx, dy);
    position_data_t mul = speed / dpos;
    return {dx * mul, dy * mul};
  }
  return {0, 0};
}

//Jak zginie - utworzenie ciała, rozwiązanie oddziału, wpływ na psychikę okolicznych jednostek
void Soldier::die() {
  if (!did_remove_self) {
    removeSelf();
    team->notifyMemberDeath();
    if (regiment) {
      regiment->removeSoldier(this);
    }
    did_remove_self = true;
  }

  if (auto corpse = createCorpse()) {
    corpse->pos_x = pos_x;
    corpse->pos_y = pos_y;
    corpse->angle = angle;
    environment->corpses.addEntity(std::move(corpse));
  }

  for (Entity* e : entitiesAdjacentRadius(pos_x, pos_y, random.randInt(16, 64))) {
    if (auto p = dynamic_cast<Soldier*>(e)) {
      if (team != p->team) {
        p->stress += p->stress_on_enemy_death;
      } else {
        p->stress += p->stress_on_ally_death;
      }
    }
  }
}

//Jak znajdzie się poza granicami mapy uciekając - rozwiązanie oddziału
void Soldier::escape() {
  if (!did_remove_self) {
    removeSelf();
    team->notifyMemberEscape();
    if (regiment) {
      regiment->removeSoldier(this);
    }
    did_remove_self = true;
  }
}
