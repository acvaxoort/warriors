//
// Created by aleksander on 31.05.19.
//

#include <world/regiments/types/kokenhausen/PikeAndShootLine.h>
#include <world/entities/indicators/CannonIndicator.h>
#include <world/regiments/types/kokenhausen/CannonLine.h>
#include <util/RenderShapes.h>
#include "Cannon.h"

Cannon::Cannon() {
  collision_radius = 0.3;
  hp = 50;
  speed = 0.4;
  inertia = 0.85;
  stress_threshold = 30;
}

void Cannon::update() {
  switch (action) {
    case FORMATION: {
      if (!regiment) {
        action = FLEE;
        detailed_update_cd = 0;
        formation_x_offset = 0;
        formation_y_offset = 0;
      } else {
        defaultKeepingFormation();
        if (ranged_delay == 0) {
          Regiment* reg_target = nullptr;
          Soldier* temp_target = nullptr;
          if (auto reg = dynamic_cast<CannonLine*>(regiment)) {
            reg_target = reg->target;
          }
          if (reg_target) {
            size_t target_size = reg_target->soldiers.size();
            if (target_size > 0) {
              Entity* e = reg_target->soldiers[random.randInt(target_size)];
              temp_target = dynamic_cast<Soldier*>(e);
            }
          } else {
            temp_target = defaultFindTarget();
          }
          if (temp_target) {
            ranged_delay = RANGED_COOLDOWN;
            shootAt(temp_target);
          }
        }

        angle = regiment->angle;
      }
    } break;
    case FLEE: {
      //Porzucenie armoaty
      if (!did_remove_self) {
        removeSelf();
        team->notifyMemberEscape();
        if (regiment) {
          regiment->removeSoldier(this);
        }
        did_remove_self = true;
      }
      if (auto corpse = createCorpse()) {
        corpse->pos_x = pos_x;
        corpse->pos_y = pos_y;
        corpse->angle = angle;
        environment->corpses.addEntity(std::move(corpse));
      }
    } break;
    default: {
      action = FORMATION;
    }
  }

  if (ranged_delay > 0) {
    ranged_delay--;
  }

  Soldier::update();
}

void Cannon::shootAt(Soldier *s) {
  const position_data_t EFFECT_RADIUS = 1.0;
  position_data_t dist_sqr = pgeom::getDistanceSqr(pos_x - s->pos_x, pos_y - s->pos_y);
  position_data_t dist = std::sqrt(dist_sqr);
  position_data_t random_offset = random.randDouble(1.0);
  position_data_t random_angle = random.randDouble(M_PI*2);
  position_data_t random_angle_sin = std::sin(random_angle);
  position_data_t random_angle_cos = std::cos(random_angle);
  position_data_t alt_target_x = s->pos_x + random_angle_cos*random_offset - random_angle_sin*random_offset;
  position_data_t alt_target_y = s->pos_y + random_angle_sin*random_offset + random_angle_cos*random_offset;

  for (Entity* e : entitiesAdjacentRadius(alt_target_x, alt_target_y, EFFECT_RADIUS)) {
    if (pgeom::getDistanceSqr(e->pos_x - alt_target_x, e->pos_y - alt_target_y) < EFFECT_RADIUS * EFFECT_RADIUS) {
      if (auto another = dynamic_cast<Soldier*>(e)) {
        if (random.randDouble() < 0.5) {
          another->takeDamage(this, random.randFloat(0.0f, RANGED_MAX_DAMAGE));
        }
      }
    }
  }
  environment->indicators.addEntity(std::make_unique<CannonIndicator>(*this, alt_target_x, alt_target_y, 1.0));
}

void Cannon::forgetAbout(Entity *e) {
  if (target == e) {
    target = nullptr;
    detailed_update_cd = 0;
  }
}

std::unique_ptr<Entity::DisplayData> Cannon::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Cannon);
}

std::unique_ptr<Entity> Cannon::createCorpse() {
  CREATE_CORPSE(Cannon);
}

Cannon::DisplayData::DisplayData(const Cannon &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void Cannon::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                     position_data_t angle) {
  rendershapes::infantry_base.setFillColor(color);
  rendershapes::infantry_base.setPosition(pos_x, pos_y);
  rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
  rendershapes::cannon.setFillColor(sf::Color::Black);
  rendershapes::cannon.setPosition(pos_x, pos_y);
  rendershapes::cannon.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::infantry_base);
  window.draw(rendershapes::cannon);
}

Cannon::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> Cannon::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Cannon::Corpse);
}

Cannon::Corpse::DisplayData::DisplayData(const Cannon::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void Cannon::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                             position_data_t angle) {
  rendershapes::infantry_base.setFillColor(color);
  rendershapes::infantry_base.setPosition(pos_x, pos_y);
  rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
  rendershapes::cannon.setFillColor(CorpseBlackColor);
  rendershapes::cannon.setPosition(pos_x, pos_y);
  rendershapes::cannon.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::infantry_base);
  window.draw(rendershapes::cannon);
}
