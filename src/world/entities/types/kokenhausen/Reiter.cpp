//
// Created by aleksander on 24.05.19.
//

#include <world/regiments/types/kokenhausen/ReiterRegiment.h>
#include <util/PlanarGeometryUtil.h>
#include <world/entities/indicators/RangedMissedIndicator.h>
#include <world/entities/indicators/RangedIndicator.h>
#include <world/entities/indicators/MeleeIndicator.h>
#include <util/RenderShapes.h>
#include "Reiter.h"

Reiter::Reiter() {
  collision_radius = 0.5;
  speed = 0.9;
  hp = 90;
  inertia = 0.87;
  stress_threshold = 30;
}

void Reiter::update() {

  switch (action) {
    case FORMATION: {
      if (!regiment) {
        setAction(CHARGE);
      } else {
        defaultKeepingFormation();
      }
    } break;

    case CARACOLE: {
      if (!regiment) {
        setAction(CHARGE);
      } else {
        if (auto reg = dynamic_cast<ReiterRegiment*>(regiment)) {
          if (current_waypoint > 2) {
            current_waypoint = 0;
            action = FORMATION;
          } else {
            auto& wp = reg->rows_data[row].waypoints[current_waypoint];
            if (current_waypoint == 2) {
              if (ranged_delay == 0) {
                Regiment* reg_target = nullptr;
                Soldier* temp_target = nullptr;
                if (random.randInt(2) == 0) {
                  reg_target = reg->target;
                }
                if (reg_target) {
                  size_t target_size = reg_target->soldiers.size();
                  if (target_size > 0) {
                    Entity* e = reg_target->soldiers[random.randInt(target_size)];
                    temp_target = dynamic_cast<Soldier*>(e);
                  }
                } else {
                  temp_target = defaultFindTarget();
                }
                if (temp_target) {
                  ranged_delay = RANGED_COOLDOWN;
                  ranged_secondary_delay = RANGED_SECOND_SHOT_DELAY;
                  shootAt(temp_target);
                }
              }
            }

            goTo(wp.first + reg->vel_x, wp.second + reg->vel_y);
            if (pgeom::getDistanceSqr(pos_x - wp.first, pos_y - wp.second) < 4*4) {
              current_waypoint++;
            }
          }
        }
      }
    } break;

    case CHARGE: {
      if (ranged_delay <= 10) {
        if (target) {
          if (pgeom::getDistanceSqr(pos_x - target->pos_x, pos_y - target->pos_y) > 16*16) {
            goToFlocking(target->pos_x, target->pos_y, 8, 3);
          } else {
            if (ranged_delay == 0) {
              ranged_delay = RANGED_COOLDOWN;
              ranged_secondary_delay = RANGED_SECOND_SHOT_DELAY;
              shootAt(target);
            }
            accelerateFlocking(local_enemy_x, local_enemy_y, 8, 3);
          }
        }
      } else if (local_enemy_x != 0 || local_enemy_y != 0) {
        accelerateFlocking(local_enemy_x, local_enemy_y, 8, 3, 0.75); // reloading = slower
      } else if (target) {
        goToFlocking(target->pos_x, target->pos_y, 8, 3, 0.75); // reloading = slower
      }
      if (detailed_update_cd <= 0) {
        detailed_update_cd = random.randInt(3, 5);
        auto local_enemy = avoidContact(24, 0.5);
        local_enemy_x = local_enemy.first;
        local_enemy_y = local_enemy.second;
        if (ranged_delay <= 10 || !target) {
          target = defaultFindTarget();
        }
      }
      detailed_update_cd--;
    } break;

    case FLEE: {
      defaultFleeingBehaviour();
    } break;

    default:
      setAction(FORMATION);
  }

  if (action != FLEE) {
    if (melee_delay > 0) {
      melee_delay--;
    }

    if (ranged_delay > 0) {
      ranged_delay--;
    }

    if (ranged_secondary_delay > 0) {
      ranged_secondary_delay--;
      if (ranged_secondary_delay == 0) {
        if (target) {
          shootAt(target);
        } else if (Soldier* s = defaultFindTarget()) {
          shootAt(s);
        }
      }
    }

    if (melee_delay == 0) {
      for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 2)) {
        if (pgeom::getDistanceSqr(e->pos_x - pos_x, e->pos_y - pos_y) > 4) {
          continue;
        }
        if (auto s = dynamic_cast<Soldier*>(e)) {
          s->takeDamage(this, random.randFloat(MELEE_MAX_DAMAGE*0.5f, MELEE_MAX_DAMAGE));
          environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, *s));
          melee_delay = MELEE_COOLDOWN;
        }
      }
    }
  }

  angle = std::atan2(vel_y, vel_x);

  Soldier::update();
}

void Reiter::shootAt(Soldier *s) {
  defaultShootAt(s, 0.04, 0.15, RANGED_MAX_DAMAGE);
}

std::unique_ptr<Entity::DisplayData> Reiter::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Reiter);
}

std::unique_ptr<Entity> Reiter::createCorpse() {
  CREATE_CORPSE(Reiter);
}

void Reiter::forgetAbout(Entity *e) {
  if (target == e) {
    target = nullptr;
    detailed_update_cd = 0;
  }
}

Reiter::DisplayData::DisplayData(const Reiter &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void Reiter::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                         position_data_t angle) {
  rendershapes::cavalry_base.setFillColor(color);
  rendershapes::cavalry_base.setPosition(pos_x, pos_y);
  rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
  rendershapes::musketeer.setFillColor(sf::Color::Black);
  rendershapes::musketeer.setPosition(pos_x, pos_y);
  rendershapes::musketeer.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::cavalry_base);
  window.draw(rendershapes::musketeer);
}

Reiter::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> Reiter::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Reiter::Corpse);
}

Reiter::Corpse::DisplayData::DisplayData(const Reiter::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void Reiter::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                          position_data_t angle) {
  rendershapes::cavalry_base.setFillColor(color);
  rendershapes::cavalry_base.setPosition(pos_x, pos_y);
  rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
  rendershapes::musketeer.setFillColor(CorpseBlackColor);
  rendershapes::musketeer.setPosition(pos_x, pos_y);
  rendershapes::musketeer.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::cavalry_base);
  window.draw(rendershapes::musketeer);
}
