//
// Created by aleksander on 26.05.19.
//

#include <util/PlanarGeometryUtil.h>
#include <world/entities/indicators/MeleeIndicator.h>
#include <util/RenderShapes.h>
#include "Pikeman.h"

Pikeman::Pikeman() {
  collision_radius = 0.3;
  hp = 75;
  speed = 0.4;
  inertia = 0.85;
  stress_threshold = 30;
}

void Pikeman::update() {

  switch (action) {
    case FORMATION: {
      if (!regiment) {
        action = FLEE;
        detailed_update_cd = 0;
        formation_x_offset = 0;
        formation_y_offset = 0;
      } else {
        defaultKeepingFormation();
        angle = regiment->angle;
      }
    } break;
    case CHARGE: {
      if (detailed_update_cd == 0) {
        target = defaultFindTarget();
        detailed_update_cd = random.randInt(3, 5);
      }
      detailed_update_cd--;
      if (target) {
        goToFlocking(target->pos_x, target->pos_y, 6, 2);
      }
      angle = std::atan2(vel_y, vel_x);
    } break;
    case FLEE: {
      defaultFleeingBehaviour();
      angle = std::atan2(vel_y, vel_x);
    } break;
    default: {
      action = FORMATION;
    }
  }

  if (action != FLEE) {
    if (melee_delay > 0) {
      melee_delay--;
    }

    if (melee_delay == 0) {
      for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 5)) {
        if (pgeom::getDistanceSqr(e->pos_x - pos_x, e->pos_y - pos_y) > 5*5) {
          continue;
        }
        if (auto s = dynamic_cast<Soldier*>(e)) {
          position_data_t attacker_angle = std::atan2(s->vel_y, s->vel_x);
          position_data_t attacker_speed = pgeom::getDistance(s->vel_x, s->vel_y);
          position_data_t damage_mult = 1.0;
          if (action == FORMATION) {
            damage_mult -= 24.0 * attacker_speed * std::cos(angle - attacker_angle);
          }
          if (damage_mult < 1.0) { damage_mult = 1.0; }
          float dmg_val = (float) damage_mult * random.randFloat(MELEE_MAX_DAMAGE*0.5f, MELEE_MAX_DAMAGE);
          s->takeDamage(this, dmg_val);
          environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, *s));
          if (damage_mult > 10) {
            melee_delay = 1;
          } else {
            melee_delay = MELEE_COOLDOWN;
          }
        }
      }
    }
  }

  Soldier::update();
}

void Pikeman::takeDamage(Soldier* attacker, float value) {
  if (pgeom::getDistanceSqr(attacker->pos_x - pos_x, attacker->pos_y - pos_y) < 16) {
    position_data_t attacker_angle = std::atan2(attacker->vel_y, attacker->vel_x);
    float mult = std::max(0.0f, (float)-std::cos(angle - attacker_angle));
    value -= pgeom::getDistance(attacker->vel_x, attacker->vel_y) * 10 * mult;
  }
  if (value > 0) {
    hp -= value;
  }
}

void Pikeman::forgetAbout(Entity *e) {
  if (target == e) {
    target = nullptr;
    detailed_update_cd = 0;
  }
}

std::unique_ptr<Entity::DisplayData> Pikeman::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Pikeman);
}

std::unique_ptr<Entity> Pikeman::createCorpse() {
  CREATE_CORPSE(Pikeman);
}

Pikeman::DisplayData::DisplayData(const Pikeman &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void Pikeman::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                   position_data_t angle) {
  rendershapes::infantry_base.setFillColor(color);
  rendershapes::infantry_base.setPosition(pos_x, pos_y);
  rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
  rendershapes::pikeman.setFillColor(sf::Color::Black);
  rendershapes::pikeman.setPosition(pos_x, pos_y);
  rendershapes::pikeman.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::infantry_base);
  window.draw(rendershapes::pikeman);
}

Pikeman::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> Pikeman::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Pikeman::Corpse);
}

Pikeman::Corpse::DisplayData::DisplayData(const Pikeman::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void Pikeman::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
  rendershapes::infantry_base.setFillColor(color);
  rendershapes::infantry_base.setPosition(pos_x, pos_y);
  rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
  rendershapes::pikeman.setFillColor(CorpseBlackColor);
  rendershapes::pikeman.setPosition(pos_x, pos_y);
  rendershapes::pikeman.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::infantry_base);
  window.draw(rendershapes::pikeman);
}
