//
// Created by aleksander on 26.05.19.
//

#ifndef WARRIORS_PIKEMAN_H
#define WARRIORS_PIKEMAN_H

#include <world/entities/types/Soldier.h>

class Pikeman : public Soldier {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const Pikeman &e);
    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
  private:
    sf::Color color;
  };

  Pikeman();
  void update() override;
  void forgetAbout(Entity *e) override;
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

  void takeDamage(Soldier* attacker, float value) override;

protected:
  std::unique_ptr<Entity> createCorpse() override;

  Soldier* target = nullptr;

  const float MELEE_MAX_DAMAGE = 15;
  const uint16_t MELEE_COOLDOWN = 6;
  uint16_t melee_delay = 0;

  class Corpse : public Entity {
  public:
    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);
      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
    private:
      sf::Color color;
    };

    explicit Corpse(const sf::Color& color);
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  private:
    sf::Color color;
  };
};


#endif //WARRIORS_PIKEMAN_H
