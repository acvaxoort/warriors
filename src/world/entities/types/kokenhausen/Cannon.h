//
// Created by aleksander on 31.05.19.
//

#ifndef WARRIORS_CANNON_H
#define WARRIORS_CANNON_H

#include <world/entities/types/Soldier.h>

class Cannon : public Soldier {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const Cannon &e);
    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
  private:
    sf::Color color;
  };

  Cannon();
  void update() override;
  void forgetAbout(Entity *e) override;
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

protected:
  std::unique_ptr<Entity> createCorpse() override;

  void shootAt(Soldier* s);

  Soldier* target = nullptr;

  position_data_t local_enemy_x = 0;
  position_data_t local_enemy_y = 0;

  const float RANGED_MAX_DAMAGE = 200;
  const uint16_t RANGED_COOLDOWN = 240;
  uint16_t ranged_delay = 0;

  class Corpse : public Entity {
  public:
    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);
      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
    private:
      sf::Color color;
    };

    explicit Corpse(const sf::Color& color);
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  private:
    sf::Color color;
  };
};

#endif //WARRIORS_CANNON_H
