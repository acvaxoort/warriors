//
// Created by aleksander on 30.05.19.
//

#include <world/regiments/types/kokenhausen/HussarRegiment.h>
#include <world/entities/indicators/MeleeIndicator.h>
#include <world/entities/indicators/RangedIndicator.h>
#include <world/entities/indicators/RangedMissedIndicator.h>
#include <util/RenderShapes.h>
#include "Hussar.h"

Hussar::Hussar() {
  collision_radius = 0.5;
  speed = 0.95;
  hp = 130;
  inertia = 0.87;
  stress_threshold = 30;
}

void Hussar::update() {

  switch (action) {
    case FORMATION: {
      if (!regiment) {
        action = CHARGE;
        detailed_update_cd = 0;
        formation_x_offset = 0;
        formation_y_offset = 0;
      } else {
        defaultKeepingFormation();
      }
    } break;

    case LOOSE_FORMATION: {
      if (!regiment) {
        action = CHARGE;
        detailed_update_cd = 0;
        formation_x_offset = 0;
        formation_y_offset = 0;
      } else {
        if (auto reg = dynamic_cast<HussarRegiment*>(regiment)) {
          if (reg->target) {
            goToFlocking(reg->loose_target_x, reg->loose_target_y, 10, 4, 0.75);
          } else {
            defaultKeepingFormation();
          }
        } else {
          defaultKeepingFormation();
        }
      }
      if (detailed_update_cd == 0) {
        target = defaultFindTarget();
        detailed_update_cd = random.randInt(3, 5);
      }
      if (target) {
        if (pgeom::getDistanceSqr(pos_x - target->pos_x, pos_y - target->pos_y) < 8*8) {
          if (ranged_delay == 0) {
            ranged_delay = RANGED_COOLDOWN;
            shootAt(target);
          }
        }
      }
    } break;

    case CHARGE: {
      if (detailed_update_cd == 0) {
        target = defaultFindTarget();
        detailed_update_cd = random.randInt(3, 5);
      }
      detailed_update_cd--;
      if (target) {
        goToFlocking(target->pos_x, target->pos_y, 10, 4);
        if (pgeom::getDistanceSqr(pos_x - target->pos_x, pos_y - target->pos_y) < 8*8) {
          if (ranged_delay == 0) {
            ranged_delay = RANGED_COOLDOWN;
            shootAt(target);
          }
        }
      }
    } break;

    case FLEE: {
      defaultFleeingBehaviour();
    } break;
    default: {
      action = FORMATION;
    }
  }

  if (action != FLEE) {
    if (melee_delay > 0) {
      melee_delay--;
    }

    if (ranged_delay > 0) {
      ranged_delay--;
    }

    if (melee_delay == 0) {
      for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 2)) {
        if (pgeom::getDistanceSqr(e->pos_x - pos_x, e->pos_y - pos_y) > 2*2) {
          continue;
        }
        if (auto s = dynamic_cast<Soldier*>(e)) {
          float damage = random.randFloat(MELEE_MAX_DAMAGE*0.5f, MELEE_MAX_DAMAGE);
          if (charge_bonus_delay == 0) {
            damage *= 2;
            charge_bonus_delay = CHARGE_BONUS_COOLDOWN;
          }
          s->takeDamage(this, damage);
          environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, *s));
          melee_delay = MELEE_COOLDOWN;
        }
      }
    }

    if (charge_bonus_delay > 0) {
      charge_bonus_delay--;
    }
  }

  angle = std::atan2(vel_y, vel_x);

  Soldier::update();
}

void Hussar::forgetAbout(Entity *e) {
  if (target == e) {
    target = nullptr;
    detailed_update_cd = 0;
  }
}

void Hussar::shootAt(Soldier *s) {
  defaultShootAt(s, 0.04, 0.18, RANGED_MAX_DAMAGE);
}
std::unique_ptr<Entity::DisplayData> Hussar::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Hussar);
}

std::unique_ptr<Entity> Hussar::createCorpse() {
  CREATE_CORPSE(Hussar);
}

void Hussar::takeDamage(Soldier* attacker, float value) {
  if (pgeom::getDistanceSqr(attacker->pos_x - pos_x, attacker->pos_y - pos_y) < 16) {
    value -= pgeom::getDistance(vel_x, vel_y) * 10;
  }
  if (value > 0) {
    hp -= value;
  }
}

Hussar::DisplayData::DisplayData(const Hussar &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void Hussar::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
  rendershapes::cavalry_base.setFillColor(color);
  rendershapes::cavalry_base.setPosition(pos_x, pos_y);
  rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
  rendershapes::hussar.setFillColor(sf::Color::Black);
  rendershapes::hussar.setPosition(pos_x, pos_y);
  rendershapes::hussar.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::cavalry_base);
  window.draw(rendershapes::hussar);
}

Hussar::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> Hussar::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Hussar::Corpse);
}

Hussar::Corpse::DisplayData::DisplayData(const Hussar::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void Hussar::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                                   position_data_t angle) {
  rendershapes::cavalry_base.setFillColor(color);
  rendershapes::cavalry_base.setPosition(pos_x, pos_y);
  rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
  rendershapes::hussar.setFillColor(CorpseBlackColor);
  rendershapes::hussar.setPosition(pos_x, pos_y);
  rendershapes::hussar.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::cavalry_base);
  window.draw(rendershapes::hussar);
}