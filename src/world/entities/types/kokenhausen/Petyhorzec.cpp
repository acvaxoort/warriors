//
// Created by aleksander on 31.05.19.
//

#include <util/RenderShapes.h>
#include "Petyhorzec.h"

Petyhorzec::Petyhorzec() {
  collision_radius = 0.5;
  speed = 0.95;
  hp = 110;
  inertia = 0.87;
  stress_threshold = 30;
  RANGED_MAX_DAMAGE = 100;
  RANGED_COOLDOWN = 120;
  MELEE_MAX_DAMAGE = 60;
  MELEE_COOLDOWN = 6;
}

std::unique_ptr<Entity::DisplayData> Petyhorzec::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Petyhorzec);
}

std::unique_ptr<Entity> Petyhorzec::createCorpse() {
  CREATE_CORPSE(Petyhorzec);
}

Petyhorzec::DisplayData::DisplayData(const Petyhorzec &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void Petyhorzec::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
  rendershapes::cavalry_base.setFillColor(color);
  rendershapes::cavalry_base.setPosition(pos_x, pos_y);
  rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
  rendershapes::petyhorzec.setFillColor(sf::Color::Black);
  rendershapes::petyhorzec.setPosition(pos_x, pos_y);
  rendershapes::petyhorzec.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::cavalry_base);
  window.draw(rendershapes::petyhorzec);
}

Petyhorzec::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> Petyhorzec::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Petyhorzec::Corpse);
}

Petyhorzec::Corpse::DisplayData::DisplayData(const Petyhorzec::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void Petyhorzec::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                                   position_data_t angle) {
  rendershapes::cavalry_base.setFillColor(color);
  rendershapes::cavalry_base.setPosition(pos_x, pos_y);
  rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
  rendershapes::petyhorzec.setFillColor(CorpseBlackColor);
  rendershapes::petyhorzec.setPosition(pos_x, pos_y);
  rendershapes::petyhorzec.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::cavalry_base);
  window.draw(rendershapes::petyhorzec);
}
