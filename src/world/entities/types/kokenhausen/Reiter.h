//
// Created by aleksander on 24.05.19.
//

#ifndef WARRIORS_SWEDISHREITER_H
#define WARRIORS_SWEDISHREITER_H

#include <world/entities/types/Soldier.h>

class Reiter : public Soldier {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const Reiter &e);

    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

  private:
    sf::Color color;
  };

  Reiter();
  void update() override;
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  void forgetAbout(Entity *e) override;

  uint8_t row = 0;

protected:
  std::unique_ptr<Entity> createCorpse() override;

  void shootAt(Soldier *s);

  Soldier* target = nullptr;
  uint8_t current_waypoint = 0;

  const float RANGED_MAX_DAMAGE = 80;
  const uint16_t RANGED_COOLDOWN = 210;
  const uint16_t RANGED_SECOND_SHOT_DELAY = 5;
  uint16_t ranged_delay = 0;
  uint16_t ranged_secondary_delay = 0;

  const float MELEE_MAX_DAMAGE = 20;
  const uint16_t MELEE_COOLDOWN = 6;
  uint16_t melee_delay = 0;

  class Corpse : public Entity {
  public:
    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);
      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

    private:
      sf::Color color;
    };

    Corpse(const sf::Color& color);
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  private:
    sf::Color color;
  };

};

#endif //WARRIORS_SWEDISHREITER_H
