//
// Created by aleksander on 26.05.19.
//

#include <util/PlanarGeometryUtil.h>
#include <world/entities/indicators/MeleeIndicator.h>
#include <world/entities/indicators/RangedIndicator.h>
#include <world/entities/indicators/RangedMissedIndicator.h>
#include <world/regiments/types/kokenhausen/PikeAndShootLine.h>
#include <util/RenderShapes.h>
#include "Musketeer.h"

Musketeer::Musketeer() {
  collision_radius = 0.3;
  hp = 50;
  speed = 0.4;
  inertia = 0.85;
  stress_threshold = 30;
}

void Musketeer::update() {

  switch (action) {
    case FORMATION: {
      if (!regiment) {
        action = FLEE;
        detailed_update_cd = 0;
        formation_x_offset = 0;
        formation_y_offset = 0;
      } else {
        defaultKeepingFormation();
        if (ranged_delay == 0) {
          Regiment* reg_target = nullptr;
          Soldier* temp_target = nullptr;
          if (random.randInt(2) == 0) {
            if (auto reg = dynamic_cast<PikeAndShootLine*>(regiment)) {
              reg_target = reg->target;
            }
          }
          if (reg_target) {
            size_t target_size = reg_target->soldiers.size();
            if (target_size > 0) {
              Entity* e = reg_target->soldiers[random.randInt(target_size)];
              temp_target = dynamic_cast<Soldier*>(e);
            }
          } else {
            temp_target = defaultFindTarget();
          }
          if (temp_target) {
            ranged_delay = RANGED_COOLDOWN;
            shootAt(temp_target);
          }
        }

        angle = regiment->angle;
      }
    } break;
    case CHARGE: {
      if (ranged_delay <= 10) {
        if (target) {
          if (pgeom::getDistanceSqr(pos_x - target->pos_x, pos_y - target->pos_y) > 16*16) {
            goToFlocking(target->pos_x, target->pos_y, 8, 3);
          } else {
            if (ranged_delay == 0) {
              ranged_delay = RANGED_COOLDOWN;
              shootAt(target);
            }
            accelerateFlocking(local_enemy_x, local_enemy_y, 8, 3);
          }
        }
      } else if (local_enemy_x != 0 || local_enemy_y != 0) {
        accelerateFlocking(local_enemy_x, local_enemy_y, 8, 3);
      } else if (target) {
        goToFlocking(target->pos_x, target->pos_y, 8, 3);
      }
      if (detailed_update_cd <= 0) {
        detailed_update_cd = random.randInt(3, 5);
        auto local_enemy = avoidContact(24, 0.5);
        local_enemy_x = local_enemy.first;
        local_enemy_y = local_enemy.second;
        if (ranged_delay <= 10 || !target) {
          target = defaultFindTarget();
        }
      }
      detailed_update_cd--;
      angle = std::atan2(vel_y, vel_x);
    } break;
    case FLEE: {
      defaultFleeingBehaviour();
      angle = std::atan2(vel_y, vel_x);
    } break;
    default: {
      action = FORMATION;
    }
  }

  if (action != FLEE) {
    if (melee_delay > 0) {
      melee_delay--;
    }

    if (ranged_delay > 0) {
      ranged_delay--;
    }

    if (melee_delay == 0) {
      for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 1.5)) {
        if (pgeom::getDistanceSqr(e->pos_x - pos_x, e->pos_y - pos_y) > 1.5*1.5) {
          continue;
        }
        if (auto s = dynamic_cast<Soldier*>(e)) {
          s->takeDamage(this, random.randFloat(MELEE_MAX_DAMAGE*0.5f, MELEE_MAX_DAMAGE));
          environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, *s));
          melee_delay = MELEE_COOLDOWN;
        }
      }
    }
  }

  angle = std::atan2(vel_y, vel_x);

  Soldier::update();
}

void Musketeer::shootAt(Soldier *s) {
  defaultShootAt(s, 0.024, 0.12, RANGED_MAX_DAMAGE);
}

void Musketeer::forgetAbout(Entity *e) {
  if (target == e) {
    target = nullptr;
    detailed_update_cd = 0;
  }
}

std::unique_ptr<Entity::DisplayData> Musketeer::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Musketeer);
}

std::unique_ptr<Entity> Musketeer::createCorpse() {
  CREATE_CORPSE(Musketeer);
}

Musketeer::DisplayData::DisplayData(const Musketeer &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void Musketeer::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                   position_data_t angle) {
  rendershapes::infantry_base.setFillColor(color);
  rendershapes::infantry_base.setPosition(pos_x, pos_y);
  rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
  rendershapes::musketeer.setFillColor(sf::Color::Black);
  rendershapes::musketeer.setPosition(pos_x, pos_y);
  rendershapes::musketeer.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::infantry_base);
  window.draw(rendershapes::musketeer);
}

Musketeer::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> Musketeer::Corpse::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(Musketeer::Corpse);
}

Musketeer::Corpse::DisplayData::DisplayData(const Musketeer::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void Musketeer::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
  rendershapes::infantry_base.setFillColor(color);
  rendershapes::infantry_base.setPosition(pos_x, pos_y);
  rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
  rendershapes::musketeer.setFillColor(CorpseBlackColor);
  rendershapes::musketeer.setPosition(pos_x, pos_y);
  rendershapes::musketeer.setRotation(angle * 180 / M_PI);
  window.draw(rendershapes::infantry_base);
  window.draw(rendershapes::musketeer);
}
