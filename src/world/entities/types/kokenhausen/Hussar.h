//
// Created by aleksander on 30.05.19.
//

#ifndef WARRIORS_HUSSAR_H
#define WARRIORS_HUSSAR_H

#include <world/entities/types/Soldier.h>

class Hussar : public Soldier {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const Hussar &e);
    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
  private:
    sf::Color color;
  };

  Hussar();
  void update() override;
  void forgetAbout(Entity *e) override;
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

  void takeDamage(Soldier* attacker, float value) override;

protected:
  std::unique_ptr<Entity> createCorpse() override;

  Soldier* target = nullptr;

  void shootAt(Soldier* s);

  float RANGED_MAX_DAMAGE = 80;
  uint16_t RANGED_COOLDOWN = 120;
  uint16_t ranged_delay = 0;

  float MELEE_MAX_DAMAGE = 60;
  uint16_t MELEE_COOLDOWN = 6;
  uint16_t melee_delay = 0;

  uint16_t CHARGE_BONUS_COOLDOWN = 60;
  uint16_t charge_bonus_delay = 0;


  class Corpse : public Entity {
  public:
    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);
      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
    private:
      sf::Color color;
    };

    explicit Corpse(const sf::Color& color);
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  private:
    sf::Color color;
  };
};

#endif //WARRIORS_HUSSAR_H
