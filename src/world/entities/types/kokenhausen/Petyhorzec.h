//
// Created by aleksander on 31.05.19.
//

#ifndef WARRIORS_PETYHORZEC_H
#define WARRIORS_PETYHORZEC_H

#include "Hussar.h"

class Petyhorzec : public Hussar {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const Petyhorzec &e);
    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
  private:
    sf::Color color;
  };

  Petyhorzec();
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

protected:
  std::unique_ptr<Entity> createCorpse() override;

  class Corpse : public Entity {
  public:
    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);
      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
    private:
      sf::Color color;
    };

    explicit Corpse(const sf::Color& color);
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  private:
    sf::Color color;
  };
};

#endif //WARRIORS_PETYHORZEC_H
