//
// Created by aleksander on 26.05.19.
//

#ifndef WARRIORS_MUSKETEER_H
#define WARRIORS_MUSKETEER_H

#include <world/entities/types/Soldier.h>

class Musketeer : public Soldier {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const Musketeer &e);
    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
  private:
    sf::Color color;
  };

  Musketeer();
  void update() override;
  void forgetAbout(Entity *e) override;
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

protected:
  std::unique_ptr<Entity> createCorpse() override;

  void shootAt(Soldier* s);

  Soldier* target = nullptr;

  position_data_t local_enemy_x = 0;
  position_data_t local_enemy_y = 0;

  const float RANGED_MAX_DAMAGE = 100;
  const uint16_t RANGED_COOLDOWN = 80;
  uint16_t ranged_delay = 0;

  const float MELEE_MAX_DAMAGE = 10;
  const uint16_t MELEE_COOLDOWN = 10;
  uint16_t melee_delay = 0;

  class Corpse : public Entity {
  public:
    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);
      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
    private:
      sf::Color color;
    };

    explicit Corpse(const sf::Color& color);
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
  private:
    sf::Color color;
  };
};

#endif //WARRIORS_MUSKETEER_H
