//
// Created by aleksander on 05.04.19.
//

#include <entitysystem/EntitySystem.h>
#include <util/PlanarGeometryUtil.h>
#include <util/SfmlWindowUtil.h>
#include <world/Formations.h>
#include <iostream>
#include <world/entities/indicators/MeleeIndicator.h>
#include <util/RenderShapes.h>
#include <world/regiments/types/leuctra/Falange.h>
#include <world/regiments/types/leuctra/CavalryRegiment.h>
#include "SacredBandSoldier.h"


SacredBandSoldier::SacredBandSoldier() {
    collision_radius = 0.4;
    speed *= 0.4;
    stress_threshold = 200;
    hp = 150;
    inertia = 0.85;
}

void SacredBandSoldier::update() {

    //Wykonywanie bierzącej akcji
    switch (action) {

        //Stanie w formacji, podążanie z formacją, dowodzenie oddziałem
        case FORMATION: {
            if(this->regiment)
                defaultKeepingFormation();
            else
            {
                action = CHARGE;
            }

/*
            //Jeśli oddział nie istnieje (jest nullptr), przejście w tryb szarżowania
            if (!regiment) {
                action = CHARGE;
                detailed_update_cd = 0;

                //Jeśli oddział istnieje
            } else {

                //Jeśli ma cel, podązanie za nim
                if (target) {
                    goTo(target->pos_x, target->pos_y,speed);
                    pointless_chase_counter++;

                    //Jeżeli nie ma celu, stanie w formacji
                } else {
                    defaultKeepingFormation();
                }

                if (detailed_update_cd == 0) {
                    detailed_update_cd = random.randInt(3, 5);

                    //Gonienie za pobliskimi wrogami
                    position_data_t target_distance_sqr = 10*10;
                    target = nullptr;
                    for (Entity *e : enemiesAdjacentRadius(pos_x, pos_y, 10)) {
                        position_data_t checked_distance_sqr = fabs(pos_x - e->pos_x);
                        if (checked_distance_sqr < target_distance_sqr) {
                            if (auto p = dynamic_cast<SacredBandSoldier *>(e)) {
                                target = p;
                                target_distance_sqr = checked_distance_sqr;
                                if(auto f = dynamic_cast<Falange* >(this->regiment))
                                {
                                   // f->orderToCharge();
                                    f->currently_fighting = p->regiment;
                                }

                            }
                        }
                    }

                    //Nie oddalanie się za daleko
                    position_data_t chase_rad = regiment->approx_radius + 32;
                    if (target &&
                        pgeom::getDistanceSqr(regiment->pos_x - target->pos_x, regiment->pos_y - target->pos_y)
                        > chase_rad * chase_rad) {
                        target = nullptr;
                    }
                }

                detailed_update_cd--;
            }*/
        }
            break;

            //Szarżowanie - atak najbliższej jednostki, nie będąc związany z oddziałem
        case CHARGE:
            if (detailed_update_cd == 0) {
                seekTarget();
                detailed_update_cd = random.randInt(3, 5);
            }
            detailed_update_cd--;

            if (target) {
                goTo(target->pos_x, target->pos_y);
                pointless_chase_counter++;
            }
            angle = std::atan2(vel_y, vel_x);
            break;

            //Ucieczka z pola bitwy
        case FLEE: {
            defaultFleeingBehaviour();
            angle = std::atan2(vel_y, vel_x);
        }break;
        default: {
            action = FORMATION;
        }
    }
    if (action != FLEE) {
        if (attackCooldown > 0) {
            attackCooldown--;
        }

        if (attackCooldown == 0) {
            for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, range_weapon)) {
                if (pgeom::getDistanceSqr(e->pos_x - pos_x, e->pos_y - pos_y) > range_weapon) {
                    continue;
                }
                if (auto s = dynamic_cast<Soldier*>(e)) {
                    int allies = 1;
                    for(auto & reg :team->regiments)
                    {
                        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                                     pos_y - reg->pos_y);

                        if (checked_distance_sqr < 100 * 100) {
                            if(dynamic_cast<CavalryRegiment *>(reg.get()))
                                allies++;
                        }
                    }
                    if(auto p = dynamic_cast<Falange *>(this->regiment)){
                        if(p->fal_id==1 || p->fal_id==5){
                            s->takeDamage(this, random.randFloat(allies*(MAX_DAMAGE+3)*0.5f, (allies*MAX_DAMAGE+3)));
                        }else{
                            s->takeDamage(this, random.randFloat(MAX_DAMAGE * allies*0.5f,MAX_DAMAGE * allies));
                        }
                    }
                    environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, *s));
                    attackCooldown = 5;
                }
            }
        }
    }

    // Atak falangi - pierwszy rząd atakuje reszta czeka
    //case ATTACK:




//Licznik ataku
    if (attackCooldown > 0) {
        attackCooldown--;
    }

//Metoda update klasy bazowej, potrzebna
    Soldier::update();
}

void SacredBandSoldier::forgetAbout(Entity *e) {
    if (e == target) {
        target = nullptr;
    }
}

//Szukaj celu
//pointless_chase_counter powoduje że żołnierz nie będzie zbyt długo gonił uciekającego celu którego nie może dogonić

void SacredBandSoldier::seekTarget() {
    if (pointless_chase_counter < 30) {
        target = defaultFindTarget();
    } else {
        target = defaultFindNotFleeingTarget();
    }
}
/*void SacredBandSoldier::seekTarget() {
    position_data_t target_distance_sqr = 1e300;
    target = nullptr;
    if (pointless_chase_counter < 30) {
        for (Entity* e : enemiesAdjacentRadiusSquare(pos_x, pos_y, 64)) {
            position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
            if (checked_distance_sqr < target_distance_sqr) {
                if (auto p = dynamic_cast<Soldier*>(e)) {
                    target = p;
                    target_distance_sqr = checked_distance_sqr;
                }
            }
        }
        if (!target) {
            for (Entity* e : enemiesOutwardsOuter(pos_x, pos_y, 64, environment->getWidth())) {
                if (auto p = dynamic_cast<Soldier*>(e)) {
                    target = p;
                    break;
                }
            }
        }
    } else {
        for (Entity* e : enemiesAdjacentRadiusSquare(pos_x, pos_y, 64)) {
            position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
            if (checked_distance_sqr < target_distance_sqr) {
                if (auto p = dynamic_cast<Soldier*>(e)) {
                    if (p->action != FLEE) {
                        target = p;
                        target_distance_sqr = checked_distance_sqr;
                    }
                }
            }
        }
        if (!target) {
            for (Entity* e : enemiesOutwardsOuter(pos_x, pos_y, 64, environment->getWidth())) {
                if (auto p = dynamic_cast<Soldier*>(e)) {
                    if (p->action != FLEE) {
                        target = p;
                        break;
                    }
                }
            }
        }
    }
}*/

//Kolizja - jeśli żołnierz jest gotowy do ataku (minął attackCooldown), atakuj
void SacredBandSoldier::onEntityCollision(Entity &other) {
    if (attackCooldown == 0 && action != FLEE) {
        if (auto p = dynamic_cast<Soldier *>(&other)) {
            if (team != p->team) {
                inellasticCollision(other);
               // environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, other));
            }
        }
    }
}

std::unique_ptr<Entity::DisplayData> SacredBandSoldier::outputDisplayData() {
    OUTPUT_DISPLAY_DATA(SacredBandSoldier);
}

std::unique_ptr<Entity> SacredBandSoldier::createCorpse() {
    CREATE_CORPSE(SacredBandSoldier);
}

SacredBandSoldier::DisplayData::DisplayData(const SacredBandSoldier& e)
        : Entity::DisplayData(e),
          radius(e.collision_radius),
          color(sf::Color::Green){}



void SacredBandSoldier::DisplayData::display(sf::RenderWindow& window, position_data_t pos_x, position_data_t pos_y,
                                   position_data_t angle) {
    rendershapes::infantry_base.setFillColor(color);
    rendershapes::infantry_base.setPosition(pos_x, pos_y);
    rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
    window.draw(rendershapes::infantry_base);
}

SacredBandSoldier::Corpse::Corpse(const sf::Color &color)
        : color(color) {}

std::unique_ptr<Entity::DisplayData> SacredBandSoldier::Corpse::outputDisplayData() {
    OUTPUT_DISPLAY_DATA(SacredBandSoldier::Corpse);
}

SacredBandSoldier::Corpse::DisplayData::DisplayData(const SacredBandSoldier::Corpse &e)
        : Entity::DisplayData(e),
          color(e.color) {}

void SacredBandSoldier::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
    rendershapes::infantry_base.setFillColor(color);
    rendershapes::infantry_base.setPosition(pos_x, pos_y);
    rendershapes::infantry_base.setRotation(angle * 180 / M_PI);
    window.draw(rendershapes::infantry_base);
}

void SacredBandSoldier::takeDamage(Soldier* attacker, float value) {
    if(attacker->angle-this->angle>1.2 && attacker->angle-this->angle<1.9){
        hp-=1.5*value;
    }else{
        hp-=value;
    }
}
