//
// Created by kuba on 16.06.19.
//

#ifndef WARRIORS_SPARTANELITEUNIT_H
#define WARRIORS_SPARTANELITEUNIT_H


#include <world/entities/types/Soldier.h>
#include <util/RandomWrapper.hpp>

class SpartanEliteUnit : public Soldier {

    class DisplayData : public Entity::DisplayData {
    public:
        explicit DisplayData(const SpartanEliteUnit &e);

        void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

    private:
        position_data_t radius;
        sf::Color color;

    };

public:
    SpartanEliteUnit();
    void takeDamage(Soldier* attacker, float value) override;
    void update() override;
    void forgetAbout(Entity *e) override;
    void onEntityCollision(Entity& other) override;
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
    const float MAX_DAMAGE=10;
protected:
    std::unique_ptr<Entity> createCorpse() override;

    Soldier* target = nullptr;
    int attackCooldown = 0;
    int pointless_chase_counter = 0;
    position_data_t range_weapon = 1;

    void seekTarget();

    class Corpse : public Entity {
    public:
        class DisplayData : public Entity::DisplayData {
        public:
            explicit DisplayData(const Corpse &e);
            void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;
        private:
            sf::Color color;
        };

        explicit Corpse(const sf::Color& color);
        std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
    private:
        sf::Color color;
    };
};

#endif //WARRIORS_SPARTANELITEUNIT_H
