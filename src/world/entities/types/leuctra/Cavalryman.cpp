//
// Created by kuba on 28.05.19.
//

#include <entitysystem/EntitySystem.h>
#include <util/PlanarGeometryUtil.h>
#include <util/SfmlWindowUtil.h>
#include <world/Formations.h>
#include <iostream>
#include <world/entities/indicators/MeleeIndicator.h>
#include <world/regiments/types/DefaultRegiment.h>
#include <util/RenderShapes.h>
#include "world/entities/types/TestWarrior.h"
#include "Cavalryman.h"
#include "Hoplite.h"

Cavalryman::Cavalryman() {
    speed *= 0.6;
    collision_radius = 0.5;
    stress_threshold = 100;
    hp = 130;
    inertia = 0.87;

}

void Cavalryman::speedBoost(float boost) {
    speed *= boost;
}

std::unique_ptr<Entity::DisplayData> Cavalryman::outputDisplayData() {
    OUTPUT_DISPLAY_DATA(Cavalryman);
}

std::unique_ptr<Entity> Cavalryman::createCorpse() {
    CREATE_CORPSE(Cavalryman);
}

void Cavalryman::update() {

    //Wykonywanie bierzącej akcji
    switch (action) {


        //Stanie w formacji, podążanie z formacją, dowodzenie oddziałem
        case FORMATION: {
            if(this->regiment)
                defaultKeepingFormation();
            else
            {
                action = CHARGE;
            }


            //Jeśli oddział nie istnieje (jest nullptr), przejście w tryb szarżowania
            /*if (!regiment) {
                action = CHARGE;
                detailed_update_cd = 0;
                formation_x_offset = 0;
                formation_y_offset = 0;

                //Jeśli oddział istnieje
            } else {

                //Jeśli ma cel, podązanie za nim
                if (target) {
                    goTo(target->pos_x, target->pos_y);
                    pointless_chase_counter++;

                    //Jeżeli nie ma celu, stanie w formacji
                } else {
                    defaultKeepingFormation();
                }

                if (detailed_update_cd == 0) {
                    detailed_update_cd = random.randInt(3, 5);

                    //Gonienie za pobliskimi wrogami
                    position_data_t target_distance_sqr = 16 * 16;
                    target = nullptr;
                    for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, 16)) {
                        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - e->pos_x, pos_y - e->pos_y);
                        if (checked_distance_sqr < target_distance_sqr) {
                            if (auto p = dynamic_cast<Cavalryman *>(e)) {
                                target = p;
                                target_distance_sqr = checked_distance_sqr;
                            }
                        }
                    }

                    //Nie oddalanie się za daleko
                    position_data_t chase_rad = regiment->approx_radius + 32;
                    if (target && pgeom::getDistanceSqr(regiment->pos_x - target->pos_x, regiment->pos_y - target->pos_y)
                                  > chase_rad * chase_rad) {
                        target = nullptr;
                    }
                }
                detailed_update_cd--;
            }*/
        }
            break;

            //Szarżowanie - atak najbliższej jednostki, nie będąc związany z oddziałem
        case CHARGE: {
            if (detailed_update_cd == 0) {
                target = defaultFindTarget();
                detailed_update_cd = random.randInt(3, 5);
            }
            detailed_update_cd--;

            if (target) {
                goToFlocking(target->pos_x, target->pos_y, 6, 2);
                pointless_chase_counter++;
            }
            angle = std::atan2(vel_y, vel_x);
        } break;

            //Ucieczka z pola bitwy
        case FLEE: {
            defaultFleeingBehaviour();
            angle = std::atan2(vel_y, vel_x);
        } break;
        default: {
            action = FORMATION;
        }
    }
    if (action != FLEE) {
        if (attackCooldown > 0) {
            attackCooldown--;
        }

        if (attackCooldown == 0) {
            for (Entity* e : enemiesAdjacentRadius(pos_x, pos_y, range_weapon)) {
                if (pgeom::getDistanceSqr(e->pos_x - pos_x, e->pos_y - pos_y) > range_weapon*range_weapon) {
                    continue;
                }
                if (auto s = dynamic_cast<Soldier*>(e)) {
                    position_data_t attacker_angle = std::atan2(s->vel_y, s->vel_x);
                 //   position_data_t attacker_speed = pgeom::getDistance(s->vel_x, s->vel_y);
                    position_data_t damage_mult = 1 + 2 * speed;
                    if (damage_mult < 1.0) { damage_mult = 1.0; }
                    /*if(auto x = dynamic_cast<Hoplite*>(s)){
                        std::cout<<this->angle-x->angle<<"\n";
                    }*/
                    s->takeDamage(this, random.randFloat((float)damage_mult*(MAX_DAMAGE+3)*0.5f, (float)(damage_mult*MAX_DAMAGE+3)));
                    environment->indicators.addEntity(std::make_unique<MeleeIndicator>(*this, *s));
                    attackCooldown = 5;
                }
            }
        }
    }

//Metoda update klasy bazowej, potrzebna
    Soldier::update();
}

Cavalryman::DisplayData::DisplayData(const Cavalryman &e)
    : Entity::DisplayData(e),
      color(e.team->color) {}

void Cavalryman::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
    rendershapes::cavalry_base.setFillColor(color);
    rendershapes::cavalry_base.setPosition(pos_x, pos_y);
    rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
    window.draw(rendershapes::cavalry_base);
}

Cavalryman::Corpse::Corpse(const sf::Color &color)
    : color(color) {}

std::unique_ptr<Entity::DisplayData> Cavalryman::Corpse::outputDisplayData() {
    OUTPUT_DISPLAY_DATA(Cavalryman::Corpse);
}

Cavalryman::Corpse::DisplayData::DisplayData(const Cavalryman::Corpse &e)
    : Entity::DisplayData(e),
      color(e.color) {}

void Cavalryman::Corpse::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                                   position_data_t angle) {
    rendershapes::cavalry_base.setFillColor(color);
    rendershapes::cavalry_base.setPosition(pos_x, pos_y);
    rendershapes::cavalry_base.setRotation(angle * 180 / M_PI);
    window.draw(rendershapes::cavalry_base);
}
void Cavalryman::takeDamage(Soldier* attacker, float value) {
    if(attacker->angle-this->angle>1.2 && attacker->angle-this->angle<1.9){
        hp-=1.5*value;
    }else{
        hp-=value;
    }
}

