//
// Created by kuba on 28.05.19.
//

#ifndef WARRIORS_CAVALRYMAN_H
#define WARRIORS_CAVALRYMAN_H


#include "world/entities/types/TestWarrior.h"

class Cavalryman : public Soldier {
public:
    class DisplayData : public Entity::DisplayData {
    public:
        explicit DisplayData(const Cavalryman &e);

        void
        display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

    private:
        sf::Color color;
    };

    Cavalryman();

    void update() override;

    void speedBoost(float boost);

    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;
    const float MAX_DAMAGE=15;
protected:
    std::unique_ptr<Entity> createCorpse() override;


    int pointless_chase_counter = 0;
    Soldier *target = nullptr;
    position_data_t range_weapon = 1;
    int attackCooldown;


    class Corpse : public Entity {
    public:
        class DisplayData : public Entity::DisplayData {
        public:
            explicit DisplayData(const Corpse &e);

            void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                         position_data_t angle) override;

        private:
            sf::Color color;
        };

        explicit Corpse(const sf::Color &color);

        std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

    private:
        sf::Color color;
    };

    void takeDamage(Soldier *attacker, float value) override;
};


#endif //WARRIORS_CAVALRYMAN_H
