//
// Created by aleksander on 05.04.19.
//

#ifndef WARRIORS_TESTWARRIOR_H
#define WARRIORS_TESTWARRIOR_H

#include <world/entities/types/Soldier.h>

class TestWarrior : public Soldier {

  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const TestWarrior &e);

    void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

  private:
    position_data_t radius;
    sf::Color color;
  };

public:
  TestWarrior();
  void update() override;
  void forgetAbout(Entity *e) override;
  void onEntityCollision(Entity& other) override;
  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

protected:
  std::unique_ptr<Entity> createCorpse() override;
  Soldier* target = nullptr;
  int attackCooldown = 0;
  int pointless_chase_counter = 0;

  void seekTarget();

  class Corpse : public Entity {

    class DisplayData : public Entity::DisplayData {
    public:
      explicit DisplayData(const Corpse &e);

      void display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

    private:
      position_data_t radius;
      sf::Color color;
    };

  public:
    Corpse(const sf::Color& color, position_data_t radius);

    void update() override;
    void onEntityCollision(Entity& other) override;
    std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

  private:
    sf::Color color;
  };
};


#endif //WARRIORS_TESTWARRIOR_H
