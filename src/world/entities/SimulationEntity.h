//
// Created by aleksander on 15.02.19.
//

#ifndef WARRIORS_INGAMEENTITY_H
#define WARRIORS_INGAMEENTITY_H


#include <entitysystem/Entity.h>
#include <world/Team.h>
#include <world/environment/Environment.h>
#include <entitysystem/EntitySystemCollection.h>

class SimulationEntity : public Entity {
public:
  Environment* environment;
  SimulationEntitySystem* collection;
  size_t team_id;
  Team* team;

  void removeSelf() override;

  //Iteratory przestrzenne, dla lepszego opisu, zobacz klasę EntitySystem

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::Adjacent>
  entitiesAdjacent(position_data_t pos_x, position_data_t pos_y) const {
    return collection->adjacent(pos_x, pos_y);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius>
  entitiesAdjacentRadius(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const {
    return collection->adjacentRadius(pos_x, pos_y, radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare>
  entitiesAdjacentRadiusSquare(position_data_t pos_x, position_data_t pos_y,
                       position_data_t radius) const {
    return collection->adjacentRadiusSquare(pos_x, pos_y, radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter>
  entitiesOutwardsOuter(position_data_t pos_x, position_data_t pos_y, position_data_t inner_radius,
                position_data_t outer_radius) const {
    return collection->outwardsOuter(pos_x, pos_y, inner_radius, outer_radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<EntitySystem::OutwardsInner>
  entitiesOutwardsInnerSorted(position_data_t pos_x, position_data_t pos_y,
                      position_data_t inner_radius) const {
    return collection->outwardsInnerSorted(pos_x, pos_y, inner_radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<EntitySystem::All>
  entitiesAll() const {
    return collection->all();
  }
  
  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::Adjacent>
  alliesAdjacent(position_data_t pos_x, position_data_t pos_y) const {
    return collection->adjacentIn(team_id, pos_x, pos_y);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius>
  alliesAdjacentRadius(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const {
    return collection->adjacentRadiusIn(team_id, pos_x, pos_y, radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare>
  alliesAdjacentRadiusSquare(position_data_t pos_x, position_data_t pos_y,
                             position_data_t radius) const {
    return collection->adjacentRadiusSquareIn(team_id, pos_x, pos_y, radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter>
  alliesOutwardsOuter(position_data_t pos_x, position_data_t pos_y, position_data_t inner_radius,
                      position_data_t outer_radius) const {
    return collection->outwardsOuterIn(team_id, pos_x, pos_y, inner_radius, outer_radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<EntitySystem::OutwardsInner>
  alliesOutwardsInnerSorted(position_data_t pos_x, position_data_t pos_y,
                            position_data_t inner_radius) const {
    return collection->outwardsInnerSortedIn(team_id, pos_x, pos_y, inner_radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<EntitySystem::All>
  alliesAll() const {
    return collection->allIn(team_id);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::Adjacent>
  enemiesAdjacent(position_data_t pos_x, position_data_t pos_y) const {
    return collection->adjacentExcept(team_id, pos_x, pos_y);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadius>
  enemiesAdjacentRadius(position_data_t pos_x, position_data_t pos_y, position_data_t radius) const {
    return collection->adjacentRadiusExcept(team_id, pos_x, pos_y, radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::AdjacentRadiusSquare>
  enemiesAdjacentRadiusSquare(position_data_t pos_x, position_data_t pos_y,
                             position_data_t radius) const {
    return collection->adjacentRadiusSquareExcept(team_id, pos_x, pos_y, radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<SpatialIndexGrid::OutwardsOuter>
  enemiesOutwardsOuter(position_data_t pos_x, position_data_t pos_y, position_data_t inner_radius,
                      position_data_t outer_radius) const {
    return collection->outwardsOuterExcept(team_id, pos_x, pos_y, inner_radius, outer_radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<EntitySystem::OutwardsInner>
  enemiesOutwardsInnerSorted(position_data_t pos_x, position_data_t pos_y,
                            position_data_t inner_radius) const {
    return collection->outwardsInnerSortedExcept(team_id, pos_x, pos_y, inner_radius);
  }

  inline SimulationEntitySystem::CollectionIteratorWrapper<EntitySystem::All>
  enemiesAll() const {
    return collection->allExcept(team_id);
  }

};


#endif //WARRIORS_INGAMEENTITY_H
