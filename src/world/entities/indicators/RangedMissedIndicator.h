//
// Created by aleksander on 25.05.19.
//

#ifndef WARRIORS_RANGEDMISSEDINDICATOR_H
#define WARRIORS_RANGEDMISSEDINDICATOR_H

#include "Indicator.h"
#include <SFML/Graphics.hpp>

class RangedMissedIndicator : public Indicator {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const RangedMissedIndicator &e);

    void
    display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

  private:
    sf::ConvexShape line;
  };

  RangedMissedIndicator(const Entity& attacker, const Entity& target);

  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

private:
  sf::ConvexShape line;
};

#endif //WARRIORS_RANGEDMISSEDINDICATOR_H
