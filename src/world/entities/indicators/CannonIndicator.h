//
// Created by aleksander on 31.05.19.
//

#ifndef WARRIORS_CANNONINDICATOR_H
#define WARRIORS_CANNONINDICATOR_H

#include "Indicator.h"
#include <SFML/Graphics.hpp>

class CannonIndicator : public Indicator {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const CannonIndicator &e);

    void
    display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

  private:
    sf::ConvexShape line;
    sf::CircleShape circle;
  };

  CannonIndicator(const Entity& attacker, position_data_t target_x, position_data_t target_y, position_data_t radius);

  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

private:
  sf::ConvexShape line;
  sf::CircleShape circle;
};

#endif //WARRIORS_CANNONINDICATOR_H
