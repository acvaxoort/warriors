//
// Created by aleksander on 25.05.19.
//

#include "RangedMissedIndicator.h"
#include <cmath>

std::unique_ptr<Entity::DisplayData> RangedMissedIndicator::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(RangedMissedIndicator);
}

RangedMissedIndicator::RangedMissedIndicator(const Entity &attacker, const Entity &target) {
  setPos(attacker.pos_x, attacker.pos_y);
  position_data_t angle = std::atan2(target.pos_y-attacker.pos_y, target.pos_x-attacker.pos_x);
  position_data_t sin_a = std::sin(angle);
  position_data_t cos_a = std::cos(angle);
  line.setPointCount(4);
  float perp_offset_x = static_cast<float>(0.1 * sin_a);
  float perp_offset_y = static_cast<float>(-0.1 * cos_a);
  line.setPoint(0, sf::Vector2f(perp_offset_x, perp_offset_y));
  line.setPoint(1, sf::Vector2f(-perp_offset_x, -perp_offset_y));
  line.setPoint(2, sf::Vector2f((float)(target.pos_x-attacker.pos_x) - perp_offset_x,
                                (float)(target.pos_y-attacker.pos_y) - perp_offset_y));
  line.setPoint(3, sf::Vector2f((float)(target.pos_x-attacker.pos_x) + perp_offset_x,
                                (float)(target.pos_y-attacker.pos_y) + perp_offset_y));
  line.setFillColor(sf::Color(160, 160, 160));
  line.setPosition((float)attacker.pos_x, (float)attacker.pos_y);
}

RangedMissedIndicator::DisplayData::DisplayData(const RangedMissedIndicator &e)
    : Entity::DisplayData(e),
      line(e.line) {}


void RangedMissedIndicator::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
  window.draw(line);
}
