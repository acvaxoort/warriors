//
// Created by aleksander on 25.05.19.
//

#include "RangedIndicator.h"
#include <cmath>

std::unique_ptr<Entity::DisplayData> RangedIndicator::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(RangedIndicator);
}

RangedIndicator::RangedIndicator(const Entity &attacker, const Entity &target) {
  setPos(attacker.pos_x, attacker.pos_y);
  position_data_t angle = std::atan2(target.pos_y-attacker.pos_y, target.pos_x-attacker.pos_x);
  position_data_t sin_a = std::sin(angle);
  position_data_t cos_a = std::cos(angle);

  line.setPointCount(4);
  float perp_offset_x = static_cast<float>(0.1 * sin_a);
  float perp_offset_y = static_cast<float>(-0.1 * cos_a);
  line.setPoint(0, sf::Vector2f(perp_offset_x, perp_offset_y));
  line.setPoint(1, sf::Vector2f(-perp_offset_x, -perp_offset_y));
  line.setPoint(2, sf::Vector2f((float)(target.pos_x-attacker.pos_x) - perp_offset_x,
                                (float)(target.pos_y-attacker.pos_y) - perp_offset_y));
  line.setPoint(3, sf::Vector2f((float)(target.pos_x-attacker.pos_x) + perp_offset_x,
                                (float)(target.pos_y-attacker.pos_y) + perp_offset_y));
  line.setFillColor(sf::Color::Black);
  line.setPosition((float)attacker.pos_x, (float)attacker.pos_y);

  arrow.setPointCount(3);
  arrow.setPoint(0, sf::Vector2f((float)(- 0.6 * cos_a + 0.25 * sin_a), (float)(- 0.6 * sin_a - 0.25 * cos_a)));
  arrow.setPoint(1, sf::Vector2f((float)(- 0.6 * cos_a - 0.25 * sin_a), (float)(- 0.6 * sin_a + 0.25 * cos_a)));
  arrow.setPoint(2, sf::Vector2f(0, 0));
  arrow.setFillColor(sf::Color::Black);
  arrow.setPosition((float)target.pos_x, (float)target.pos_y);
}

RangedIndicator::DisplayData::DisplayData(const RangedIndicator &e)
    : Entity::DisplayData(e),
      line(e.line),
      arrow(e.arrow) {}


void RangedIndicator::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                          position_data_t angle) {
  window.draw(line);
  window.draw(arrow);
}
