//
// Created by aleksander on 25.05.19.
//

#ifndef WARRIORS_RANGEDINDICATOR_H
#define WARRIORS_RANGEDINDICATOR_H

#include "Indicator.h"
#include <SFML/Graphics.hpp>

class RangedIndicator : public Indicator {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const RangedIndicator &e);

    void
    display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

  private:
    sf::ConvexShape line;
    sf::ConvexShape arrow;
  };

  RangedIndicator(const Entity& attacker, const Entity& target);

  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

private:
  sf::ConvexShape line;
  sf::ConvexShape arrow;
};

#endif //WARRIORS_RANGEDINDICATOR_H
