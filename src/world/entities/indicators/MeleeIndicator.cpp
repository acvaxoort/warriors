//
// Created by aleksander on 24.05.19.
//

#include "MeleeIndicator.h"
#include <cmath>

std::unique_ptr<Entity::DisplayData> MeleeIndicator::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(MeleeIndicator);
}

MeleeIndicator::MeleeIndicator(const Entity &attacker, const Entity &target) {
  setPos(attacker.pos_x, attacker.pos_y);
  position_data_t angle = std::atan2(target.pos_y-attacker.pos_y, target.pos_x-attacker.pos_x);
  position_data_t sin_a = std::sin(angle);
  position_data_t cos_a = std::cos(angle);

  shape.setPointCount(3);
  shape.setPoint(0, sf::Vector2f((float)(0.2 * sin_a), (float)(-0.2 * cos_a)));
  shape.setPoint(1, sf::Vector2f((float)(-0.2 * sin_a), (float)(0.2 * cos_a)));
  shape.setPoint(2, sf::Vector2f((float)(target.pos_x-attacker.pos_x), (float)(target.pos_y-attacker.pos_y)));
  shape.setFillColor(sf::Color::Black);
  shape.setPosition((float)attacker.pos_x, (float)attacker.pos_y);
}

MeleeIndicator::DisplayData::DisplayData(const MeleeIndicator &e)
    : Entity::DisplayData(e),
      shape(e.shape) {}


void MeleeIndicator::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                          position_data_t angle) {
  window.draw(shape);
}
