//
// Created by aleksander on 31.05.19.
//

#include "CannonIndicator.h"
#include <cmath>

std::unique_ptr<Entity::DisplayData> CannonIndicator::outputDisplayData() {
  OUTPUT_DISPLAY_DATA(CannonIndicator);
}

CannonIndicator::CannonIndicator(const Entity &attacker, position_data_t target_x, position_data_t target_y,
                                 position_data_t radius) {
  setPos(attacker.pos_x, attacker.pos_y);
  position_data_t angle = std::atan2(target_y-attacker.pos_y, target_x-attacker.pos_x);
  position_data_t sin_a = std::sin(angle);
  position_data_t cos_a = std::cos(angle);

  line.setPointCount(4);
  float perp_offset_x = static_cast<float>(0.1 * sin_a);
  float perp_offset_y = static_cast<float>(-0.1 * cos_a);
  line.setPoint(0, sf::Vector2f(perp_offset_x, perp_offset_y));
  line.setPoint(1, sf::Vector2f(-perp_offset_x, -perp_offset_y));
  line.setPoint(2, sf::Vector2f((float)(target_x-attacker.pos_x) - perp_offset_x,
                                (float)(target_y-attacker.pos_y) - perp_offset_y));
  line.setPoint(3, sf::Vector2f((float)(target_x-attacker.pos_x) + perp_offset_x,
                                (float)(target_y-attacker.pos_y) + perp_offset_y));
  line.setFillColor(sf::Color::Black);
  line.setPosition((float)attacker.pos_x, (float)attacker.pos_y);

  circle.setPointCount(24);
  circle.setRadius((float)radius);
  circle.setOutlineColor(sf::Color::Black);
  circle.setOutlineThickness(-0.5f);
  circle.setPosition((float)target_x, (float)target_y);
}

CannonIndicator::DisplayData::DisplayData(const CannonIndicator &e)
    : Entity::DisplayData(e),
      line(e.line),
      circle(e.circle) {}


void CannonIndicator::DisplayData::display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y,
                                           position_data_t angle) {
  window.draw(line);
  window.draw(circle);
}
