//
// Created by aleksander on 24.05.19.
//

#ifndef WARRIORS_INDICATOR_H
#define WARRIORS_INDICATOR_H

#include <entitysystem/Entity.h>

class Indicator : public Entity {
public:
  Indicator();
  void update() override;
  int lifespan;
};


#endif //WARRIORS_INDICATOR_H
