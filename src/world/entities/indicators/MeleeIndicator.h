//
// Created by aleksander on 24.05.19.
//

#ifndef WARRIORS_MELEEINDICATOR_H
#define WARRIORS_MELEEINDICATOR_H


#include "Indicator.h"
#include <SFML/Graphics.hpp>

class MeleeIndicator : public Indicator {
public:
  class DisplayData : public Entity::DisplayData {
  public:
    explicit DisplayData(const MeleeIndicator &e);

    void
    display(sf::RenderWindow &window, position_data_t pos_x, position_data_t pos_y, position_data_t angle) override;

  private:
    sf::ConvexShape shape;
  };

  MeleeIndicator(const Entity& attacker, const Entity& target);

  std::unique_ptr<Entity::DisplayData> outputDisplayData() override;

private:
  sf::ConvexShape shape;
};


#endif //WARRIORS_MELEEINDICATOR_H
