//
// Created by aleksander on 15.02.19.
//

#ifndef WARRIORS_INGAMEENTITYSYSTEM_H
#define WARRIORS_INGAMEENTITYSYSTEM_H


#include <entitysystem/EntitySystemCollection.h>
class SimulationEntity;
class Team;
class Environment;

class SimulationEntitySystem : public EntitySystemCollection {
public:
  SimulationEntitySystem(Environment* environment, position_data_t grid_size);

  void addTeam(std::unique_ptr<Team> team);

  void updateRegiments();

  void addSimulationEntity(size_t team_id, std::unique_ptr<SimulationEntity> e);

  Team* getTeam(size_t index);

  Environment* const environment;

  std::vector<std::unique_ptr<Team>> teams;
};


#endif //WARRIORS_INGAMEENTITYSYSTEM_H
