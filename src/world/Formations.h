//
// Created by aleksander on 27.04.19.
//

#ifndef WARRIORS_FORMATIONS_H
#define WARRIORS_FORMATIONS_H

#include <vector>
#include <entitysystem/Entity.h>

class Formation {
public:
  std::vector<std::pair<position_data_t, position_data_t>> positions;
  int commander_index;

  static Formation rectangle(int count, int rows, position_data_t space);

  static std::pair<position_data_t, position_data_t> positionInFormation(position_data_t rel_x, position_data_t rel_y,
                                                                         position_data_t center_x, position_data_t center_y,
                                                                         position_data_t angle);
  static std::pair<position_data_t, position_data_t> positionInFormation(position_data_t rel_x, position_data_t rel_y,
                                                                         position_data_t center_x, position_data_t center_y,
                                                                         position_data_t angle_sin, position_data_t angle_cos);

  static std::vector<std::vector<std::pair<position_data_t, position_data_t>>>
  rectangleWithRows(size_t count, uint16_t rows, position_data_t space);

};


#endif //WARRIORS_FORMATIONS_H
