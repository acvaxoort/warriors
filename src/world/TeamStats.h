//
// Created by aleksander on 21.05.19.
//

#ifndef WARRIORS_TEAMSTATS_H
#define WARRIORS_TEAMSTATS_H


#include <SFML/Graphics.hpp>
#include <string>

struct TeamStats {
  std::string name;
  sf::Color color;
  size_t alive_count = 0;
  size_t dead_count = 0;
  size_t escaped_count = 0;
};


#endif //WARRIORS_TEAMSTATS_H
