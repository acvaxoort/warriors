//
// Created by aleksander on 14.10.18.
//

#ifndef WARRIORS_ENVIRONMENT_H
#define WARRIORS_ENVIRONMENT_H

#include <vector>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics.hpp>
#include <entitysystem/EntitySystem.h>

class Environment {
public:
  Environment(size_t width, size_t height);

  class DisplayData {
  public:
    void display(sf::RenderWindow& window);
    void displayIndicators(sf::RenderWindow& window);
    std::unique_ptr<EntitySystem::DisplayData> corpses;
    std::unique_ptr<EntitySystem::DisplayData> indicators;
    float width;
    float height;
  };

  void update();
  std::unique_ptr<Environment::DisplayData> outputDisplayData();

  EntitySystem corpses;
  EntitySystem indicators;

  inline size_t getWidth() { return width; }
  inline size_t getHeight() { return height; }

  int64_t seed = 0;

private:
  inline size_t getIndex(size_t x, size_t y) { return x + y*width; }

  size_t width;
  size_t height;
};


#endif //WARRIORS_ENVIRONMENT_H
