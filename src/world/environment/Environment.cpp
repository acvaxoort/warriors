//
// Created by aleksander on 14.10.18.
//

#include <util/RandomWrapper.hpp>
#include <util/SfmlWindowUtil.h>
#include "Environment.h"

Environment::Environment(size_t width, size_t height)
    : width(width),
      height(height) {
}

void Environment::update() {
  indicators.update1_Entities();
  indicators.update3_Delete();
}

std::unique_ptr<Environment::DisplayData> Environment::outputDisplayData() {
  auto result = std::make_unique<Environment::DisplayData>();
  result->width = width;
  result->height = height;
  result->corpses = corpses.outputDisplayData();
  result->indicators = indicators.outputDisplayData();
  return std::move(result);
}

void Environment::DisplayData::display(sf::RenderWindow& window) {
  const float grid_size = 250;
  int x_squares = (int) std::ceil(width / grid_size);
  int y_squares = (int) std::ceil(height / grid_size);
  for (int j = 0; j < y_squares; ++j) {
    for (int i = 0; i < x_squares; ++i) {
      float x = i*grid_size;
      float y = j*grid_size;
      sf::RectangleShape rect({std::min(grid_size, width - x), std::min(grid_size, height - y)});
      rect.setPosition(x, y);
      rect.setFillColor((i+j) % 2 ? sf::Color::White : sf::Color(240, 240, 240));
      window.draw(rect);
    }
  }
  corpses->display(window, *corpses, 1);
}

void Environment::DisplayData::displayIndicators(sf::RenderWindow &window) {
  indicators->display(window, *indicators, 1);
}
