//
// Created by aleksander on 14.02.19.
//

#include <iostream>
#include "Team.h"

Team::Team(const sf::Color& color, const std::string& name)
    : color(color),
      name(name) {
}

TeamStats Team::getStats() {
  TeamStats ret;
  ret.name = name;
  ret.color = color;
  ret.alive_count = alive_count;
  ret.dead_count = dead_count;
  ret.escaped_count = escaped_count;
  return ret;
}

void Team::addRegiment(std::unique_ptr<Regiment> reg) {
  reg->system = system;
  reg->init();
  regiments.emplace_back(std::move(reg));
}

void Team::update() {
  for(auto & i: regiments){
    i->update();
  }

  for (auto reg : removal_queue) {
    auto it = regiments.begin();
    for (; it != regiments.end(); ++it) {
      if (it->get() == reg) {
        break;
      }
    }
    if (it != regiments.end()) {
      regiments.erase(it);
      for (auto& enemy : enemies) {
        for (auto& en_reg : enemy->regiments) {
          en_reg->forgetAbout(reg);
        }
      }
      for (auto& al_reg : regiments) {
        al_reg->forgetAbout(reg);
      }
    }
  }
}

/*
void Team::disband(TestWarrior* commander) {
  auto found_it = regiments.end();
  for (auto it = regiments.begin(); it != regiments.end(); ++it) {
    if ((*it)->commander == commander) {
      found_it = it;
      break;
    }
  }

  if (found_it != regiments.end()) {
    Regiment* deleted = (*found_it).get();
    regiments.erase(found_it);
    for (auto enemy : enemies) {
      for (auto& reg : enemy->regiments) {
        if (reg->target == deleted) {
          reg->target = nullptr;
        }
      }
    }
  } else {
    std::cout<<"Team::disband: Invalid commander\n";
  }
}
*/
