//
// Created by aleksander on 27.04.19.
//

#include <cmath>
#include "Formations.h"

Formation Formation::rectangle(int count, int rows, position_data_t space) {
  if (count < 1) {
    if (count == 0) {
      return {};
    }
    throw std::invalid_argument("count < 0");
  }
  if (rows < 1) {
    throw std::invalid_argument("rows < 1");
  }
  int count_per_row = (count + rows - 1) / rows;
  int count_last_row = count - (rows - 1) * count_per_row;
  int center_index_y = (count_per_row - 1) / 2;
  int center_index_x = (rows - 1) / 2;
  int last_row_offset_index = (count_per_row - count_last_row) / 2;
  position_data_t first_offset_y = -center_index_y * space;
  position_data_t first_offset_x = center_index_x * space;
  position_data_t last_row_offset_y = first_offset_y + space * last_row_offset_index;

  Formation result;
  result.positions.reserve(static_cast<size_t>(count));
  for (int j = 0; j < rows - 1; ++j) {
    for (int i = 0; i < count_per_row; ++i) {
      result.positions.emplace_back(first_offset_x - j*space, first_offset_y + i*space);
    }
  }
  for (int i = 0; i < count_last_row; ++i) {
    result.positions.emplace_back(first_offset_x - (rows-1)*space, last_row_offset_y + i*space);
  }
  result.commander_index = center_index_x*count_per_row + center_index_y;
  return result;
}

std::pair<position_data_t, position_data_t>
Formation::positionInFormation(position_data_t rel_x, position_data_t rel_y, position_data_t center_x,
                               position_data_t center_y, position_data_t angle) {
  position_data_t temp_sin = std::sin(angle);
  position_data_t temp_cos = std::cos(angle);
  return { center_x + temp_cos * rel_x - temp_sin * rel_y, center_y + temp_sin * rel_x + temp_cos * rel_y };
}

std::pair<position_data_t, position_data_t>
Formation::positionInFormation(position_data_t rel_x, position_data_t rel_y, position_data_t center_x,
                               position_data_t center_y, position_data_t angle_sin, position_data_t angle_cos) {
  return { center_x + angle_cos * rel_x - angle_sin * rel_y, center_y + angle_sin * rel_x + angle_cos * rel_y };
}

std::vector<std::vector<std::pair<position_data_t, position_data_t>>>
Formation::rectangleWithRows(size_t count, uint16_t rows, position_data_t space) {
  if (count < 1) {
    return {};
  }
  if (rows < 1) {
    throw std::invalid_argument("rows < 1");
  }
  size_t count_per_row;
  size_t count_last_row;

  if (count % rows == 0) {
    count_per_row = count / rows;
    count_last_row = count_per_row;
  } else {
    count_per_row = count / rows + 2;
    do {
      count_per_row--;
      count_last_row = count - count_per_row * (rows - 1);
    } while (count_last_row > count);
  }
  size_t last_row_offset_index = (count_per_row - count_last_row) / 2;
  position_data_t first_offset_y = -((double)count_per_row - 1) * space / 2;
  position_data_t first_offset_x = (rows - 1) * space / 2;
  position_data_t last_row_offset_y = first_offset_y + space * last_row_offset_index;

  std::vector<std::vector<std::pair<position_data_t, position_data_t>>> result;
  result.resize(rows);
  for (int j = 0; j < rows - 1; ++j) {
    result[j].reserve(static_cast<size_t>(count_per_row));
    for (int i = 0; i < count_per_row; ++i) {
      result[j].emplace_back(first_offset_x - j*space, first_offset_y + i*space);
    }
  }
  auto& last_row = result[rows-1];
  last_row.reserve(static_cast<size_t>(count_last_row));
  for (int i = 0; i < count_last_row; ++i) {
    last_row.emplace_back(first_offset_x - (rows-1)*space, last_row_offset_y + i*space);
  }
  return result;
}
