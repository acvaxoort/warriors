//
// Created by aleksander on 04.05.19.
//

#include "Regiment.h"
#include <util/PlanarGeometryUtil.h>
#include <world/entities/types/Soldier.h>
#include <iostream>
#include <world/Formations.h>

Regiment::Regiment(position_data_t pos_x, position_data_t pos_y, position_data_t angle)
    : pos_x(pos_x), pos_y(pos_y), vel_x(0), vel_y(0),
      angle_sin(std::sin(angle)), angle_cos(std::cos(angle)), angle(angle) {}

void Regiment::addSoldier(Soldier* soldier){
  auto it = std::find(soldiers.begin(), soldiers.end(), soldier);
  if (it == soldiers.end()) {
    soldiers.emplace_back(soldier);
    soldier->setRegiment(this);
  } else {
    std::cout<<"Tried to insert a soldier that was already there\n";
  }
}

void Regiment::removeSoldier(Soldier* soldier) {
  auto it = std::find(soldiers.begin(), soldiers.end(), soldier);
  if (it != soldiers.end()) {
    auto it2 = soldiers.end()-1;
    if (it == it2) {
      soldiers.pop_back();
    } else {
      *it = *it2;
      soldiers.pop_back();
    }
    soldier->setRegiment(nullptr);
  } else {
    std::cout<<"Tried to remove a soldier that was not there\n";
  }
}

void Regiment::init() {
  auto seed = static_cast<uint_fast32_t>(system->environment->seed);
  for (auto& t : system->teams) {
    if (t.get() == team) {
      break;
    } else {
      seed += 1234;
    }
  }
  for (auto& r : team->regiments) {
    if (r.get() == this) {
      break;
    } else {
      seed++;
    }
  }
  random.seed(seed);
}

void Regiment::update() {
  vel_x *= intertia;
  vel_y *= intertia;
  pos_x += vel_x;
  pos_y += vel_y;
}

void Regiment::setAngle(position_data_t formation_angle) {
  angle = formation_angle;
  angle_sin = std::sin(formation_angle);
  angle_cos = std::cos(formation_angle);
}

void Regiment::goTo(position_data_t x, position_data_t y, position_data_t speed) {
  position_data_t dx = x - pos_x;
  position_data_t dy = y - pos_y;
  if (dx == 0 && dy == 0) {
    return;
  }
  position_data_t dpos = pgeom::getDistance(dx, dy);
  if (dpos > speed) {
    position_data_t mul = speed / dpos;
    vel_x += dx*mul;
    vel_y += dy*mul;
  }
}

void Regiment::updateToCenterPosition() {
  size_t soldier_count = soldiers.size();
  if (soldier_count == 0) {
    return;
  }

  position_data_t x = 0;
  position_data_t y = 0;
  for (size_t i = 0; i < soldier_count; ++i) {
    x += soldiers[i]->pos_x;
    y += soldiers[i]->pos_y;
  }

  pos_x = x / soldier_count;
  pos_y = y / soldier_count;
}

void Regiment::removeSelf() {
  for (auto soldier : soldiers) {
    soldier->setRegiment(nullptr);
  }
  soldiers.clear();
  team->removal_queue.push_back(this);
}

void Regiment::forgetAbout(Regiment *reg) {

}

void Regiment::setSoldiersInitPositions() {
  for (auto s : soldiers) {
    auto offset = s->getFormationOffset();
    auto expected = Formation::positionInFormation(offset.first , offset.second, pos_x, pos_y, angle_sin, angle_cos);
    s->pos_x = expected.first;
    s->pos_y = expected.second;
    s->angle = angle;
  }
}

void Regiment::avoidAlliedCollision() {
  for (auto &reg : team->regiments) {
    if (reg.get() == this) {
      continue;
    }

    position_data_t dx = pos_x - reg->pos_x;
    position_data_t dy = pos_y - reg->pos_y;
    position_data_t checked_distance_sqr = pgeom::getDistanceSqr(dx, dy);
    position_data_t allowed_distance = approx_radius + reg->approx_radius;
    if (checked_distance_sqr < allowed_distance*allowed_distance) {
      position_data_t dist = std::sqrt(checked_distance_sqr);
      if (dist > 0.01) {
        vel_x += 0.1 * dx / dist;
        vel_y += 0.1 * dy / dist;
      }
    }
  }
}





