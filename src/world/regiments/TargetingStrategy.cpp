//
// Created by aleksander on 01.06.19.
//

#include "TargetingStrategy.h"
#include <world/Team.h>
#include <util/PlanarGeometryUtil.h>
#include <world/regiments/types/kokenhausen/PikeAndShootLine.h>

void TargetingStrategy::update() {
  if (waiting_time > 0) {
    waiting_time--;
  }
}

bool TargetingStrategy::isAllowedToMove() {
  return waiting_time <= 0;
}

Regiment* TargetingStrategy::seekTargetFor(Regiment* this_regiment) {
  if (prioritize_infantry) {
    position_data_t target_distance_sqr = 1e300;
    Regiment* target = nullptr;
    position_data_t infantry_target_distance_sqr = 1e300;
    Regiment* infantry_target = nullptr;
    for (auto enemy : this_regiment->team->enemies) {
      for (auto &reg : enemy->regiments) {
        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(this_regiment->pos_x - reg->pos_x,
                                                                     this_regiment->pos_y - reg->pos_y);
        if (auto inf_reg = dynamic_cast<PikeAndShootLine*>(reg.get())) {
          if (checked_distance_sqr < infantry_target_distance_sqr && !reg->soldiers.empty()) {
            infantry_target = reg.get();
            infantry_target_distance_sqr = checked_distance_sqr;
          }
        } else {
          if (checked_distance_sqr < target_distance_sqr && !reg->soldiers.empty()) {
            target = reg.get();
            target_distance_sqr = checked_distance_sqr;
          }
        }
      }
    }
    if (infantry_target && target_distance_sqr > this_regiment->approx_radius*this_regiment->approx_radius*9) {
      return infantry_target;
    } else {
      return target;
    }
  } else {
    position_data_t target_distance_sqr = 1e300;
    Regiment* target = nullptr;
    for (auto enemy : this_regiment->team->enemies) {
      for (auto &reg : enemy->regiments) {
        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(this_regiment->pos_x - reg->pos_x,
                                                                     this_regiment->pos_y - reg->pos_y);
        if (checked_distance_sqr < target_distance_sqr && !reg->soldiers.empty()) {
          target = reg.get();
          target_distance_sqr = checked_distance_sqr;
        }
      }
    }
    return target;
  }
}
