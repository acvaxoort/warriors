//
// Created by aleksander on 21.05.19.
//

#ifndef WARRIORS_DEFAULTREGIMENT_H
#define WARRIORS_DEFAULTREGIMENT_H

#include <world/regiments/Regiment.h>
#include <world/entities/types/Soldier.h>

class DefaultRegiment : public Regiment {
public:
  DefaultRegiment(position_data_t x, position_data_t y, position_data_t angle,
                  position_data_t speed, int initial_count, int rows, position_data_t spacing);

  void update() override;

  //Nadanie nowych pozycji żołnierzom w oddziale
  void updateFormation();

  void forgetAbout(Regiment* reg) override;

private:
  //Rozkaz ataku
  void orderToCharge();

  //Rozkaz ponownego uformowania szeregu
  void restoreFormation();

  int detailed_update_cd = 0;

  position_data_t speed;
  int initial_count;
  int rows;
  position_data_t spacing;
  Soldier::Action formation_action = Soldier::FORMATION;
  Regiment* target = nullptr;
  Regiment* currently_fighting = nullptr; // currently fighting ma inne znaczenie - inne oddziały patrzą na to sprawdzając czy trzeba pomóc
  position_data_t advancing_speed = 0.5;
  bool fast_advance = false;

};


#endif //WARRIORS_DEFAULTREGIMENT_H
