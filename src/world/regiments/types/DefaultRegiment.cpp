//
// Created by aleksander on 21.05.19.
//

#include "DefaultRegiment.h"
#include <world/Formations.h>
#include <util/PlanarGeometryUtil.h>
#include <iostream>

DefaultRegiment::DefaultRegiment(position_data_t x, position_data_t y, position_data_t angle,
                                 position_data_t speed, int initial_count, int rows, position_data_t spacing)
    : Regiment(x, y, angle),
      speed(speed),
      initial_count(initial_count),
      rows(rows),
      spacing(spacing) {}

void DefaultRegiment::update() {

  //Jeśli jest cel, podązanie za nim
  if (target) {
    if (target->soldiers.empty()) {
      target = nullptr;
      detailed_update_cd = 0;

    } else {
      //fast_advance jest prawdą jeśli spieszymy z pomocą pobliskim sojusznikom
      //W przeciwnym wypadku, podążamy za przeciwnikiem wolniej
      goTo(target->pos_x, target->pos_y, speed * (fast_advance ? 1.0f : 0.5f));
      setAngle(std::atan2(target->pos_y - pos_y, target->pos_x - pos_x));
    }
  }

  avoidAlliedCollision();

  //Akcje nie wykonywane za każdym razem - poszukiwanie celów i wydawanie rozkazów

  // -Jesli wroga formacja jest odpowiednio blisko, rozkaz szarży
  // -Jeśli w pobliżu oddział sojuszniczy walczy, należy zmierzać w jego kierunku
  // -Jeśli istnieje oddział przeciwnika, należy zmierzać w jego kierunku
  // -Jeśli wszystkie oddziały przeciwnika są rozbite, wydanie rozkazu szarży
  if (detailed_update_cd == 0) {
    detailed_update_cd = random.randInt(3, 5);


    position_data_t target_distance_sqr = 1e300;
    position_data_t nearby_distance_sqr = 2*approx_radius + 64;
    nearby_distance_sqr *= nearby_distance_sqr;
    target = nullptr;

    //Znalezienie najbliższego oddziału przeciwnika
    for (auto enemy : team->enemies) {
      for (auto &reg : enemy->regiments) {

        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                     pos_y - reg->pos_y);
        if (checked_distance_sqr < target_distance_sqr && !reg->soldiers.empty()) {
          target = reg.get();
          target_distance_sqr = checked_distance_sqr;
        }
      }
    }

    //Istnieje jakikolwiek oddział przeciwnika
    if (target) {

      //Oddział przeciwnika jest w pobliżu
      if (target_distance_sqr < nearby_distance_sqr) {
        orderToCharge();
        currently_fighting = target;
        fast_advance = false;
      } else {

        //W przeciwnym wypadku - sprawdzenie czy jest w pobliżu oddział sojusznika w bezpośredniej walce
        target_distance_sqr = 1e300;
        nearby_distance_sqr = approx_radius + 64;
        nearby_distance_sqr *= nearby_distance_sqr;
        DefaultRegiment *ally_in_need = nullptr;
        for (auto &reg : team->regiments) {
          if (auto p = dynamic_cast<DefaultRegiment *>(reg.get())) {
            if (p->currently_fighting && p != this) {
              position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                           pos_y - reg->pos_y);
              if (checked_distance_sqr < target_distance_sqr) {
                ally_in_need = p;
                target_distance_sqr = checked_distance_sqr;
              }
            }
          }
        }
        if (ally_in_need && target_distance_sqr < nearby_distance_sqr) {
          restoreFormation();
          currently_fighting = nullptr;
          target = ally_in_need->currently_fighting;
          fast_advance = true;
        } else {

          //Jeśli nie ma to podążać za najbliższym oddziałem przeciwnika
          restoreFormation();
          currently_fighting = nullptr;
          fast_advance = false;
        }
      }

      //Nie istnieje żaden oddział przeciwnika
    } else {
      orderToCharge();
      currently_fighting = nullptr;
      fast_advance = false;
    }
    updateFormation();

    if (formation_action == Soldier::CHARGE) {
      updateToCenterPosition();
    }
  }
  detailed_update_cd--;

  if (soldiers.size() < initial_count / 10) {
    removeSelf();
  }

  Regiment::update();
}

void DefaultRegiment::updateFormation() {
  size_t soldier_count = soldiers.size();
  if (soldier_count == 0) {
    return;
  }

  auto formation = Formation::rectangle((int) soldier_count, rows, spacing);
  for (size_t i = 0; i < soldier_count; ++i) {
    soldiers[i]->setFormationOffset(formation.positions[i].first, formation.positions[i].second);
  }
  approx_radius = std::sqrt((position_data_t)soldier_count*spacing);
}

//Rozkaz ataku
void DefaultRegiment::orderToCharge() {
  if (formation_action != Soldier::CHARGE) {
    formation_action = Soldier::CHARGE;

    for (auto p : soldiers) {
      p->setAction(Soldier::CHARGE);
    }
  }
}

//Rozkaz uformowania formacji/szeregu
void DefaultRegiment::restoreFormation() {
  if (formation_action != Soldier::FORMATION) {
    formation_action = Soldier::FORMATION;

    updateToCenterPosition();
    updateFormation();

    for (auto p : soldiers) {
      p->setAction(Soldier::FORMATION);
    }
  }
}

void DefaultRegiment::forgetAbout(Regiment *reg) {
  if (target == reg) {
    target = nullptr;
  }
  if (currently_fighting == reg) {
    target = nullptr;
  }
}
