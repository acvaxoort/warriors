//
// Created by aleksander on 24.05.19.
//

#ifndef WARRIORS_REITERREGIMENT_H
#define WARRIORS_REITERREGIMENT_H

#include <world/regiments/Regiment.h>
#include <world/regiments/TargetingStrategy.h>
#include <world/entities/types/Soldier.h>

class ReiterRegiment :public Regiment {
public:
  ReiterRegiment(position_data_t x, position_data_t y, position_data_t angle, size_t initial_count, uint16_t rows,
                 TargetingStrategy targeting_strategy);

  void update() override;
  void forgetAbout(Regiment* reg) override;
  void updateFormation();

  struct RowData {
    std::array<std::pair<position_data_t, position_data_t>, 3> waypoints;
    position_data_t middle_wp_offset = 0;
    position_data_t row_offset = 0;
  };

  std::vector<RowData> rows_data;
  Regiment* target = nullptr;

private:
  Soldier::Action formation_action = Soldier::FORMATION;
  size_t initial_count;
  uint16_t rows;
  position_data_t row_begin_dist = -1;
  position_data_t row_end_dist = 1;
  TargetingStrategy targeting_strategy;

  const uint16_t SHIFT_PERIOD = 240;

  uint16_t caracole_clock = SHIFT_PERIOD - (uint16_t)1;
  uint16_t detailed_update_cd = 0;
};


#endif //WARRIORS_REITERREGIMENT_H
