//
// Created by aleksander on 26.05.19.
//

#ifndef WARRIORS_PIKEANDSHOOTLINE_H
#define WARRIORS_PIKEANDSHOOTLINE_H

#include <world/regiments/Regiment.h>
#include <world/regiments/TargetingStrategy.h>
#include <world/entities/types/Soldier.h>

class PikeAndShootLine : public Regiment {
public:
  PikeAndShootLine(position_data_t x, position_data_t y, position_data_t angle, size_t initial_count, uint16_t rows,
                   TargetingStrategy targeting_strategy);

  void update() override;
  void forgetAbout(Regiment* reg) override;
  void updateFormation();

  Regiment* target = nullptr;

private:
  Soldier::Action formation_action = Soldier::FORMATION;
  size_t initial_count;
  uint16_t rows;
  uint16_t detailed_update_cd = 0;
  TargetingStrategy targeting_strategy;

  friend class CannonLine;
};


#endif //WARRIORS_PIKEANDSHOOTLINE_H
