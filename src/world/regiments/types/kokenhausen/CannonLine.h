//
// Created by aleksander on 31.05.19.
//

#ifndef WARRIORS_CANNONLINE_H
#define WARRIORS_CANNONLINE_H

#include <world/regiments/Regiment.h>
#include <world/entities/types/Soldier.h>

class CannonLine : public Regiment {
public:
  CannonLine(Regiment* attached, size_t initial_count);

  void update() override;
  void forgetAbout(Regiment* reg) override;
  void updateFormation();

  Regiment* target = nullptr;
  Regiment* attached = nullptr;

private:
  Soldier::Action formation_action = Soldier::FORMATION;
  size_t initial_count;
  uint16_t detailed_update_cd = 0;
};

#endif //WARRIORS_CANNONLINE_H
