//
// Created by aleksander on 30.05.19.
//

#ifndef WARRIORS_HUSSARREGIMENT_H
#define WARRIORS_HUSSARREGIMENT_H

#include <world/regiments/Regiment.h>
#include <world/entities/types/Soldier.h>
#include <world/regiments/TargetingStrategy.h>

class HussarRegiment :public Regiment {
public:
  HussarRegiment(position_data_t x, position_data_t y, position_data_t angle, size_t initial_count, uint16_t rows,
                 TargetingStrategy targeting_strategy);

  void update() override;
  void forgetAbout(Regiment* reg) override;
  void updateFormation();

  Regiment* target = nullptr;
  position_data_t loose_target_x;
  position_data_t loose_target_y;

private:
  Soldier::Action formation_action = Soldier::FORMATION;
  size_t initial_count;
  uint16_t rows;
  TargetingStrategy targeting_strategy;

  uint16_t detailed_update_cd = 0;
};

#endif //WARRIORS_HUSSARREGIMENT_H
