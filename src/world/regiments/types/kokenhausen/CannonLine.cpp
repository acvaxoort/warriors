//
// Created by aleksander on 31.05.19.
//

#include <world/Formations.h>
#include "CannonLine.h"
#include "PikeAndShootLine.h"

CannonLine::CannonLine(Regiment* attached, size_t initial_count)
    : Regiment(attached->pos_x, attached->pos_y, attached->angle),
      attached(attached),
      initial_count(initial_count) {
  position_data_t y_offset;
  if (auto reg = dynamic_cast<PikeAndShootLine*>(attached)) {
    y_offset = (reg->rows + 3) * 0.45;
  } else {
    y_offset = attached->approx_radius;
  }
  pos_x -= attached->angle_cos * y_offset;
  pos_y += attached->angle_sin * y_offset;
}

void CannonLine::update() {
  if (attached) {
    position_data_t desired_x = attached->pos_x;
    position_data_t desired_y = attached->pos_y;
    position_data_t y_offset;
    if (auto reg = dynamic_cast<PikeAndShootLine*>(attached)) {
      y_offset = (reg->rows + 3) * 0.45;
    } else {
      y_offset = attached->approx_radius;
    }
    desired_x -= attached->angle_cos * y_offset;
    desired_y += attached->angle_sin * y_offset;
    goTo(desired_x, desired_y, 0.5);

  } else {
    removeSelf();
  }

  if (detailed_update_cd == 0) {
    detailed_update_cd = random.randInt<uint16_t>(3, 5);
    updateFormation();
    position_data_t target_distance_sqr = 1e300;
    target = nullptr;

    //Znalezienie najbliższego oddziału przeciwnika w pobliżu którego nie ma sojuszników
    for (auto enemy : team->enemies) {
      for (auto &reg : enemy->regiments) {
        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                     pos_y - reg->pos_y);
        if (checked_distance_sqr < target_distance_sqr && !reg->soldiers.empty()) {
          bool safe = true;
          for (auto& ally_reg : team->regiments) {
            if (pgeom::getDistance(reg->pos_x - ally_reg->pos_x, reg->pos_y - ally_reg->pos_y)
                < 1.25*(reg->approx_radius + ally_reg->approx_radius)) {
              safe = false;
              break;
            }
          }
          if (safe) {
            target = reg.get();
            target_distance_sqr = checked_distance_sqr;
          }
        }
      }
    }
  }

  if (soldiers.size() < initial_count / 2) {
    removeSelf();
  }

  Regiment::update();
}

void CannonLine::forgetAbout(Regiment *reg) {
  if (target == reg) {
    target = nullptr;
  }
  if (attached == reg) {
    attached = nullptr;
  }
}

void CannonLine::updateFormation() {
  size_t soldier_count = soldiers.size();
  if (soldier_count == 0) {
    return;
  }

  auto formation = Formation::rectangleWithRows(soldier_count, 1, 0.9);
  size_t index = 0;
  for (size_t i = 0; i < 1; ++i) {
    auto& row = formation[i];
    for (auto pos : row) {
      if (index == soldier_count) {
        std::cout<<"Too many positions returned\n";
        break;
      }
      soldiers[index]->setFormationOffset(pos.first, pos.second);
      index++;
    }
  }
  if (index != soldier_count) {
    std::cout<<"Not enough positions returned\n";
  }
  approx_radius = std::sqrt((position_data_t)soldier_count*2);
}
