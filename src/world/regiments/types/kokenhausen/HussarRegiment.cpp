//
// Created by aleksander on 30.05.19.
//

#include "HussarRegiment.h"

#include <world/Formations.h>
#include <util/PlanarGeometryUtil.h>
#include <world/entities/types/kokenhausen/Reiter.h>
#include "ReiterRegiment.h"
#include "PikeAndShootLine.h"
#include "CannonLine.h"

HussarRegiment::HussarRegiment(position_data_t x, position_data_t y, position_data_t angle,
                               size_t initial_count, uint16_t rows, TargetingStrategy targeting_strategy)
    : Regiment(x, y, angle),
      initial_count(initial_count),
      rows(rows),
      targeting_strategy(targeting_strategy) {}

void HussarRegiment::update() {
  avoidAlliedCollision();

  if (detailed_update_cd == 0) {
    detailed_update_cd = random.randInt<uint16_t>(3, 5);
    updateFormation();
    if (formation_action != Soldier::FORMATION) {
      updateToCenterPosition();
    }

    target = targeting_strategy.seekTargetFor(this);
  }
  detailed_update_cd--;

  targeting_strategy.update();

  if (target) {
    loose_target_x = target->pos_x;
    loose_target_y = target->pos_y;
  }

  if (target) {
    position_data_t dx = target->pos_x - pos_x;
    position_data_t dy = target->pos_y - pos_y;
    if (std::fabs(dx) < 1e-10) { dx = 1; }
    if (std::fabs(dy) < 1e-10) { dy = 1; }
    position_data_t dist = pgeom::getDistance(dx, dy);
    position_data_t dist_charge = 1.5*approx_radius;
    position_data_t max_dist_reg = 1.5*target->approx_radius + 5.0*approx_radius;

    if (targeting_strategy.isAllowedToMove()) {
      setAngle(std::atan2(dy, dx));
    }
    if (dist < max_dist_reg) {
      goTo(target->pos_x, target->pos_y, 0.35);

      bool dangerous_to_charge = false;

      position_data_t angle_to_them;
      if (!targeting_strategy.reckless_charge) {
        bool check = false;
        if (auto p = dynamic_cast<PikeAndShootLine*>(target)) {
          check = true;
        }
        if (auto p = dynamic_cast<CannonLine*>(target)) {
          check = true;
        }
        if (check) {
          angle_to_them = std::atan2(pos_y - target->pos_y, pos_x - target->pos_x);
          if (std::cos(angle_to_them - target->angle) > -0.7) {
            dangerous_to_charge = true;
          }
        }
      }

      if (!dangerous_to_charge) {
        //Jeśli za blisko - po prostu szarża
        if (dist < dist_charge) {
          if (formation_action != Soldier::CHARGE) {
            formation_action = Soldier::CHARGE;
            for (auto s : soldiers) {
              s->setAction(Soldier::CHARGE);
            }
          }

          //Jeśli wystarczająco blisko ale nie za blisko - nacieranie w luźnym szyku
        } else {
          if (formation_action != Soldier::LOOSE_FORMATION) {
            formation_action = Soldier::LOOSE_FORMATION;
            for (auto s : soldiers) {
              s->setAction(Soldier::LOOSE_FORMATION);
            }
          }
        }
      } else {

        //Okrążanie
        if (formation_action != Soldier::LOOSE_FORMATION) {
          formation_action = Soldier::LOOSE_FORMATION;
          for (auto s : soldiers) {
            s->setAction(Soldier::LOOSE_FORMATION);
          }
        }
        position_data_t avoid_dist = 2.0*target->approx_radius + 3.0*approx_radius;
        position_data_t angle_diff = angle_to_them - target->angle;
        angle_diff += M_PI;
        int temp = static_cast<int>(angle_diff / (M_PI * 2));
        angle_diff = angle_diff - temp * M_PI * 2 - M_PI;

        position_data_t dir = angle_to_them;
        position_data_t correction_dist = avoid_dist - dist;
        if (angle_diff < 0) {
          dir -= M_PI_2 - std::atan(correction_dist / avoid_dist);
        } else {
          dir += M_PI_2 - std::atan(correction_dist / avoid_dist);
        }
        position_data_t dir_sin = std::sin(dir);
        position_data_t dir_cos = std::cos(dir);

        loose_target_x = pos_x + 1000*dir_cos;
        loose_target_y = pos_y + 1000*dir_sin;
      }


      //Jeśli dalej - pójście w tamtym kierunku
    } else {
      if (formation_action != Soldier::FORMATION) {
        formation_action = Soldier::FORMATION;
        for (auto s : soldiers) {
          s->setAction(Soldier::FORMATION);
        }
      }
      if (targeting_strategy.isAllowedToMove()) {
        goTo(target->pos_x, target->pos_y, 0.5);
      }
    }
  } else {
    if (formation_action != Soldier::CHARGE) {
      formation_action = Soldier::CHARGE;
      for (auto s : soldiers) {
        s->setAction(Soldier::CHARGE);
      }
    }
  }

  if (soldiers.size() < initial_count / 3) {
    removeSelf();
  }

  Regiment::update();
}

void HussarRegiment::forgetAbout(Regiment *reg) {
  if (reg == target) {
    target = nullptr;
  }
}

void HussarRegiment::updateFormation() {
  size_t soldier_count = soldiers.size();
  if (soldier_count == 0) {
    return;
  }

  auto formation = Formation::rectangleWithRows(soldier_count, rows, 1.3);
  size_t index = 0;
  for (size_t i = 0; i < rows; ++i) {
    auto& row = formation[i];
    for (auto pos : row) {
      if (index == soldier_count) {
        std::cout<<"Too many positions returned\n";
        break;
      }
      soldiers[index]->setFormationOffset(pos.first, pos.second);
      index++;
    }
  }
  if (index != soldier_count) {
    std::cout<<"Not enough positions returned\n";
  }
  approx_radius = std::sqrt((position_data_t)soldier_count*2.2);
}
