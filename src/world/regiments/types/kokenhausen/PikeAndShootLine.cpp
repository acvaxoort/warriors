//
// Created by aleksander on 26.05.19.
//

#include "PikeAndShootLine.h"
#include <util/PlanarGeometryUtil.h>
#include <world/Formations.h>

PikeAndShootLine::PikeAndShootLine(position_data_t x, position_data_t y, position_data_t angle, size_t initial_count,
                                   uint16_t rows, TargetingStrategy targeting_strategy)
    : Regiment(x, y, angle),
      initial_count(initial_count),
      rows(rows),
      targeting_strategy(targeting_strategy)  {
  if (rows % 2 == 1) {
    std::cout<<"PikeAndShootLine should have an even amount of rows, changing from"
        <<rows<<" to "<<rows+1<<"\n";
    this->rows++;
  }
}

void PikeAndShootLine::update() {
  if (detailed_update_cd == 0) {
    detailed_update_cd = random.randInt<uint16_t>(3, 5);
    updateFormation();

    target = targeting_strategy.seekTargetFor(this);
  }
  detailed_update_cd--;

  targeting_strategy.update();

  if (soldiers.size() < initial_count / 2) {
    removeSelf();
  }

  Regiment::update();
}

void PikeAndShootLine::forgetAbout(Regiment *reg) {
  if (target == reg) {
    target = nullptr;
  }
}

void PikeAndShootLine::updateFormation() {
  size_t soldier_count = soldiers.size();
  if (soldier_count == 0) {
    return;
  }

  auto formation = Formation::rectangleWithRows(soldier_count, rows, 0.9);
  size_t index = 0;
  for (size_t i = 0; i < rows; ++i) {
    auto& row = formation[i];
    for (auto pos : row) {
      if (index == soldier_count) {
        std::cout<<"Too many positions returned\n";
        break;
      }
      soldiers[index]->setFormationOffset(pos.first, pos.second);
      index++;
    }
  }
  if (index != soldier_count) {
    std::cout<<"Not enough positions returned\n";
  }
  approx_radius = std::sqrt((position_data_t)soldier_count*2);
}
