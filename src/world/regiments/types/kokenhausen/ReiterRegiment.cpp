//
// Created by aleksander on 24.05.19.
//

#include <world/Formations.h>
#include <util/PlanarGeometryUtil.h>
#include <world/entities/types/kokenhausen/Reiter.h>
#include "ReiterRegiment.h"

ReiterRegiment::ReiterRegiment(position_data_t x, position_data_t y, position_data_t angle,
                               size_t initial_count, uint16_t rows, TargetingStrategy targeting_strategy)
    : Regiment(x, y, angle),
      rows_data(rows),
      initial_count(initial_count),
      rows(rows),
      targeting_strategy(targeting_strategy)  {}

void ReiterRegiment::update() {
  avoidAlliedCollision();

  if (formation_action != Soldier::FLEE) {
    if (detailed_update_cd == 0) {
      detailed_update_cd = random.randInt<uint16_t>(3, 5);
      updateFormation();
      if (formation_action != Soldier::FORMATION && formation_action != Soldier::CARACOLE) {
        updateToCenterPosition();
      }

      target = targeting_strategy.seekTargetFor(this);
    }
    detailed_update_cd--;
  }

  targeting_strategy.update();

  //Obliczanie odpowiednich pozycji dla formacji - trzymanie się na odpowiednią odległość
  if (target) {
    position_data_t dx = target->pos_x - pos_x;
    position_data_t dy = target->pos_y - pos_y;
    if (std::fabs(dx) < 1e-10) { dx = 1; }
    if (std::fabs(dy) < 1e-10) { dy = 1; }
    position_data_t dist = pgeom::getDistance(dx, dy);
    position_data_t dist_charge = 1.5*approx_radius;
    position_data_t desired_dist_wp = 1.5*target->approx_radius;
    position_data_t desired_dist_reg = desired_dist_wp + 3.5*approx_radius;
    position_data_t max_dist_reg = desired_dist_reg + 0.5*approx_radius;
    position_data_t reg_middle_x = target->pos_x - dx * desired_dist_reg / dist;
    position_data_t reg_middle_y = target->pos_y - dy * desired_dist_reg / dist;

    if (targeting_strategy.isAllowedToMove()) {
      setAngle(std::atan2(dy, dx));
    }
    if (dist < max_dist_reg) {
      goTo(reg_middle_x, reg_middle_y, 0.35);

      //Jeśli za blisko - rozwiązanie szyku
      if (dist < dist_charge) {
        if (target->soldiers.size() > 2*soldiers.size()) {
          for (auto s : soldiers) {
            s->setAction(Soldier::FLEE);
          }
          removeSelf();
        } else {
          if (formation_action != Soldier::CHARGE) {
            formation_action = Soldier::CHARGE;
          }
          for (auto s : soldiers) {
            s->setAction(Soldier::CHARGE);
          }
        }

      //Jeśli wystarczająco blisko ale nie za blisko - obliczanie trasy ataku dla jednostek
      } else {
        if (formation_action != Soldier::CARACOLE) {
          formation_action = Soldier::CARACOLE;
          for (auto s : soldiers) {
            s->setAction(Soldier::FORMATION);
          }
        }
        position_data_t wp_middle_x = target->pos_x - dx * desired_dist_wp / dist;
        position_data_t wp_middle_y = target->pos_y - dy * desired_dist_wp / dist;

        for (size_t i = 0; i < rows; ++i) {
          auto& wp = rows_data[i];
          wp.waypoints[0].first  = pos_x + (angle_cos * wp.row_offset - angle_sin * row_begin_dist);
          wp.waypoints[0].second = pos_y + (angle_sin * wp.row_offset + angle_cos * row_begin_dist);
          wp.waypoints[1].first  = wp_middle_x - angle_sin * target->approx_radius*wp.middle_wp_offset;
          wp.waypoints[1].second = wp_middle_y + angle_cos * target->approx_radius*wp.middle_wp_offset;
          wp.waypoints[2].first  = pos_x + (angle_cos * wp.row_offset - angle_sin * row_end_dist);
          wp.waypoints[2].second = pos_y + (angle_sin * wp.row_offset + angle_cos * row_end_dist);
        }
      }

    //Jeśli dalej - pójście w tamtym kierunku
    } else {
      if (formation_action != Soldier::FORMATION) {
        formation_action = Soldier::FORMATION;
        for (auto s : soldiers) {
          s->setAction(Soldier::FORMATION);
        }
      }
      if (targeting_strategy.isAllowedToMove()) {
        goTo(target->pos_x, target->pos_y, 0.5);
      }
    }
  } else {
    if (formation_action != Soldier::CHARGE) {
      formation_action = Soldier::CHARGE;
      for (auto s : soldiers) {
        s->setAction(Soldier::CHARGE);
      }
    }
  }

  //Regulowanie kolejnością caracole
  if (formation_action == Soldier::CARACOLE) {
    int16_t previous_clock = caracole_clock;
    caracole_clock++;
    if (caracole_clock >= SHIFT_PERIOD) {
      caracole_clock = 0;
    }
    int16_t prev_row = previous_clock * rows / SHIFT_PERIOD;
    int16_t curr_row = caracole_clock * rows / SHIFT_PERIOD;
    if (prev_row != curr_row) {
      rows_data[curr_row].middle_wp_offset = random.randDouble(-1, 1);
      for (auto s : soldiers) {
        if (auto p = dynamic_cast<Reiter*>(s)) {
          if (p->row == curr_row) {
            p->setAction(Soldier::CARACOLE);
          }
        }
      }
    }
  }

  if (soldiers.size() < initial_count / 2) {
    for (auto s : soldiers) {
      s->setAction(Soldier::FLEE);
    }
    removeSelf();
  }

  Regiment::update();
}

void ReiterRegiment::forgetAbout(Regiment *reg) {
  if (reg == target) {
    target = nullptr;
  }
}

void ReiterRegiment::updateFormation() {
  size_t soldier_count = soldiers.size();
  if (soldier_count == 0) {
    return;
  }

  auto formation = Formation::rectangleWithRows(soldier_count, rows, 1.2);
  size_t index = 0;
  if (!formation.empty()) {
    if (!formation[0].empty()) {
      row_begin_dist = formation[0][0].second - 1.2;
      row_end_dist = -row_begin_dist;
    }
  }
  for (size_t i = 0; i < rows; ++i) {
    auto& row = formation[i];
    if (!row.empty()) {
      rows_data[i].row_offset = row[0].first;
    }
    for (auto pos : row) {
      if (index == soldier_count) {
        std::cout<<"Too many positions returned\n";
        break;
      }
      soldiers[index]->setFormationOffset(pos.first, pos.second);
      if (auto p = dynamic_cast<Reiter*>(soldiers[index])) {
        p->row = static_cast<uint8_t>(i);
      }
      index++;
    }
  }
  if (index != soldier_count) {
    std::cout<<"Not enough positions returned\n";
  }
  approx_radius = std::sqrt((position_data_t)soldier_count*2);
}
