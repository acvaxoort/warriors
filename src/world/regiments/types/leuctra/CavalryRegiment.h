//
// Created by kuba on 28.05.19.
//

#ifndef WARRIORS_CAVELRYREGIMENT_H
#define WARRIORS_CAVELRYREGIMENT_H


#include <world/regiments/Regiment.h>
#include <entitysystem/Entity.h>
#include <util/RandomWrapper.hpp>
#include <world/entities/types/Soldier.h>
#include "Falange.h"

class CavalryRegiment : public Regiment {
public:
    CavalryRegiment(position_data_t x, position_data_t y, position_data_t angle,
                    position_data_t speed, int initial_count, int rows, position_data_t spacing);

    void update() override;

    void updateFormation();

    enum Action {
        PART1,
        PART2,
        PART3,
        PART4,
        PART5,
        PART6
    };
    bool isKingDead = false;
    bool pos_after_battle = false;
    position_data_t pos_after_battle_x = 0;

    position_data_t pos_after_battle_y = 0;



private:
    //Rozkaz ataku
    void orderToCharge();

    Action actions = PART1;

    //Rozkaz ponownego uformowania szeregu
    void restoreFormation();

    int detailed_update_cd = 0;
    RandomWrapper<std::minstd_rand> random;

    position_data_t speed;
    int initial_count;
    int rows;
    position_data_t spacing;
    Soldier::Action formation_action = Soldier::FORMATION;
    Regiment *target = nullptr;
    Regiment *currently_fighting = nullptr; // currently fighting ma inne znaczenie - inne oddziały patrzą na to sprawdzając czy trzeba pomóc
    position_data_t advancing_speed = 0.5;
    bool enemyCavalryDestroyed = false;



    void forgetAbout(Regiment *reg);


};

#endif //WARRIORS_CAVELRYREGIMENT_H
