//
// Created by aleksander on 21.05.19.
//

#include "Falange.h"
#include <world/Formations.h>
#include <util/PlanarGeometryUtil.h>
#include <iostream>
#include <world/entities/types/leuctra/Hoplite.h>


Falange::Falange(position_data_t x, position_data_t y, position_data_t angle,
                 position_data_t speed, int initial_count, int rows, position_data_t spacing, int fal_id)
        : Regiment(x, y, angle),
          speed(speed),
          initial_count(initial_count),
          rows(rows),
          spacing(spacing), fal_id(fal_id){}

void Falange::update() {
    //Jeśli jest cel, podązanie za nim
    //restoreFormation();

    if (target) {
        if (target->soldiers.empty()) {
            target = nullptr;
            detailed_update_cd = 0;

        } else {
            //fast_advance jest prawdą jeśli spieszymy z pomocą pobliskim sojusznikom
            //W przeciwnym wypadku, podążamy za przeciwnikiem wolniej
            if(team->name == "Thebans" && pos_x > 350)
            {
                goTo(pos_x,target->pos_y + 100,0.5 * speed); // unikanie walki srodkowyh i prawych rzedow
            }
            else
            {
                if(fabs(target->pos_y - pos_y) > 4)
                {
                    moveForward(target->pos_y, 0.5*speed);
                }
                else
                {
                    goTo(target->pos_x,pos_y,speed);
                }
            }


            //setAngle(std::atan2(target->pos_y - pos_y, target->pos_x - pos_x));
        }
    }

    avoidCollision();

    //Akcje nie wykonywane za każdym razem - poszukiwanie celów i wydawanie rozkazów

    // -Jesli wroga formacja jest odpowiednio blisko, rozkaz szarży
    // -Jeśli w pobliżu oddział sojuszniczy walczy, należy zmierzać w jego kierunku
    // -Jeśli istnieje oddział przeciwnika, należy zmierzać w jego kierunku
    // -Jeśli wszystkie oddziały przeciwnika są rozbite, wydanie rozkazu szarży
    if (detailed_update_cd == 0) {
        detailed_update_cd = random.randInt(4, 5);



        position_data_t target_distance_sqr = 1e300;
        position_data_t target_pos_x = 200;
        position_data_t nearby_distance_sqr = approx_radius + 64;
        nearby_distance_sqr *= nearby_distance_sqr;
        target = nullptr;

        //Znalezienie najbliższego oddziału przeciwnika
        for (auto enemy : team->enemies) {
            for (auto &reg : enemy->regiments) {
                if (auto p = dynamic_cast<Falange *>(reg.get())) {
                    position_data_t checked_distance_x = fabs(pos_x - reg->pos_x);
                    position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                                 pos_y - reg->pos_y);
                    if (checked_distance_x < target_pos_x && !reg->soldiers.empty()) {
                        target = reg.get();
                        target_pos_x = checked_distance_x;
                        target_distance_sqr = checked_distance_sqr;
                    }


                }
            }
        }
        if (!target) {
            for (auto enemy : team->enemies) {
                for (auto &reg : enemy->regiments) {
                    if (auto p = dynamic_cast<Falange *>(reg.get())) {
                        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                                     pos_y - reg->pos_y);
                        if (checked_distance_sqr < target_distance_sqr && !reg->soldiers.empty()) {
                            target = reg.get();
                            target_distance_sqr = checked_distance_sqr;
                        }


                    }
                }
            }
        }

//Istnieje jakikolwiek oddział przeciwnika
        if (target) {


            currently_fighting = nullptr;
        }
        if(isKingDead)
        {
            restoreFormation();
            target = nullptr;
        }

//Nie istnieje żaden oddział przeciwnika
        else {
            restoreFormation();

            currently_fighting = nullptr;
        }

        updateFormation();

        if (formation_action == Soldier::CHARGE) {
            updateToCenterPosition();

        }
    }
    detailed_update_cd--;



    Regiment::update();

}

void Falange::updateFormation() {
    size_t soldier_count = soldiers.size();
    if (soldier_count == 0) {
        return;
    }

    auto formation = Formation::rectangle((int) soldier_count, rows, spacing);
    for (size_t i = 0; i < soldier_count; ++i) {
        soldiers[i]->setFormationOffset(formation.positions[i].first, formation.positions[i].second);
    }
    approx_radius = std::sqrt((position_data_t) soldier_count * spacing);
}

//Rozkaz ataku
void Falange::orderToCharge() {
    if (formation_action != Soldier::CHARGE) {
        formation_action = Soldier::CHARGE;

        for (auto p : soldiers) {
            p->setAction(Soldier::CHARGE);
        }
    }
}



//Rozkaz uformowania formacji/szeregu
void Falange::restoreFormation() {
    if (formation_action != Soldier::FORMATION) {
        formation_action = Soldier::FORMATION;

        updateToCenterPosition();
        updateFormation();

        for (auto p : soldiers) {
            p->setAction(Soldier::FORMATION);
        }
    }
}

void Falange::forgetAbout(Regiment *reg) {
    if (target == reg) {
        target = nullptr;
    }
    if (currently_fighting == reg) {
        target = nullptr;
    }
}


void Falange::moveForward(position_data_t y, position_data_t speed) {
    //position_data_t dx = x - pos_x;
    position_data_t dy = y - pos_y;
    if (pos_x == 0 && dy == 0) {
        return;
    }
    position_data_t dpos = pgeom::getDistance(pos_x, dy);
    if (dpos > speed) {
        position_data_t mul = speed / dpos;
        //vel_x += pos_x*mul;
        vel_y += dy * mul;
    }
}


void Falange::orderToAttack() {
    if (formation_action != Soldier::ATTACK) {
        formation_action = Soldier::ATTACK;

        for (auto p : soldiers) {
            p->setAction(Soldier::ATTACK);
        }
    }
}

void Falange::avoidCollision() {
    for (auto &reg : team->regiments) {
        if (reg.get() == this) {
            continue;
        }

        position_data_t dx = pos_x - reg->pos_x;
        position_data_t dy = pos_y - reg->pos_y;
        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(dx, dy);
        position_data_t allowed_distance = 15;
        if (checked_distance_sqr < allowed_distance*allowed_distance) {
            position_data_t dist = std::sqrt(checked_distance_sqr);
            if (dist > 0.01) {
                vel_x += 0.1 * dx / dist;
                vel_y += 0.1 * dy / dist;
            }
        }
    }
}

void Falange::setIsKingDead(bool isKingDead) {
    this->isKingDead = isKingDead;
}
