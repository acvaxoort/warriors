//
// Created by kuba on 24.05.19.
//

#ifndef WARRIORS_FALANGE_H
#define WARRIORS_FALANGE_H

#include <world/regiments/Regiment.h>
#include <entitysystem/Entity.h>
#include <util/RandomWrapper.hpp>
#include <world/entities/types/Soldier.h>

#include <world/regiments/Regiment.h>
#include <entitysystem/Entity.h>
#include <util/RandomWrapper.hpp>
#include <world/entities/types/Soldier.h>

class Falange : public Regiment {
public:

    //Zmierzanie na wprost
    void moveForward(position_data_t y, position_data_t speed);


    Falange(position_data_t x, position_data_t y, position_data_t angle,
            position_data_t speed, int initial_count, int rows, position_data_t spacing, int fal_id);

    void update() override;

    //Nadanie nowych pozycji żołnierzom w oddziale
    void updateFormation();

    void forgetAbout(Regiment *reg) override;

    Regiment *currently_fighting = nullptr;

    //id sluzace do wyznaczenia zlotego zastepu i elitarnego oddziału spartanskiego
    int fal_id;

//Rozkaz szarży (po przebiciu się przez falange)
    void orderToCharge();

    void setIsKingDead(bool isKingDead);

private:
    //Rozkaz ataku
    void orderToAttack();

    //Rozkaz ponownego uformowania szeregu
    void restoreFormation();

    int detailed_update_cd = 0;
    RandomWrapper<std::minstd_rand> random;


    position_data_t speed;
    int initial_count;
    int rows;
    position_data_t spacing;
    Soldier::Action formation_action = Soldier::FORMATION;
    Regiment *target = nullptr;// currently fighting ma inne znaczenie - inne oddziały patrzą na to sprawdzając czy trzeba pomóc
    bool isKingDead = false;

    void avoidCollision();

};

#endif //WARRIORS_FALANGE_H