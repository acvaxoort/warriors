
#include "CavalryRegiment.h"
#include <world/Formations.h>
#include <util/PlanarGeometryUtil.h>
#include <iostream>
#include <world/entities/types/leuctra/Cavalryman.h>
#include <world/entities/types/leuctra/Hoplite.h>

CavalryRegiment::CavalryRegiment(position_data_t x, position_data_t y, position_data_t angle,
                                 position_data_t speed, int initial_count, int rows, position_data_t spacing)
        : Regiment(x, y, angle),
          speed(speed),
          initial_count(initial_count),
          rows(rows),
          spacing(spacing) {}

void CavalryRegiment::update() {
    switch (actions) {
        case PART1 : {
            if (target) {
                if (target->soldiers.empty()) {
                    target = nullptr;
                    detailed_update_cd = 0;

                } else {
                    //fast_advance jest prawdą jeśli spieszymy z pomocą pobliskim sojusznikom
                    //W przeciwnym wypadku, podążamy za przeciwnikiem wolniej
                    goTo(target->pos_x, target->pos_y, speed);
                    setAngle(std::atan2(pos_y - target->pos_y, pos_x - target->pos_x));
                }
            }

            //Odpychanie się pobliskich oddziałów sojuszniczych


            for (auto &reg : team->regiments) {
                if (reg.get() == this) {
                    continue;
                }

                position_data_t dx = pos_x - reg->pos_x;
                position_data_t dy = pos_y - reg->pos_y;
                position_data_t checked_distance_sqr = pgeom::getDistanceSqr(dx, dy);
                position_data_t allowed_distance = approx_radius + reg->approx_radius;
                if (checked_distance_sqr < allowed_distance * allowed_distance) {
                    position_data_t dist = std::sqrt(checked_distance_sqr);
                    if (dist > 0.01) {
                        vel_x += 0.1 * dx / dist;
                        vel_y += 0.1 * dy / dist;
                    }
                }
            }

            //Akcje nie wykonywane za każdym razem - poszukiwanie celów i wydawanie rozkazów

            // -Jesli wroga formacja jest odpowiednio blisko, rozkaz szarży
            // -Jeśli w pobliżu oddział sojuszniczy walczy, należy zmierzać w jego kierunku
            // -Jeśli istnieje oddział przeciwnika, należy zmierzać w jego kierunku
            // -Jeśli wszystkie oddziały przeciwnika są rozbite, wydanie rozkazu szarży
            if (detailed_update_cd == 0) {
                detailed_update_cd = random.randInt(3, 5);


                position_data_t target_distance_sqr = 1e300;
                position_data_t nearby_distance_sqr = approx_radius + 32;
                nearby_distance_sqr *= nearby_distance_sqr;
                target = nullptr;

                //Znalezienie najbliższego oddziału przeciwnika
                for (auto enemy : team->enemies) {
                    for (auto &reg : enemy->regiments) {
                        if (auto p = dynamic_cast<Cavalryman *>(reg->soldiers[0])) {
                            position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                                         pos_y - reg->pos_y);
                            if (checked_distance_sqr < target_distance_sqr && !reg->soldiers.empty()) {
                                target = reg.get();
                                target_distance_sqr = checked_distance_sqr;
                            }
                        }
                    }
                }

                //Istnieje jakikolwiek oddział przeciwnika
                if (target) {

                    //Oddział przeciwnika jest w pobliżu
                    if (target_distance_sqr < nearby_distance_sqr) {
                        orderToCharge();
                    } else {

                        //W przeciwnym wypadku - sprawdzenie czy jest w pobliżu oddział sojusznika w bezpośredniej walce
                        target_distance_sqr = 1e300;
                        nearby_distance_sqr = approx_radius + 64;
                        nearby_distance_sqr *= nearby_distance_sqr;
                        CavalryRegiment *ally_in_need = nullptr;
                        for (auto &reg : team->regiments) {
                            if (auto p = dynamic_cast<CavalryRegiment *>(reg.get())) {
                                if (p->currently_fighting && p != this) {
                                    position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                                                 pos_y - reg->pos_y);
                                    if (checked_distance_sqr < target_distance_sqr) {
                                        ally_in_need = p;
                                        target_distance_sqr = checked_distance_sqr;
                                    }
                                }
                            }
                        }
                        if (ally_in_need && target_distance_sqr < nearby_distance_sqr) {
                            restoreFormation();
                            currently_fighting = nullptr;
                            target = ally_in_need->currently_fighting;
                        } else {

                            //Jeśli nie ma to podążać za najbliższym oddziałem przeciwnika
                            restoreFormation();
                            currently_fighting = nullptr;
                        }
                    }

                    //Nie istnieje żaden oddział przeciwnika
                } else {
                    restoreFormation();
                    actions = PART2;
                }

                if (formation_action == Soldier::CHARGE) {
                    updateToCenterPosition();
                }
            }
            detailed_update_cd--;

            if (soldiers.size() == 0) {
                removeSelf();
            }
        }
            break;

        case PART2 : {
            goTo(10.0f, pos_y, 1.8f);
            restoreFormation();
            position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - 10, 0.0);
            if (checked_distance_sqr < 80 * 80) {
                actions = PART3;
            }
        }
            break;
        case PART3 : {
            goTo(10.0f, 380.0f, 1.8f);
            position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - 10, pos_y - 380.0);
            if (checked_distance_sqr < 80 * 80) {
                actions = PART4;
            }

        }
            break;
        case PART4 : {
            if (target) {
                if (target->soldiers.empty()) {
                    target = nullptr;
                    detailed_update_cd = 0;

                } else {
                    //fast_advance jest prawdą jeśli spieszymy z pomocą pobliskim sojusznikom
                    //W przeciwnym wypadku, podążamy za przeciwnikiem wolniej
                    goTo(target->pos_x, target->pos_y, speed);
                    setAngle(std::atan2(pos_y - target->pos_y, pos_x - target->pos_x));
                }
            }

            //Odpychanie się pobliskich oddziałów sojuszniczych


            for (auto &reg : team->regiments) {
                if (reg.get() == this) {
                    continue;
                }

                position_data_t dx = pos_x - reg->pos_x;
                position_data_t dy = pos_y - reg->pos_y;
                position_data_t checked_distance_sqr = pgeom::getDistanceSqr(dx, dy);
                position_data_t allowed_distance = approx_radius + reg->approx_radius;
                if (checked_distance_sqr < allowed_distance * allowed_distance) {
                    position_data_t dist = std::sqrt(checked_distance_sqr);
                    if (dist > 0.01) {
                        vel_x += 0.1 * dx / dist;
                        vel_y += 0.1 * dy / dist;
                    }
                }
            }

            //Akcje nie wykonywane za każdym razem - poszukiwanie celów i wydawanie rozkazów

            // -Jesli wroga formacja jest odpowiednio blisko, rozkaz szarży
            // -Jeśli w pobliżu oddział sojuszniczy walczy, należy zmierzać w jego kierunku
            // -Jeśli istnieje oddział przeciwnika, należy zmierzać w jego kierunku
            // -Jeśli wszystkie oddziały przeciwnika są rozbite, wydanie rozkazu szarży
            if (detailed_update_cd == 0) {
                detailed_update_cd = random.randInt(3, 5);


                position_data_t target_distance_sqr = 1e300;
                position_data_t nearby_distance_sqr = approx_radius + 64;
                nearby_distance_sqr *= nearby_distance_sqr;
                target = nullptr;

                //Znalezienie najbliższego oddziału przeciwnika
                for (auto enemy : team->enemies) {
                    for (auto &reg : enemy->regiments) {
                        position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                                     pos_y - reg->pos_y);
                        if (checked_distance_sqr < target_distance_sqr && !reg->soldiers.empty()) {
                            target = reg.get();
                            target_distance_sqr = checked_distance_sqr;
                        }
                    }
                }


                //Istnieje jakikolwiek oddział przeciwnika
                if (target) {

                    //Oddział przeciwnika jest w pobliżu
                    if (target_distance_sqr < nearby_distance_sqr) {
                        orderToCharge();
                        //removeSelf(); // usuniecie odzdzialu , zeby nie kolidowac z innym odzdzialami
                    } else {
                        Falange *ally_in_need = nullptr;
                        for (auto &reg : team->regiments) {
                            if (auto p = dynamic_cast<Falange *>(reg.get())) {
                                if (p->currently_fighting) {
                                    position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - reg->pos_x,
                                                                                                 pos_y - reg->pos_y);
                                    ally_in_need = p;
                                }
                            }
                        }
                        if (ally_in_need) {
                            ally_in_need = nullptr;
                        } else {

                            //Jeśli nie ma to podążać za najbliższym oddziałem przeciwnika
                            restoreFormation();
                            currently_fighting = nullptr;
                        }
                    }
                    if (isKingDead) {
                        restoreFormation();
                        target = nullptr;
                        formation_action = Soldier::FORMATION;
                        speed = 0.0;
                        currently_fighting = nullptr;
                        actions = PART5;
                    }


                    //Nie istnieje żaden oddział przeciwnika
                } else {
                    setAngle(0.0);
                    restoreFormation();

                }

                if (formation_action == Soldier::CHARGE) {
                    updateToCenterPosition();
                }
            }
            detailed_update_cd--;

            if (soldiers.size() < initial_count / 10) {
                removeSelf();
            }
        }
            break;
        case PART5 : {
            speed = 0.0;
            setAngle(0.0);
            restoreFormation();
            if(!pos_after_battle)
            {
                pos_after_battle_x = pos_x;
                pos_after_battle_y = pos_y - 100;
                pos_after_battle = true;
            }
            goTo(pos_after_battle_x,pos_after_battle_y,1.0f);
            position_data_t checked_distance_sqr = pgeom::getDistanceSqr(pos_x - pos_after_battle_x, pos_y - pos_after_battle_y);
            if (checked_distance_sqr < 4 * 4) {
                actions = PART6;
            }

        }break;
        case PART6 : {
            speed = 0.0;
            setAngle(0.0);
            restoreFormation();
        }
    }
    Regiment::update();
}


//Rozkaz ataku
void CavalryRegiment::orderToCharge() {
    if (formation_action != Soldier::CHARGE) {
        formation_action = Soldier::CHARGE;

        for (auto p : soldiers) {
            p->setAction(Soldier::CHARGE);
        }
    }
}

//Rozkaz uformowania formacji/szeregu
void CavalryRegiment::restoreFormation() {
    if (formation_action != Soldier::FORMATION) {
        formation_action = Soldier::FORMATION;

        updateToCenterPosition();
        updateFormation();

        for (auto p : soldiers) {
            p->setAction(Soldier::FORMATION);
        }
    }
}

void CavalryRegiment::forgetAbout(Regiment *reg) {
    if (target == reg) {
        target = nullptr;
    }
    if (currently_fighting == reg) {
        target = nullptr;
    }
}

void CavalryRegiment::updateFormation() {
    size_t soldier_count = soldiers.size();
    if (soldier_count == 0) {
        return;
    }

    auto formation = Formation::rectangle((int) soldier_count, rows, spacing);
    for (size_t i = 0; i < soldier_count; ++i) {
        soldiers[i]->setFormationOffset(formation.positions[i].first, formation.positions[i].second);
    }
    approx_radius = std::sqrt((position_data_t) soldier_count * spacing);
}

