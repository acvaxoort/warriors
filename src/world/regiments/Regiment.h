//
// Created by aleksander on 04.05.19.
//

#ifndef WARRIORS_REGIMENT_H
#define WARRIORS_REGIMENT_H

class Soldier;
class Team;

#include <entitysystem/Entity.h>
#include <world/entities/SimulationEntitySystem.h>

class Regiment {
public:
  Regiment(position_data_t pos_x, position_data_t pos_y, position_data_t angle);

  virtual ~Regiment() = default;

  //Wykonywane po włożeniu do systemu
  virtual void init();

  //Wynkonywane z każdym updatem
  virtual void update();

  //Pozwala na usunięcie odwołań (do obiektu który zaraz przestanie istnieć)
  virtual void forgetAbout(Regiment* reg);

  //Zaprzestanie istnienia od następnego updatu
  void removeSelf();

  //Dodaje żołnierza, ustawia też pole regiment w dodawanym żołnierzu
  void addSoldier(Soldier* soldier);
  void removeSoldier(Soldier* soldier);


  std::vector<Soldier*> soldiers;
  Team* team;

  //Ustawia kąt ale także sinus i cosinus
  void setAngle(position_data_t formation_angle);

  //Zmierzanie w kierunku
  void goTo(position_data_t x, position_data_t y, position_data_t speed);

  //Ustawienie pozycji na średnią swoich żołnierzy - do śledzenia pozycji gdy oddział walczy
  void updateToCenterPosition();

  //Natychmiastowe ustawienie żołnierzom ich pozycji na mapie - używać tylko przy tworzeniu (poza EntitySystem)
  void setSoldiersInitPositions();

  //Unikanie kolizji z oddziałami sojuszniczymi
  void avoidAlliedCollision();

  position_data_t pos_x;
  position_data_t pos_y;
  position_data_t vel_x;
  position_data_t vel_y;
  position_data_t angle;
  position_data_t angle_sin;
  position_data_t angle_cos;
  position_data_t approx_radius;
  position_data_t intertia = 0.85;
  RandomWrapper<std::minstd_rand> random;
  SimulationEntitySystem* system;
};


#endif //WARRIORS_REGIMENT_H
