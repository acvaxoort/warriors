//
// Created by aleksander on 01.06.19.
//

#ifndef WARRIORS_TARGETINGPOLICY_H
#define WARRIORS_TARGETINGPOLICY_H

#include "Regiment.h"

class TargetingStrategy {
public:
  void update();
  bool isAllowedToMove();
  Regiment* seekTargetFor(Regiment* this_regiment);

  bool prioritize_infantry = false;
  bool reckless_charge = false;
  int waiting_time = 0;
};


#endif //WARRIORS_TARGETINGPOLICY_H
