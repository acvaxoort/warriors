//
// Created by aleksander on 31.05.19.
//

#ifndef WARRIORS_RENDERSHAPES_H
#define WARRIORS_RENDERSHAPES_H

#include <SFML/Graphics.hpp>

namespace rendershapes {

  void prepare();

  extern sf::ConvexShape cavalry_base;
  extern sf::ConvexShape hussar;
  extern sf::ConvexShape petyhorzec;
  extern sf::ConvexShape infantry_base;
  extern sf::ConvexShape pikeman;
  extern sf::ConvexShape musketeer;
  extern sf::ConvexShape cannon;

}

#endif //WARRIORS_RENDERSHAPES_H
