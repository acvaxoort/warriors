//
// Created by aleksander on 15.02.19.
//

#include "SmoothView.h"
#include "PlanarGeometryUtil.h"
#include "SfmlWindowUtil.h"
#include <cmath>

SmoothView::SmoothView(double scrollSpeed, double scrollFriction, double scaleSpeed, double scaleFriction)
    : scrollSpeed(scrollSpeed+scrollFriction),
      scrollFriction(scrollFriction),
      scaleSpeed(scaleSpeed+scaleFriction),
      scaleFriction(scaleFriction) {
}

void SmoothView::setPosition(double x, double y) {
  this->x = x;
  this->y = y;
  this->dx = 0;
  this->dy = 0;
}

void SmoothView::setScale(double s) {
  this->scale = s;
  this->dscale = 0;
}

void SmoothView::accelerate(double dx2, double dy2) {
  dx += dx2*scrollSpeed;
  dy += dy2*scrollSpeed;
  if (dx < -scrollSpeedMax)    { dx = -scrollSpeedMax; }
  if (dx > scrollSpeedMax)     { dx = scrollSpeedMax; }
  if (dy < -scrollSpeedMax)    { dy = -scrollSpeedMax; }
  if (dy > scrollSpeedMax)     { dy = scrollSpeedMax; }
}

void SmoothView::accelerateScale(double ds2) {
  dscale += ds2*scaleSpeed;
  if (dscale < -scaleSpeedMax) { dscale = -scaleSpeedMax; }
  if (dscale > scaleSpeedMax)  { dscale = scaleSpeedMax; }
}

void SmoothView::setScrollSpeedMax(double v) {
  scrollSpeedMax = v;
}

void SmoothView::setScaleSpeedMax(double v) {
  scaleSpeedMax = v;
}

void SmoothView::setScrollBounds(double xmin, double xmax, double ymin, double ymax) {
  xMin = xmin;
  xMax = xmax;
  yMin = ymin;
  yMax = ymax;
}

void SmoothView::setScaleBounds(double smin, double smax) {
  scaleMin = smin;
  scaleMax = smax;
}

void SmoothView::mouseDrag(const sf::Window& window, int dmx, int dmy) {
  auto view_size = getSfmlView(window).getSize();
  auto res = window.getSize();
  dx = -view_size.x*dmx/res.x/expscale;
  dy = -view_size.y*dmy/res.y/expscale;
}

void SmoothView::mouseScroll(const sf::Window& window, float dmz) {
  auto mouse_pos = sf::Mouse::getPosition(window);
  auto& r = dynamic_cast<const sf::RenderWindow&>(window);
  auto computed_view = getSfmlView(window);
  auto mouse_game_pos = r.mapPixelToCoords(mouse_pos, computed_view);
  scale -= dmz * scaleSpeedMax * 3;
  double new_expscale = exp(scale);
  double scale_proportion = new_expscale/expscale;
  x += (mouse_game_pos.x-x)*(1-scale_proportion);
  y += (mouse_game_pos.y-y)*(1-scale_proportion);
  expscale = new_expscale;
}

void SmoothView::tick() {
  scale += dscale;
  expscale = exp(scale);
  x += dx*expscale;
  y += dy*expscale;

  if (x < xMin) { x = xMin; }
  if (y < yMin) { y = yMin; }
  if (x > xMax) { x = xMax; }
  if (y > yMax) { y = yMax; }
  if (scale < scaleMin) { scale = scaleMin; }
  if (scale > scaleMax) { scale = scaleMax; }

  auto tickVariable = [] (double& dv, double friction) {
    if (dv > -friction && dv < friction) {
      dv = 0;
    } else if (dv < 0) {
      dv += friction;
    } else {
      dv -= friction;
    }
  };

  tickVariable(dx, scrollFriction);
  tickVariable(dy, scrollFriction);
  tickVariable(dscale, scaleFriction);
}

sf::View SmoothView::getSfmlView(const sf::Window& window) {
  return sfmlwutil::getView(window, x, y, expscale);
}