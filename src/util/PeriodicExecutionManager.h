//
// Created by aleksander on 08.02.19.
//

#ifndef WARRIORS_PERIODICEXECUTIONMANAGER_H
#define WARRIORS_PERIODICEXECUTIONMANAGER_H

#include <mutex>
#include <chrono>
#include "AccumulatedMeasure.hpp"

class PeriodicExecutionManager {
public:
  explicit PeriodicExecutionManager(double updates_per_second, int64_t timer_reset_ticks = 0);
  virtual ~PeriodicExecutionManager() = default;
  void run();
  void stop();
  void pause();
  void resume();
  void startMeasuring();
  void resumeMeasuring();
  void stopMeasuring();
  void setUpdatesPerSecond(double updates_per_second);
  virtual void update() = 0;

  AccumulatedMeasure<double> measure;
private:
  void actuallySetUpdatesPerSecond(double updates_per_second);

  int update_count = 0;
  double update_rate = 0;
  int64_t update_time = 0;
  int64_t update_period = 0;
  int64_t update_compensation = 0;
  int64_t ticks_since_reset = 0;
  int64_t timer_reset_ticks;
  std::chrono::time_point<std::chrono::high_resolution_clock> run_time_measure_clock;
  std::chrono::time_point<std::chrono::high_resolution_clock> update_clock;
  bool running = false;
  bool paused = false;
  bool do_measure = false;
  bool do_set_new_rate = false;
  double new_rate;
  double last_update_rate = 0;
};


#endif //WARRIORS_PERIODICEXECUTIONMANAGER_H
