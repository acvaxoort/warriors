//
// Created by aleksander on 22.04.19.
//

#ifndef INTERACTIVECOMPLEXFRACTAL_LUAUTIL_H
#define INTERACTIVECOMPLEXFRACTAL_LUAUTIL_H

#include <stdexcept>
#include <lua5.2/lua.h>
#include <sstream>

namespace luautil {

  class LoadingError : public std::runtime_error {
  public:
    explicit LoadingError(const std::string& msg) : runtime_error(msg) {}
  };

  void unloadTable(lua_State* L) {
    if (lua_istable(L, -1)) {
      lua_pop(L, 1);
    } else {
      throw LoadingError("Cannot unload table when none is loaded");
    }
  }

  template <typename num_t>
  num_t getValueNumber(lua_State* L, const std::string& key) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    if (!lua_isnumber(L, -1)) {
      lua_pop(L, 1);
      throw LoadingError("Could not get a number with key: " + key);
    }
    int result = (num_t) lua_tonumber(L, -1);
    lua_pop(L, 1);
    return result;
  }

  bool getValueBool(lua_State* L, const std::string& key) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    if (!lua_isboolean(L, -1)) {
      lua_pop(L, 1);
      throw LoadingError("Could not get a boolean with key: " + key);
    }
    bool result = (bool) lua_toboolean(L, -1);
    lua_pop(L, 1);
    return result;
  }

  std::string getValueString(lua_State* L, const std::string& key) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    if (!lua_isstring(L, -1)) {
      lua_pop(L, 1);
      throw LoadingError("Could not get a string with key: " + key);
    }
    std::string result(lua_tostring(L, -1), lua_rawlen(L, -1));
    lua_pop(L, 1);
    return result;
  }

  template <typename num_t>
  void getValueNumber(lua_State* L, const std::string& key, num_t& val) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    if (!lua_isnumber(L, -1)) {
      lua_pop(L, 1);
      throw LoadingError("Could not get a number with key: " + key);
    }
    val = (num_t) lua_tonumber(L, -1);
    lua_pop(L, 1);
  }

  void getValueBool(lua_State* L, const std::string& key, bool& val) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    if (!lua_isboolean(L, -1)) {
      lua_pop(L, 1);
      throw LoadingError("Could not get a boolean with key: " + key);
    }
    val = (bool) lua_toboolean(L, -1);
    lua_pop(L, 1);
  }

  void getValueString(lua_State* L, const std::string& key, std::string& val) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    if (!lua_isstring(L, -1)) {
      lua_pop(L, 1);
      throw LoadingError("Could not get a string with key: " + key);
    }
    val = std::string(lua_tostring(L, -1), lua_rawlen(L, -1));
    lua_pop(L, 1);
  }

  template <typename num_t>
  bool getValueNumberOptional(lua_State* L, const std::string& key, num_t& val) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    bool found = false;
    if (lua_isnumber(L, -1)) {
      val = (num_t) lua_tonumber(L, -1);
      found = true;
    }
    lua_pop(L, 1);
    return found;
  }

  bool getValueBoolOptional(lua_State* L, const std::string& key, bool& val) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    bool found = false;
    if (lua_isboolean(L, -1)) {
      val = (bool) lua_toboolean(L, -1);;
      found = true;
    }
    lua_pop(L, 1);
    return found;
  }

  bool getValueStringOptional(lua_State* L, const std::string& key, std::string& val) {
    if (lua_istable(L, -1)) {
      lua_pushstring(L, key.c_str());
      lua_gettable(L, -2);
    } else {
      lua_getglobal(L, key.c_str());
    }
    bool found = false;
    if (lua_isstring(L, -1)) {
      val = std::string(lua_tostring(L, -1), lua_rawlen(L, -1));
      found = true;
    }
    lua_pop(L, 1);
    return found;
  }

}


#endif //INTERACTIVECOMPLEXFRACTAL_LUAUTIL_H
