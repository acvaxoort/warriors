//
// Created by aleksander on 08.04.19.
//

#ifndef WARRIORS_TIMEUTIL_H
#define WARRIORS_TIMEUTIL_H

#include <chrono>

namespace timeutil {
  typedef std::chrono::time_point<std::chrono::high_resolution_clock> point;
  inline timeutil::point now() {
    return std::chrono::high_resolution_clock::now();
  }
  inline double duration(timeutil::point p) {
    return std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - p).count();
  }
}

#endif //WARRIORS_TIMEUTIL_H
