//
// Created by aleksander on 08.02.19.
//

#include <thread>
#include <iostream>
#include "PeriodicExecutionManager.h"

PeriodicExecutionManager::PeriodicExecutionManager(double updates_per_second, int64_t timer_reset_ticks):
  timer_reset_ticks(timer_reset_ticks),
  new_rate(updates_per_second) {
  actuallySetUpdatesPerSecond(updates_per_second);
}

void PeriodicExecutionManager::run() {
  if (running) {
    throw std::runtime_error("Already running");
  }
  running = true;
  while (running) {
    update_time = std::chrono::duration_cast<std::chrono::microseconds>
        (std::chrono::high_resolution_clock::now() - update_clock).count();
    if (update_time >= update_period + update_compensation)
    {
      if (do_measure && !paused && update_count > 1) {
        measure.add(1000000.0/update_time);
      }
      update_clock = std::chrono::high_resolution_clock::now();

      int64_t actual_time = std::chrono::duration_cast<std::chrono::microseconds>
          (std::chrono::high_resolution_clock::now() - run_time_measure_clock).count();

      if (update_rate > 0.0) {
        update_compensation = static_cast<int64_t>(1000000.0 / update_rate * update_count - actual_time);
      } else {
        update_compensation = 0;
      }

      ++update_count;
      ++ticks_since_reset;

      /*
      if (timer_reset_ticks > 0 && ticks_since_reset >= timer_reset_ticks) {
        ticks_since_reset = 0;
        do_set_new_rate = true;
      }*/

      if (do_set_new_rate) {
        actuallySetUpdatesPerSecond(new_rate);
        do_set_new_rate = false;
      }
      if (!paused) {
        update();
      }
    }
    if (update_period + update_compensation - update_time > 1000) {
      std::this_thread::sleep_for(std::chrono::microseconds(update_period + update_compensation - update_time - 1000));
    }
  }
}

void PeriodicExecutionManager::stop() {
  running = false;
}

void PeriodicExecutionManager::pause() {
  paused = true;
}

void PeriodicExecutionManager::resume() {
  paused = false;
}

void PeriodicExecutionManager::startMeasuring() {
  measure = AccumulatedMeasure<double>();
  do_measure = true;
}

void PeriodicExecutionManager::resumeMeasuring() {
  do_measure = true;
}

void PeriodicExecutionManager::stopMeasuring() {
  do_measure = false;
}

void PeriodicExecutionManager::setUpdatesPerSecond(double updates_per_second) {
  do_set_new_rate = true;
  new_rate = updates_per_second;
}

void PeriodicExecutionManager::actuallySetUpdatesPerSecond(double updates_per_second) {
  update_rate = updates_per_second;
  if (update_rate > 0) {
    update_period = static_cast<int64_t>(1000000.0 / update_rate);
  } else {
    update_period = 0;
  }
  update_compensation = 0;
  update_count = 0;
  run_time_measure_clock = std::chrono::high_resolution_clock::now();
}
