//
// Created by aleksander on 13.07.18.
//

#include "SfmlWindowUtil.h"
#include "PlanarGeometryUtil.h"
#include <lua5.2/lua.hpp>
#include <fstream>

namespace sfmlwutil {

  sf::View getView(const sf::Window &window, float x, float y, float scale) {
    auto s = window.getSize();
    scale *= 1.0f / (s.x + s.y);
    sf::View result({x,           y},
                    {s.x * scale, s.y * scale});
    sf::View t({-50, -50, 100, 100});

    return result;
  }

  sf::View getDefaultView(const sf::Window &window) {
    auto s = window.getSize();
    return sf::View({0, 0, (float) s.x, (float) s.y});
  }

  sf::View interpolate(const sf::View& previous, const sf::View& current, float fraction) {
    return {
      pgeom::interpolate<float>(previous.getCenter(), current.getCenter(), fraction),
      pgeom::interpolate<float>(previous.getSize(), current.getSize(), fraction)
    };
  }

  sf::Color interpolate(const sf::Color& col1, const sf::Color& col2, float fraction) {
    float fr2 = 1-fraction;
    return {static_cast<sf::Uint8>(fr2 * col1.r + fraction * col2.r),
            static_cast<sf::Uint8>(fr2 * col1.g + fraction * col2.g),
            static_cast<sf::Uint8>(fr2 * col1.b + fraction * col2.b),
            static_cast<sf::Uint8>(fr2 * col1.a + fraction * col2.a) };
  }

  const char* CONFIG_FILENAME = "displayconfig.lua";

  void openWindowFromConfig(sf::Window& window) {
    lua_State* L = luaL_newstate();
    if (luaL_loadfile(L, CONFIG_FILENAME) != LUA_OK) {
      lua_close(L);
      return openDefaultWindow(window);
    }
    if (lua_pcall(L, 0, 0, 0) != LUA_OK) {
      lua_close(L);
      return openDefaultWindow(window);
    }
    int width, height;
    bool fullscreen;

    lua_getglobal(L, "width");
    if (lua_isnumber(L, -1)) {
      width = static_cast<int>(lua_tonumber(L, -1));
    } else {
      lua_close(L);
      return openDefaultWindow(window);
    }
    lua_pop(L, 1);

    lua_getglobal(L, "height");
    if (lua_isnumber(L, -1)) {
      height = static_cast<int>(lua_tonumber(L, -1));
    } else {
      lua_close(L);
      return openDefaultWindow(window);
    }
    lua_pop(L, 1);

    lua_getglobal(L, "fullscreen");
    if (lua_isboolean(L, -1)) {
      fullscreen = static_cast<bool>(lua_toboolean(L, -1));
    } else {
      lua_close(L);
      return openDefaultWindow(window);
    }
    lua_pop(L, 1);

    if (width <= 0 && height <= 0) {
      lua_close(L);
      return openDefaultWindow(window);
    }
    lua_close(L);

    auto v = sf::VideoMode::getDesktopMode();
    v.width = static_cast<unsigned int>(width);
    v.height = static_cast<unsigned int>(height);
    sf::ContextSettings settings;
    settings.antialiasingLevel = 1;
    window.create(v, "warriors", fullscreen ? sf::Style::Fullscreen : sf::Style::Default, settings);
    window.setVerticalSyncEnabled(true);
  }

  void openDefaultWindow(sf::Window& window) {
    auto v = sf::VideoMode::getDesktopMode();
    v.width = 800;
    v.height = 600;
    sf::ContextSettings settings;
    settings.antialiasingLevel = 1;
    window.create(v, "warriors", sf::Style::Default, settings);
    window.setVerticalSyncEnabled(true);
    std::ofstream ofs;
    ofs.open(CONFIG_FILENAME);
    ofs<<"width = 800\nheight = 600\nfullscreen = false\n";
    ofs.close();
  }

}