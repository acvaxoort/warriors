//
// Created by aleksander on 15.10.18.
//

#ifndef WARRIORS_FONTS_H
#define WARRIORS_FONTS_H


#include <SFML/Graphics/Font.hpp>

class Fonts {
public:
  static sf::Font gfont;
private:
  static bool initialized;
  static bool PrepareFonts() {
    if (!gfont.loadFromFile("assets/fonts/OpenSans-Regular.ttf")) {
      throw std::runtime_error("Cannot load font");
    }
    return true;
  }
};


#endif //WARRIORS_FONTS_H
