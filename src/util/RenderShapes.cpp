//
// Created by aleksander on 31.05.19.
//

#include "RenderShapes.h"

sf::ConvexShape rendershapes::cavalry_base;
sf::ConvexShape rendershapes::hussar;
sf::ConvexShape rendershapes::petyhorzec;
sf::ConvexShape rendershapes::infantry_base;
sf::ConvexShape rendershapes::pikeman;
sf::ConvexShape rendershapes::musketeer;
sf::ConvexShape rendershapes::cannon;

void rendershapes::prepare() {

  cavalry_base.setPointCount(3);
  cavalry_base.setPoint(0, {0.8f, 0.0f});
  cavalry_base.setPoint(1, {-0.4f, -0.4f});
  cavalry_base.setPoint(2, {-0.4f, 0.4f});

  hussar.setPointCount(4);
  hussar.setPoint(0, {0.2f, -0.2f});
  hussar.setPoint(1, {-0.2f, -0.2f});
  hussar.setPoint(2, {-0.2f, 0.2f});
  hussar.setPoint(3, {0.2f, 0.2f});

  petyhorzec.setPointCount(3);
  petyhorzec.setPoint(0, {0.4f, 0.0f});
  petyhorzec.setPoint(1, {-0.2f, -0.2f});
  petyhorzec.setPoint(2, {-0.2f, 0.2f});

  infantry_base.setPointCount(4);
  infantry_base.setPoint(0, {-0.2f, 0.2f});
  infantry_base.setPoint(1, {-0.2f, -0.2f});
  infantry_base.setPoint(2, {0.2f, -0.2f});
  infantry_base.setPoint(3, {0.2f, 0.2f});

  pikeman.setPointCount(3);
  pikeman.setPoint(0, {-0.1f, 0.05f});
  pikeman.setPoint(1, {-0.1f, -0.05f});
  pikeman.setPoint(2, {0.6f, 0.0f});

  musketeer.setPointCount(4);
  musketeer.setPoint(0, {0.0f, 0.05f});
  musketeer.setPoint(1, {0.0f, -0.05f});
  musketeer.setPoint(2, {0.3f, -0.05f});
  musketeer.setPoint(3, {0.3f, 0.05f});

  cannon.setPointCount(4);
  cannon.setPoint(0, {-0.25f, 0.15f});
  cannon.setPoint(1, {-0.25f, -0.15f});
  cannon.setPoint(2, {0.3f, -0.15f});
  cannon.setPoint(3, {0.3f, 0.15f});
}
