//
// Created by aleksander on 07.02.19.
//

#ifndef WARRIORS_AVERAGEMEASURE_H
#define WARRIORS_AVERAGEMEASURE_H

#include <vector>
#include <cmath>
#include <mutex>

template <typename T>
class AccumulatedMeasure {
public:

  void add(T val) {
    std::lock_guard<std::mutex> lock(mut);
    data.push_back(val);
    if (memory_len > 0) {
      size_t data_size = data.size();
      if (data_size > memory_len) {
        data.erase(data.begin(), data.begin() + (data_size - memory_len)); //lazy implementation
      }
    }
  }

  void setMemoryLength(size_t len) { // 0 = unlimited
    std::lock_guard<std::mutex> lock(mut);
    memory_len = len;
  }

  T getSum() {
    return getLocalSum(data.size());
  }

  T getAverage() {
    return getLocalAverage(data.size());
  }

  T getStandardDeviation() {
    return getLocalStandardDeviation(data.size());
  }

  T getLocalSum(size_t amount) {
    std::lock_guard<std::mutex> lock(mut);
    T result = 0;
    size_t len = data.size();
    for (size_t i = (size_t) std::max((long int)0, (long int)(len-amount)); i < len; ++i) {
      result += data[i];
    }
    return result;
  }

  T getLocalAverage(size_t amount) {
    std::lock_guard<std::mutex> lock(mut);
    T result = 0;
    size_t len = data.size();
    if (len == 0) {
      return 0;
    }
    for (size_t i = (size_t) std::max((long int)0, (long int)(len-amount)); i < len; ++i) {
      result += data[i];
    }
    return result / std::min(len, amount);
  }

  T getLocalStandardDeviation(size_t amount) {
    std::lock_guard<std::mutex> lock(mut);
    T result = 0;
    size_t len = data.size();
    if (len == 0) {
      return 0;
    } else if (len == 1) {
      return data[0];
    }
    if (amount == 1) {
      return data[len-1];
    }
    T avg = getLocalAverage(amount);
    for (size_t i = (size_t) std::max((long int)0, (long int)(len-amount)); i < len; ++i) {
      T temp = data[i] - avg;
      result += temp*temp;
    }
    return sqrt(result/(std::min(len, amount)-1));
  }

  AccumulatedMeasure() = default;

  AccumulatedMeasure(const AccumulatedMeasure& other) {
    std::lock_guard<std::mutex> lock(other.mut);
    data = other.data;
    memory_len = other.memory_len;
  }

  AccumulatedMeasure& operator=(const AccumulatedMeasure& other) {
    std::lock_guard<std::mutex> lock(other.mut);
    data = other.data;
    memory_len = other.memory_len;
    return *this;
  }

  AccumulatedMeasure(AccumulatedMeasure&& other) noexcept {
    std::lock_guard<std::mutex> lock(other.mut);
    data = std::move(other.data);
    memory_len = other.memory_len;
  }

  AccumulatedMeasure& operator=(AccumulatedMeasure&& other) noexcept {
    std::lock_guard<std::mutex> lock(other.mut);
    data = std::move(other.data);
    memory_len = other.memory_len;
    return *this;
  }

private:
  std::vector<T> data;
  size_t memory_len = 0;
  std::mutex mut;
};


#endif //WARRIORS_AVERAGEMEASURE_H
