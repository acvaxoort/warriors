//
// Created by aleksander on 14.07.18.
//

#ifndef WARRIORS_PLANARGEOMETRYUTIL_H
#define WARRIORS_PLANARGEOMETRYUTIL_H

#include <cmath>
#include <SFML/Graphics.hpp>

namespace pgeom {


  template <typename T>
  inline float getDistance(T dx, T dy) {
    return std::sqrt(dx*dx + dy*dy);
  }
/*
  inline double getDistance(double dx, double dy) {
    return sqrt(dx*dx + dy*dy);
  }*/

  template <typename T>
  inline T getDistanceSqr(T dx, T dy) {
    return dx*dx + dy*dy;
  }
  /*
  inline double getDistanceSqr(double dx, double dy) {
    return dx*dx + dy*dy;
  }*/

  template <typename T>
  inline void limitVectorAbs(T& x, T&y, T limit) {
    T len = getDistance(x, y);
    if (len > limit) {
      len /= limit;
    }
    x /= len;
    y /= len;
  }

  template<typename T>
  T capAngle(T angle) {
    if (angle > M_PI*2) {
      angle -= ((int) (angle / M_PI*2)) * M_PI*2;
    } else if (angle < -M_PI*2) {
      angle += ((int) (-angle / M_PI*2)) * M_PI*2;
    }
    return angle;
  }

  template<typename T>
  T interpolate(T v1, T v2, T fraction) {
    T f1 = 1 - fraction;
    return v1 * f1 + v2 * fraction;
  }

  template<typename T>
  sf::Rect<T> interpolate(const sf::Rect<T>& previous, const sf::Rect<T>& current, T fraction) {
    return {
        pgeom::interpolate(previous.left, current.left, fraction),
        pgeom::interpolate(previous.top, current.top, fraction),
        pgeom::interpolate(previous.width, current.width, fraction),
        pgeom::interpolate(previous.height, current.height, fraction)
    };
  }

  template<typename T>
  sf::Vector2<T> interpolate(const sf::Vector2<T> previous, const sf::Vector2<T> current, T fraction) {
    return {
        pgeom::interpolate(previous.x, current.x, fraction),
        pgeom::interpolate(previous.y, current.y, fraction)
    };
  }

  template<typename T>
  T clockwiseToRad(T angle_deg_clockwise) {
    return ((T) 90 - angle_deg_clockwise) * (T) M_PI / (T) 180;
  }

  template<typename T>
  T radToClockwise(T angle_rad) {
    return (T) 90 - angle_rad * (T) 180 / (T) M_PI;
  }

  template<typename T>
  T radToCounterClockwise(T angle_rad) {
    return angle_rad * (T) 180 / (T) M_PI - (T) 90;
  }




}

#endif //WARRIORS_PLANARGEOMETRYUTIL_H
