//
// Created by aleksander on 13.07.18.
//

#ifndef WARRIORS_SFMLVIEWUTIL_H
#define WARRIORS_SFMLVIEWUTIL_H

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "PlanarGeometryUtil.h"

namespace sfmlwutil {

  sf::View getView(const sf::Window &window, float x, float y, float scale);

  sf::View getDefaultView(const sf::Window &window);

  sf::View interpolate(const sf::View& previous, const sf::View& current, float fraction);

  template<typename T>
  sf::Rect<T> getViewRect(const sf::View& view) {
    auto c = view.getCenter();
    auto s = view.getSize();
    return {c.x + s.x*0.5, c.y + s.y*0.5, s.x, s.y};
  }
  sf::Color interpolate(const sf::Color& col1, const sf::Color& col2, float fraction);

  extern const char* CONFIG_FILENAME;

  void openWindowFromConfig(sf::Window &window);

  void openDefaultWindow(sf::Window &window);

}

#endif //WARRIORS_SFMLVIEWUTIL_H
