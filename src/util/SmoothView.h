//
// Created by aleksander on 15.02.19.
//

#ifndef WARRIORS_SMOOTHVIEW_H
#define WARRIORS_SMOOTHVIEW_H


#include <SFML/Graphics/RenderWindow.hpp>

class SmoothView {
public:
  SmoothView(double scrollSpeed = 1, double scrollFriction = 0.5, double scaleSpeed = 1, double scaleFriction = 0.5);

  void setPosition(double x, double y);
  void setScale(double s);
  void accelerate(double dx2, double dy2);
  void accelerateScale(double ds2);
  void setScrollSpeedMax(double v);
  void setScaleSpeedMax(double v);;
  void setScrollBounds(double xmin, double xmax, double ymin, double ymax);
  void setScaleBounds(double smin, double smax);
  void mouseDrag(const sf::Window& window, int dmx, int dmy);
  void mouseScroll(const sf::Window& window, float dmz);
  void tick();
  sf::View getSfmlView(const sf::Window& window);

private:
  double x = 0;
  double y = 0;
  double dx = 0;
  double dy = 0;
  double scale = 0;
  double dscale = 0;
  double expscale = 1;
  double scrollSpeed;
  double scrollFriction;
  double scrollSpeedMax = std::numeric_limits<double>::quiet_NaN();
  double xMin = std::numeric_limits<double>::quiet_NaN();
  double xMax = std::numeric_limits<double>::quiet_NaN();
  double yMin = std::numeric_limits<double>::quiet_NaN();
  double yMax = std::numeric_limits<double>::quiet_NaN();
  double scaleSpeed;
  double scaleFriction;
  double scaleSpeedMax = std::numeric_limits<double>::quiet_NaN();
  double scaleMin = std::numeric_limits<double>::quiet_NaN();
  double scaleMax = std::numeric_limits<double>::quiet_NaN();
};


#endif //WARRIORS_SMOOTHVIEW_H
