setArea(1000, 1000)
Spartans = addTeam("Spartans", 100, 120, 255)
Thebans = addTeam("Thebans", 255, 100, 100)

addFalangeRegiment{team = Spartans, type =  "Hoplite", count = 1250, rows = 12, x = 250, y = 50, angle = 180, space = 1, fal_id=10}
for i = 0, 2 do
  addFalangeRegiment{team = Spartans, type =  "Hoplite", count = 1250, rows = 12, x = 450 + i*200, y = 50, angle = 180, space = 1, fal_id=0}
end
for i = 0, 3 do
  addFalangeRegiment{team = Spartans, type = "Hoplite", count = 1250, rows = 12, x = 250 + i*200, y = 80, angle = 180, space = 1, fal_id=5+i}
end
addCavalryRegiment{team = Spartans, type = "Cavalryman", count = 1000, rows = 10, x =200, y = 350, angle = 180, space = 1.5, fal_id=0}

for i = 0, 3 do
  addFalangeRegiment{team = Thebans, type = "Hoplite", count = 1166, rows = 10, x = 250, y = 880 + i*20, angle = 0, space = 1, fal_id=1+i}
end
addFalangeRegiment{team = Thebans, type = "Hoplite", count = 1168, rows = 10, x = 450, y = 910, angle = 0, space = 1, fal_id=0}
addFalangeRegiment{team = Thebans, type = "Hoplite", count = 1168, rows = 10, x = 650, y = 940, angle = 0, space =  1, fal_id=0}
addCavalryRegiment{team = Thebans, type = "Cavalryman", count = 1500, rows = 10, x = 200, y = 600, angle = 0, space = 1.5, fal_id=0}
