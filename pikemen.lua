setArea(1000, 1000)
setGridSize(10);
infantry = addTeam("Piechota", 100, 120, 255)
hussars = addTeam("Husaria", 255, 100, 100)

addPikeAndShootRegiment{team = infantry, count = 1000, rows = 10, cannons = 10, x = 500, y = 500, angle = 90}

addHussarRegiment{team = hussars, type = "Husarz", count = 1000, rows = 6, x = 900, y = 500, angle = 270, reckless_charge = false}
